<?php

namespace App\Http\Requests;

use App\Http\Repositories\AlbumRepository;
use App\Models\seo;
use Illuminate\Foundation\Http\FormRequest;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class AlbumRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $seo_ids = seo::where('seoable_type', 'like' , '%Album%')->where('seoable_id','!=',$this->id ?? 0)->pluck('id')->toArray();
        return RuleFactory::make([
            '%title%'                =>'required|string',
            '%description%'          =>'required|string',
            'seo.%meta_title%'       =>'required|string',
            'seo.%meta_keywords%'    =>'required|string',
            'seo.%meta_description%' =>'required|string',
            'seo.%slug%'             => ['required','string', Rule::unique('seo_translations','slug')->whereIn('seo_id',$seo_ids)],
            'mainImg.file'           =>'required_without:id|image|mimes:jpg,png,jpeg,',
            'mainImg.%alts%'           =>    'required_with:id',
         ]);

    }

    protected function prepareForValidation()
    {
        $languages    = new AlbumRepository();
        $languages    = $languages->languages()->pluck('code')->toArray();

        $seo = [];
        $req_seo = $this->seo;

        if(!$this->id){
            foreach ($languages as $language_code)
            {
                $seo[$language_code] = [
                    "meta_title"        => @$req_seo[ $language_code]['meta_title']          ??  @$this->$language_code['title'] ,
                    "slug"              => @$req_seo[ $language_code]['slug']                ??  Str::replace(' ' , '_' , @$this->$language_code['title'])  ,
                    "meta_keywords"     => @$req_seo[ $language_code]['meta_keywords']       ??  @$this->$language_code['title'] ,
                    "meta_description"  => @$req_seo[ $language_code]['meta_description']    ??  Str::limit(strip_tags(@$this->$language_code['description']),180),
                ];
            }
        }else{
            $seo = $req_seo;
        }

        $this->merge([
            'seo'       => $seo,
        ]);
    }


    public function messages()
    {
        return [
            'title' => 'Please Enter Title Value',
            'description' => 'Please Enter Description Value',
            'meta_title' => 'Must Be Meta title Type',
            'meta_keywords' => 'Must Be Meta Keywords Type',
            'meta_description' => 'Must Be Meta Description Type',
            'slug' => 'Must Be slug Type',
            'mainImg.file' => 'Image Field Required',
        ];
    }

}
