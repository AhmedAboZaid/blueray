<?php

namespace App\Http\Requests;

use App\Http\Repositories\MainPageRepository;
use App\Models\Language;
use App\Models\seo;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;
class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        $seo_ids = seo::where('seoable_type', 'like' , '%Package%')->where('seoable_id','!=',$this->id ?? 0)->pluck('id')->toArray();
        return RuleFactory::make([
            '%title%'                 =>    'required|string',
            '%description%'           =>    'required|string',
            '%travel_plan_description%'=>   'required|string',
            '%departure%'             =>    'nullable|string',
            'seo.%meta_title%'        =>    'required|string',
            'seo.%meta_keywords%'     =>    'required|string',
            'seo.%meta_description%'  =>    'required|string',
            'seo.%slug%'              =>    ['required','string', Rule::unique('seo_translations','slug')->whereIn('seo_id',$seo_ids)],
            'image.%alts%'            =>    'required_with:id',
            'image.file'              =>    'required_without:id|image|mimes:jpg,png,jpeg,',
            'tourism_type_id'         =>    'required|exists:tourism_types,id',
            'city_id'                 =>    'required|exists:cities,id',
            'nights'                  =>    'required|numeric',
            'languages'               =>    'nullable',
            'languages.*'             =>    'numeric|exists:languages,id',
            'travelPlans'             =>    'nullable',
            'travelPlans.*number'     =>    'required',
            'travelPlans.*description'       =>    'required',
        ]);
    }

    public function messages()
    {
        return [
            'title'                     => 'Please Enter Title Value',
            'alt'                       => 'Please Enter Alt Value',
            'description'               => 'Please Enter Description Value',
            'image'                     => 'Image Field Required',
            'meta_title'                => 'Must Be Meta title Type',
            'meta_keywords'             => 'Must Be Meta Keywords Type',
            'meta_description'          => 'Must Be Meta Description Type',
            'slug'                      => 'Must Be slug Type',
            'image.file.required'       => 'City Image Is Required',
            'image.file.image'          => 'Attachment must be Image',
            'tourism_type_id.required'  => 'This Field Is Required',
            'city_id.required'          => 'This Field Is Required',
        ];
    }

    protected function prepareForValidation()
    {
        // dd($this->all());
        $languages    = Language::where('isActive',1)->pluck('code')->toArray();
        $travelPlans = [];

        foreach ($this->travelPlans??[] as $travelPlan)
        {
            foreach ($languages as $code) {
                $travelPlan[$code] = [
                    "title"        => @$travelPlan[ $code .'_title']       ,
                    "description"  => @$travelPlan[ $code .'_description'] ,
                ];
            }
            $travelPlans[] = $travelPlan;
        }

        $seo = [];
        $req_seo = $this->seo;
        if(!$this->id){
            foreach ($languages as $language_code)
            {
                $seo[$language_code] = [
                    "meta_title"        => @$req_seo[ $language_code]['meta_title']          ??  @$this->$language_code['title'] ,
                    "slug"              => @$req_seo[ $language_code]['slug']                ??  Str::replace(' ' , '_' , @$this->$language_code['title'])  ,
                    "meta_keywords"     => @$req_seo[ $language_code]['meta_keywords']       ??  @$this->$language_code['title'] ,
                    "meta_description"  => @$req_seo[ $language_code]['meta_description']    ??  Str::limit(strip_tags(@$this->$language_code['description']),180),
                ];
            }
        }else{
            $seo = $req_seo;
        }

        $this->merge([
            'seo'       => $seo,
            'travelPlans'       => $travelPlans,
        ]);



    }
}
