<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Astrotomic\Translatable\Validation\RuleFactory;

class FAQRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
          return RuleFactory::make([
            '%questions%'             => 'required|string',
            '%answers%'               =>'required|string',
            'Pid'               =>'required|integer',
         ]);

    }


    public function messages()
    {
        return [
            'questions' => 'Please Enter Questions Value',
            'answers' => 'Please Enter Answers Value',
            'Pid' => 'Please Enter Question Parent Value',
        ];
    }
}
