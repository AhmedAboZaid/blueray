<?php

namespace App\Http\Requests;

use App\Http\Repositories\BranchRepository;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return RuleFactory::make([
            '%name%'       => 'required|string',
            '%address%'    => 'required|string',
            'mainImg.file' => 'required_without:id|image|mimes: jpg,png,jpeg,',
            'phone'        => 'required|regex:/[0-9]/',
            'fax'          => 'required|regex:/[0-9]/',
            'email'        => 'required|email',
            'facebook'     => 'required|url',
            'instagram'    => 'required|url',
            'twitter'      => 'required|url',
            'whatsapp'     => 'required|url',
         ]);
    }
    protected function prepareForValidation()
    {
        $data=$this->all();
        $img=$this->mainImg;
        $languages    = new BranchRepository();
        $languages    = $languages->languages()->pluck('code')->toArray();
        foreach($languages as $code){
            $img[$code]['alts'] = $img[$code]['alts'] ?? $data[$code]['name'];
        }
        $this->merge(['mainImg'=> $img]);
        // dd($this->all());
    }
}
