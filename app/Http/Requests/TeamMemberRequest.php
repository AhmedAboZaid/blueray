<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Astrotomic\Translatable\Validation\RuleFactory;

class TeamMemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
          return RuleFactory::make([
            '%name%'       => 'required|string',
            '%position%'   => 'required|string',
            'linkedin'     =>'required|url',  
            'mainImg.file'        =>'required_without:id|image|mimes:jpg,png,jpeg,'
         ]);

    }


    public function messages()
    {
        return [
            'name' => 'Please Enter Name Value',
            'linkedin' => 'linkedin Must Be a Link',
            'position' => 'Please Enter Position Value',
            'mainImg.file' => 'Must Be Image Type',
        ];
    }
}
