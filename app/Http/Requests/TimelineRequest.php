<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Astrotomic\Translatable\Validation\RuleFactory;

class TimelineRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {   
          return RuleFactory::make([
            '%title%'       => 'required|string',
            '%description%'   => 'required|string',
            'date'     =>'required|date',  
         ]);

    }


    public function messages()
    {
        return [
            'title' => 'Please Enter Title Value',
            'description' => 'Please Enter Descriptions Value',
            'date.required' => 'Please Enter Date Value',
            'date.date' => 'Must Be Date Type',
        ];
    }
}
