<?php

namespace App\Http\Requests;

use App\Http\Repositories\ServiceRepository;
use App\Models\seo;
use Illuminate\Foundation\Http\FormRequest;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Validation\Rule;

class ServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $seo_ids = seo::where('seoable_type', 'like' , '%Service%')->where('seoable_id','!=',$this->id ?? 0)->pluck('id')->toArray();
        return RuleFactory::make([
            '%title%'                 =>'required|string',
            '%description%'           =>'required|string',
            // 'seo.%meta_title%'        =>'required|string',
            // 'seo.%meta_keywords%'     =>'required|string',
            // 'seo.%meta_description%'  =>'required|string',
            // 'seo.%slug%'             => ['required','string', Rule::unique('seo_translations','slug')->whereIn('seo_id',$seo_ids)],
            'mainImg.file'                    =>'required_without:id|image|mimes:svg,',

        ]);
    }

    public function messages()
    {
        return [
            'title' => 'Please Enter Title Value',
            'description' => 'Please Enter Description Value',
            'mainImg.file' => 'Image Field Required',
            'meta_title' => 'Must Be Meta title Type',
            'meta_keywords' => 'Must Be Meta Keywords Type',
            'meta_description' => 'Must Be Meta Description Type',
            'slug' => 'Must Be slug Type',
            'video' => 'Must Be Link Type',
        ];
    }
}
