<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreTicketForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'from_date'            => ['nullable','date'],
            'to_date'              => ['nullable','date'],

           //persons
           'persons'               => ['nullable','array'],
           'persons.*.name'        => ['required','string'],
           'persons.*.birthday'    => ['required','date'],

        ];
    }
}
