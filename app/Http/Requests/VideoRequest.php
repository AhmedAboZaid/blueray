<?php

namespace App\Http\Requests;

use App\Http\Repositories\VideoRepository;
use App\Models\seo;
use Illuminate\Foundation\Http\FormRequest;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Validation\Rule;
use Illuminate\Support\Str;

class VideoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $seo_ids = seo::where('seoable_type', 'like' , '%Video%')->where('seoable_id','!=',$this->id ?? 0)->pluck('id')->toArray();
        return RuleFactory::make([
            '%title%'                 =>'required|string',
            '%description%'           =>'required|string',
            // '%alt%'                   =>'required|string',
            'seo.%meta_title%'        =>'required|string',
            'seo.%meta_keywords%'     =>'required|string',
            'seo.%meta_description%'  =>'required|string',
            'seo.%slug%'              => ['required','string', Rule::unique('seo_translations','slug')->whereIn('seo_id',$seo_ids)],
            'video_link'              =>'required',
        ]);

    }


    protected function prepareForValidation()
    {
        $languages    = new VideoRepository();
        $languages    = $languages->languages()->pluck('code')->toArray();

        $seo = [];
        $req_seo = $this->seo;

        foreach ($languages as $language_code) {
            $seo[$language_code] = [
                "meta_title"        => @$req_seo[$language_code]['meta_title']          ??  @$this->$language_code['title'],
                "slug"              => @$req_seo[$language_code]['slug']                ??  Str::replace(' ', '_', @$this->$language_code['title']),
                "meta_keywords"     => @$req_seo[$language_code]['meta_keywords']       ??  @$this->$language_code['title'],
                "meta_description"  => @$req_seo[$language_code]['meta_description']    ??  Str::limit(strip_tags(@$this->$language_code['description']), 180),
            ];
        }

        $this->merge([
            'seo'       => $seo,
        ]);

    }
    public function messages()
    {
        return [
            'title' => 'Please Enter Title Value',
            'alt' => 'Please Enter Alt Value',
            'description' => 'Please Enter Description Value',
            'video_link' => 'Must Be Link Type',
            'meta_title' => 'Must Be Meta title Type',
            'meta_keywords' => 'Must Be Meta Keywords Type',
            'meta_description' => 'Must Be Meta Description Type',
            'slug' => 'Must Be slug Type',
        ];
    }
}
