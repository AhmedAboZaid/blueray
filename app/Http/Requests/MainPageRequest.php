<?php

namespace App\Http\Requests;

use App\Http\Repositories\MainPageRepository;
use App\Models\seo;
use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;

class MainPageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $seo_ids = seo::where('seoable_type', 'like', '%MainPage%')->where('seoable_id', '!=', $this->id ?? 0)->pluck('id')->toArray();
        return RuleFactory::make([
            '%title%'             => 'required|string',
            '%description%'       => 'required|string',
            // '%alt%'               => 'required|string',
            '%image%'             => 'required_without:id|image|mimes:jpg,png,jpeg,',
            'seo.%meta_title%'        => 'string',
            'seo.%meta_keywords%'     => 'string',
            'seo.%meta_description%'  => 'string',
            'seo.%slug%'             => ['required', 'string', Rule::unique('seo_translations', 'slug')->whereIn('seo_id', $seo_ids)],
        ]);
    }
    protected function prepareForValidation()
    {
        // dd( $this->$ss['alt']);
        $languages    = new MainPageRepository();
        $languages    = $languages->languages()->pluck('code')->toArray();

        $seo = [];
        $req_seo = $this->seo;

        foreach ($languages as $language_code) {
            $seo[$language_code] = [
                "meta_title"        => @$req_seo[$language_code]['meta_title']          ??  @$this->$language_code['title'],
                "slug"              => @$req_seo[$language_code]['slug']                ??  Str::replace(' ', '_', @$this->$language_code['title']),
                "meta_keywords"     => @$req_seo[$language_code]['meta_keywords']       ??  @$this->$language_code['title'],
                "meta_description"  => @$req_seo[$language_code]['meta_description']    ??  Str::limit(strip_tags(@$this->$language_code['description']), 180),
            ];
        }

        $this->merge([
            'seo'       => $seo,
        ]);

    }

    public function messages()
    {
        return [
            'title' => 'Please Enter Title Value',
            'alt' => 'Please Enter Alt Value',
            'description' => 'Please Enter Description Value',
            'image' => 'Must Be Image Type',
            'meta_title' => 'Must Be Meta title Type',
            'meta_keywords' => 'Must Be Meta Keywords Type',
            'meta_description' => 'Must Be Meta Description Type',
            'slug' => 'Must Be slug Type',
        ];
    }
}
