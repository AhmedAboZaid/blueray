<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Astrotomic\Translatable\Validation\RuleFactory;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return RuleFactory::make([
            '%title%'                 =>'required|string',
            '%description%'           =>'required|string',
            'seo.%meta_title%'        =>'required|string',
            'seo.%meta_keywords%'     =>'required|string',
            'seo.%meta_description%'  =>'required|string',
            'seo.%slug%'              =>'required|string',
        ]);

    }


    public function messages()
    {
        return [
            'title' => 'Please Enter Title Value',
            'alt' => 'Please Enter Alt Value',
            'description' => 'Please Enter Description Value',
            'image' => 'Image Field Required',
            'meta_title' => 'Must Be Meta title Type',
            'meta_keywords' => 'Must Be Meta Keywords Type',
            'meta_description' => 'Must Be Meta Description Type',
            'slug' => 'Must Be slug Type',
        ];
    }
}
