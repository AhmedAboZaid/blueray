<?php

namespace App\Http\Requests;

use Astrotomic\Translatable\Validation\RuleFactory;
use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return RuleFactory::make([
            '%title%'             => 'required|string',
            '%short_description%' => 'required|string',
            'btn'                 =>'nullable|url',
            'image.%alts%'        =>'required|string',
            'image.file'          =>'required_without:id|image|mimes:jpg,png,jpeg,'
        ]);
    }


    public function messages()
    {
        return [
            'title'             => 'Please Enter Title Value',
            'alt'               => 'Please Enter Alt Value',
            'btn'               => 'BTN Must Be a Link',
            'short_description' => 'Please Enter Description Value',
            'image.file'        => 'Must Be Image Type',
        ];
    }
}
