<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Astrotomic\Translatable\Validation\RuleFactory;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
          return RuleFactory::make([
            '%title%'             => 'required|string',
            'image.file'          => 'required_without:id|image|mimes:jpg,png,jpeg,',
            'image.%alts%'        => 'string|required',
         ]);

    }


    public function messages()
    {
        return [
            'title'      => 'Please Enter Title Value',
            'image.file' => 'Must Be Image Type',
        ];
    }
}
