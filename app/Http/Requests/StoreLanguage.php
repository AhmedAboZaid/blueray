<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLanguage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'language' =>'required',
           'code' =>'required',
           'flag'    =>'nullable|image|mimes:jpg,png,jpeg,'
        ];
    }

    public function messages()
    {
        return [
            'language.required' => 'Please Enter Language Value',
            'flag' => 'Flag Type Must Be Image Type',
            'code' => 'Please Enter Code Value',
        ];
    }
}
