<?php

namespace App\Http\Resources;

use App\Models\Package;
use Illuminate\Http\Resources\Json\JsonResource;

class PackageResourceV2 extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =
        [
            'id'                =>   $this->id,
            'image_url'         =>   asset("uploads/attachments/".@$this->image?->file),
            'image_alt'         =>   @$this->image?->alts,
            'meta'              =>   new SeoResource( $this->seo ),
            'title'             =>   $this->title,
            'country'           =>   new TagResource($this->country),
            'city'              =>   new TagResource($this->city?->parent_id ? $this->city : null),
            "nights"            =>   $this->nights,
            "tour_type"         =>   $this->tour_type,
            "languages"         =>   count($this->languages) ? implode( ',' , $this->languages->pluck('language')->toArray() ) : "Any Languages",
            'information'       =>   [
                "overview"      =>   [
                    "description"   =>  $this->description
                ],
                "details"       =>  [
                    [
                        "id"        =>  1,
                        "key"       =>  "Destination",
                        "value"     =>  $this->city?->title,
                    ],
                    [
                        "id"        =>  2,
                        "key"       =>  "Depature",
                        "value"     =>  $this->travelPlans->count() ? "Yes Required" : "No Depature",
                    ],
                    // [
                    //     "id"        =>  3,
                    //     "key"       =>  "Departure Time",
                    //     "value"     =>  date_create($this->from_date)->format('d M, Y h:i A'),
                    // ],
                    // [
                    //     "id"        =>  4,
                    //     "key"       =>  "Return Time",
                    //     "value"     =>  date_create($this->to_date)->format('d M, Y h:i A'),
                    // ],
                    [
                        "id"        =>  5,
                        "key"       =>  "Included",
                        "value"     =>  FacilityResource::collection($this->facilitiesInclude),
                    ],
                    [
                        "id"        =>  6,
                        "key"       =>  "Excluded",
                        "value"     =>  FacilityResource::collection($this->facilitiesExclude),
                    ],
                ]
            ],
            'travel_plan'       => [
                "overview"      =>   [
                    "description"   =>  $this->travel_plan_description
                ],
                "days"          =>  TravelPlanResource::collection($this->travelPlans)
            ],
            "gallery"           =>  GalleryResource::collection($this->attachments),
            "popular_packages"  =>  PopularPackagesResource::collection(Package::inRandomOrder()->limit(6)->get()),


        ];

        $url = [];
        foreach ($this->seo->translations as $seo) {
            $url[$seo->locale] = $seo->locale.'/packages/'.$seo->slug;
        }
        $data['url'] = $url;
        return $data ;
    }
}
