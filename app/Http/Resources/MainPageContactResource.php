<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MainPageContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return
        [
            'image_url' => asset('uploads/main-pages/'.$this->image),
            'image_alt' => $this->alt,
            'meta'      => new SeoResource( $this->seo ),
            'title'     => $this->title,
            'offices'   => @$this->offices,
            'map'       => @$this->map,
            'from_day'  => @$this->from_day,
            'to_day'    => @$this->to_day,
            'from_hour' => @$this->from_hour,
            'to_hour'   => @$this->to_hour,
            'close_day' => @$this->close_day,
        ];
    }
}
