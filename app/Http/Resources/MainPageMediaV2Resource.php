<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MainPageMediaV2Resource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return
        [
            'id'                    => $this->id,
            'title'                 => $this->title,
            'image_url'             => $this->image_url,
            'image_alt'             => @$this->alt,
            'slug'                  => $this->seo?->slug,
        ];
    }
}
