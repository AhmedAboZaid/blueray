<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MainPageAlbumsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return
        array_merge(
        [
            'image_url'             => asset('uploads/main-pages/'.$this->image),
            'image_alt'             =>$this->alt,
            'meta'                  => new SeoResource( $this->seo ),
            'title'                 => $this->title,
            'url'                  => $this->url(),


        ]

        , $this->additionalAttr())
        ;
    }

    public function additionalAttr()
    {
        if($this->albums){
            return ['albums'                => $this->albums ?? []];
        }
        if($this->videos){
            return ['videos'                => $this->videos ?? []];
        }
        if($this->blogs){
            return ['blogs'                => $this->blogs ?? []];
        }
        return [];
    }
    public function url()
    {
        $url = [];
        foreach ($this->seo->translations as $seo) {
            $url[$seo->locale] = $seo->locale.'/'.$seo->slug;
        }
       return $url;
        return [] ;
    }
}
