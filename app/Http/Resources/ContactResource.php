<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContactResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'image_url'=>asset('uploads/main-pages/'.$this['main_page']['image']),
            'image_alt'=>$this['main_page']['alt'],
            'meta'=>new SeoResource($this['main_page']['seo']),
            'title'=>$this['main_page']['title'],
            'offices'=>$this['sections'],
            'team'=>$this['team'],
        ];
    }
}
