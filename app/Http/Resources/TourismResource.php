<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TourismResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = 
        [
            'id'                    => $this->id,
            'title'                 => $this->title,
            'image_url'             => asset("uploads/attachments/".@$this->image->file),
            'image_alt'             => @$this->image->alts,
            'icon'                  => asset("uploads/attachments/".@$this->icon->file),
            'slug'                  => $this->seo?->slug,
        ];

        $url = [];
        foreach ($this->seo->translations as $seo) {
            $url[$seo->locale] = $seo->locale.'/'.$seo->slug;
        }
        $data['url'] = $url;
        return $data ;
    }
}
