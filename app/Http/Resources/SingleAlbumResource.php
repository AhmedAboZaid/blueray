<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SingleAlbumResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = 
            [
                'image_url'    => @$this->image?->url,
                'image_alt'    => @$this->image?->alts,
                'meta'         => new SeoResource($this->seo),
                'title'        => $this->title,
                'description'  => $this->description,
                'images'  => AttachmentResource::collection($this->attachments),
            ];

            $url = [];
            foreach ($this->seo->translations as $seo) {
                $url[$seo->locale] = $seo->locale.'/albums/'.$seo->slug;
            }
            $data['url'] = $url;
            return $data ;
    }
}

