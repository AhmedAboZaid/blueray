<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MainPageServiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return
        [
            'id'                    => $this->id,
            'breadcrumb'            => $this->title,
            'title'                 => $this->title,
            'description'           => $this->description,
            'image_url'             => $this->image_url,
            'image_alt'             => @$this->alt,
            'slug'                  => $this->seo?->slug,
        ];
    }
}
