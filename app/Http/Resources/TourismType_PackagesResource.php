<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TourismType_PackagesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $data= [

            'image_url'             => asset("uploads/attachments/".$this->image->file),
            'image_alt'             => @$this->image->alts,
            'meta'                  => new SeoResource( $this->seo ),
            'title'                 => $this->title,
            'packages'              => $this->whenLoaded('packages' , function(){return PackageResource::collection($this->packages); }),
        ];

        $url = [];
        foreach ($this->seo->translations as $seo) {
            $url[$seo->locale] = $seo->locale.'/tourisms/'.$seo->slug;
        }
        $data['url'] = $url;
        return $data ;
    }
}
