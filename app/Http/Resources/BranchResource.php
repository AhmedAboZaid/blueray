<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BranchResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
       return [
            'id'=> $this->id,
            'flag'=> $this->image->url ?? null,
            'title'=> $this->name,
            'address'=> $this->address,
            'phone'=> $this->phone,
            'fax'=> $this->fax,
            'email'=> $this->email,
            'socials'=>[
                '0'=>[
                'id'=>1,
                'icon'=>'FaFacebookF',
                'url'=>@$this->facebook,
                ],
                '1'=>[
                'id'=>2,
                'icon'=>'FaInstagram',
                'url'=>@$this->instagram,
                ],
                '2'=>[
                'id'=>3,
                'icon'=>'FaTwitter',
                'url'=>@$this->twitter,
                ],
                '3'=>[
                'id'=>4,
                'icon'=>'FaWhatsapp',
                'url'=>@$this->whatsapp,
                ]
            ]
            // 'map'=> @$this->iframe_link,
       ];
    }
}
