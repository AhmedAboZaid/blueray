<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TravelPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return
        [
            'id'             => $this->id,
            'number'         => $this->number,
            'title'          => $this->title,
            // 'duration'       => date_create($this->from)->format('h:i A') . ' to ' . date_create($this->to)->format('h:i A'), // "10.00 AM to 10.00 PM"
            'description'    => $this->description,
        ];
    }
}
