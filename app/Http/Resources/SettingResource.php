<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $meta=[
            "meta_title"=> $this->where('key','meta_title')->first()->value  ,
            "meta_description"=> $this->where('key','meta_description')->first()->value ,
            "meta_keyword"=> $this->where('key','meta_keywords')->first()->value ,
            "slug"=> $this->where('key','meta_title')->first()->value
        ];
        return [
            // 'key'   => $this->key,
            // 'value' => $this->value,
            'image_url'=>asset('public/uploads/setting/'.$this->where('key','logo')->first()->value),
            'image_alt'=>$this->where('key','alt')->first()->value,
            'meta'=>$meta,


        ];
    }
}
