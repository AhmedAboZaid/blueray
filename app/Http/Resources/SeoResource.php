<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SeoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return 
        [
            'meta_title'        => $this->meta_title,
            'meta_description'  => $this->meta_description,
            'meta_keyword'      => $this->meta_keywords,
            'slug'              => $this->slug,
        ];
    }
}
