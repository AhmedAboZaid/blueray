<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SliderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'image_url'   => asset("uploads/attachments/".$this->image->file),
            'image_alt'   => $this->image->alts,
            'title'       => $this->title,
            'description' => $this->short_description,
            'tour_slug'   => $this->btn,
        ];
    }
}
