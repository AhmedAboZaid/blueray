<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class VideoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data =  [
            'id'          => $this->id,
            'title'       => $this->title,
            'code'  => substr($this->video_link,strpos($this->video_link,'v=')+2),
            'alt'         => $this->alt,
            // 'description' => $this->description,
            // 'meta'        => new SeoResource($this->seo),
        ];

        $url = [];
        foreach ($this->seo->translations as $seo) {
            $url[$seo->locale] = $seo->locale.'/videos/'.$seo->slug;
        }
        $data['url'] = $url;
        return $data ;
    }
}
