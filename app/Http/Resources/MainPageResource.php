<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MainPageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return
        [
            'id'               => $this->id,
            'fixed_name'       =>$this->fixed_name,
            'title'            => $this->title,
            'description'      => $this->description,
            'image_path'       => asset('uploads/main-pages/'.$this->image),
            'alt'              => $this->alt,
            'created_at'       => $this->created_at,
            'meta'             => new SeoResource($this->seo), 
        ];
    }
}
