<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        return
        [
            'id'          => $this->id,
            // 'author'       => $this->author,
            'title'       => $this->title,
            'date'  => date('j F, Y',strtotime($this->created_at)),
            'image_url'  => asset("uploads/attachments/".$this->image->file),
            'image_alt'    => $this->image->alts,
            'slug'        => $this->seo->slug,
        ];
    }
}
