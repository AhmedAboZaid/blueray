<?php

namespace App\Http\Resources;

use App\Models\TourismType;
use Illuminate\Http\Resources\Json\JsonResource;

class MainPageNavbarResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $dropdown = [];
        if($this->fixed_name != 'about' && $this->childs->count()){
            $dropdown = [
                'dropdown'  => MainPageNavbarResource::collection($this->childs),
            ];
        }
        if($this->fixed_name == 'tourisms'){
            $dropdown = [
                'dropdown'  => TourismNavResource::collection(TourismType::all()),
            ];
        }
        return
        array_merge(
            [
                'id'    => $this->id + 1, // as home will take id 1
                'title' => $this->title,
                'slug'  => $this->fixed_name ?? "" ,
            ],
            $dropdown
        );
    }
}
