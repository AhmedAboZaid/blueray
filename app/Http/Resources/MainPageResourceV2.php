<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MainPageResourceV2 extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return
        [
            'image_url'             => asset('uploads/main-pages/'.$this->image),
            'image_alt'             =>$this->alt,
            'meta'                  => new SeoResource( $this->seo ),
            'title'                 => $this->title,
            'description'           => $this->description,
            'tourisms'              => $this->whenLoaded('childs' , function(){return TourismResource::collection($this->childs); }),
        ];
    }
}
