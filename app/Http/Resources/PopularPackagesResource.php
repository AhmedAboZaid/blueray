<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PopularPackagesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = 
        [
            'id'                =>   $this->id,
            'title'             =>   $this->title,
            "price"             =>   $this->price,
            "nights"            =>   $this->nights,
            'image_url'         =>   asset("uploads/attachments/".@$this->image?->file),
            'image_alt'         =>   @$this->image?->alts,
            'tour_slug'         => $this->tour_slug,
            'slug'              => $this->seo?->slug
        ];
        $url = [];
        foreach ($this->seo->translations as $seo) {
            $url[$seo->locale] = $seo->locale.'/packages/'.$seo->slug;
        }
        $data['url'] = $url;
        return $data ;
    }
}
