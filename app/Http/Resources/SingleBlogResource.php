<?php

namespace App\Http\Resources;

use App\Models\Blog;
use Illuminate\Http\Resources\Json\JsonResource;

class SingleBlogResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = 
        [
            // 'id'         => $this->id,
            'image_url'     => @$this->image?->url,
            'image_alt'     => @$this->image?->alts,
            'meta'          => new SeoResource($this->seo),
            // 'author'     => $this->author,
            'title'         => $this->title,
            'description'   => $this->description,
            'date'          => date('j F, Y',strtotime($this->created_at)),
            'tags'          => TagResource::collection($this->tags),
            'popular_posts' => $this->popular_posts,
        ];

        $url = [];
        foreach ($this->seo->translations as $seo) {
            $url[$seo->locale] = $seo->locale.'/blogs/'.$seo->slug;
        }
        $data['url'] = $url;
        return $data ;
    }
}
