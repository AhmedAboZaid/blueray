<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AlbumResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $date =
        [
            'id'        => $this->id,
            'title'     => $this->title,
            'image_url' => @$this->image?->url,
            'image_alt' => $this->image?->alts,
            'slug'      => @$this->seo?->slug,
        ];
        $url = [];
        foreach ($this->seo->translations as $seo) {
            $url[$seo->locale] = $seo->locale.'/albums/'.$seo->slug;
        }
        $data['url'] = $url;
        return $data ;
    }
}
