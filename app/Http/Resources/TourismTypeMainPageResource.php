<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TourismTypeMainPageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $data = [
            'image_url'             => asset('uploads/main-pages/'.$this->image),
            'image_alt'             =>$this->alt,
            'meta'                  => new SeoResource( $this->seo ),
            'title'                 => $this->title,
            'description'           => $this->description,
            'tourisms'              => $this->tourisms,
        ];

        $url = [];
        foreach ($this->seo->translations as $seo) {
            $url[$seo->locale] = $seo->locale.'/'.$seo->slug;
        }
        $data['url'] = $url;
        return $data ;
    }
}
