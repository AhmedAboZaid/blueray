<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $data = 
        [
            'id'                => $this->id,
            'title'             => $this->title,
            'rate'              => $this->rate,

            'country'           => new CountryResource($this->country),
            'city'              => new CountryResource($this->city),

            'nights'            => $this->nights,

            'image_url'         => asset("uploads/attachments/".$this->image->file),
            'tour_slug'         => $this->tour_slug,
            'slug'              => $this->seo?->slug,
        ];
        
        $url = [];
            foreach ($this->seo->translations as $seo) {
                $url[$seo->locale] = $seo->locale.'/packages/'.$seo->slug;
            }
            $data['url'] = $url;
            return $data ;
    }

}
