<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TeamResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'position' => $this->position,
            'image_url' => asset('/uploads/team-members/' . $this->image->file),
            'image_alt' => $this->name,
            'social_links' => [
                '0' => [
                    'url' => $this->linkedin,
                    'icon'=> 'FaLinkedin'
                ]
            ],
        ];
    }
}
