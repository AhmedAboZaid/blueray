<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface SliderInterface
{
    public  function languages();
    public  function index();
    public  function store(Request $request);
    public  function edit($slider);
    public  function update(Request $request);
    public  function destroy(Request $request);
}