<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface CityInterface
{
    public  function languages();
    public  function index();
    public  function tags();
    public  function store(Request $request);
    public  function get_all();
    public  function edit($city);
    public  function update(Request $request);
    public  function destroy(Request $request);
}
