<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface FaqInterface
{
    public  function languages();
    public  function index();
    public  function store(Request $request);
    public  function edit($faq);
    public  function update(Request $request);
    public  function destroy(Request $request);
}