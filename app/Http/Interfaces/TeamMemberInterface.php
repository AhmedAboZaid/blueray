<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface TeamMemberInterface
{
    public  function languages();
    public  function index();
    public  function store(Request $request);
    public  function edit($teamMember);
    public  function update(Request $request);
    public  function destroy(Request $request);
}