<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface TourismTypeInterface
{
    public  function languages();
    public  function index();
    public  function first($whereClause=[],$with=[]);
    public  function showBySlug($slug , $with);
    public  function tags();
    public  function store(Request $request);
    public  function edit($tourismType);
    public  function update(Request $request);
    public  function destroy(Request $request);
}
