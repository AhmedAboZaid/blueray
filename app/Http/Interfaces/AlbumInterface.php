<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface AlbumInterface
{
    public  function languages();
    public  function index();
    public  function store(Request $request);
    public  function edit($album);
    public  function showBySlug($slug);
    public  function update(Request $request);
    public  function destroy(Request $request);
}