<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface LayoutInterface
{
    public  function languages();
    public  function navbar();
    public  function footer();
}
