<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface FormInterface
{
    public  function contact(Request $request);
    public  function bookForm(Request $request);
    public  function ticketForm(Request $request);

}
