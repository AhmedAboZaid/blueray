<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface VideoInterface
{
    public  function languages();
    public  function index();
    public  function store(Request $request);
    public  function edit($video);
    public  function update(Request $request);
    public  function destroy(Request $request);
}