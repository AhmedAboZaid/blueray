<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface SettingInterface
{
    public  function languages();
    public  function index();
    public  function update(Request $request);
}