<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface PackageInterface
{
    public  function languages();
    public  function index();
    public  function first($whereClause=[],$with=[]);
    public  function showBySlug($slug , $with=[]);
    public  function tags();
    public  function facilities();
    public  function cities($where=[]);
    public  function tourism_types();
    public  function store(Request $request);
    public  function edit($city);
    public  function update(Request $request);
    public  function destroy(Request $request);
}
