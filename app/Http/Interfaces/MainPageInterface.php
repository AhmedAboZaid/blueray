<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface MainPageInterface
{
    public  function languages();
    public  function index();
    public  function first($whereClause=[],$with=[]);
    public  function edit($mainPage);
    public  function update(Request $request);
}