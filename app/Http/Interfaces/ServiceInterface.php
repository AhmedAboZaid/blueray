<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface ServiceInterface
{
    public  function languages();
    public  function index();
    public  function tags();
    public  function store(Request $request);
    public  function edit($service);
    public  function update(Request $request);
    public  function destroy(Request $request);
}