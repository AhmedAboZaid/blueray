<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface LanguageInterface
{
    public  function index();
    public  function store(Request $request);
    public  function edit($language);
    public  function update(Request $request);
    public  function destroy(Request $request);
    public function changeStatus(Request $request);
}