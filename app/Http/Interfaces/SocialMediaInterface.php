<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface SocialMediaInterface
{
    public  function index();
    public  function store(Request $request);
    public  function edit($socialMedia);
    public  function update(Request $request);
    public  function destroy(Request $request);
}