<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface ProjectInterface
{
    public  function languages();
    public  function index();
    public  function tags();
    public  function store(Request $request);
    public  function edit($project);
    public  function update(Request $request);
    public  function destroy(Request $request);
}