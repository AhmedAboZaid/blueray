<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface BlogInterface
{
    public  function languages();
    public  function index();
    public  function random($limit=1000 , $where = []);
    public  function tags();
    public  function store(Request $request);
    public  function edit($blog);
    public  function showBySlug($slug);
    public  function update(Request $request);
    public  function destroy(Request $request);
}
