<?php
namespace App\Http\Interfaces;

use Illuminate\Http\Request;

interface TimelineInterface 
{
    public  function languages();
    public  function index();
    public  function store(Request $request);
    public  function edit($timeline);
    public  function update(Request $request);
    public  function destroy(Request $request);
}