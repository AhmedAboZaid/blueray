<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\AlbumInterface;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Resources\AlbumResource;
use App\Http\Resources\MainPageAlbumsResource;
use App\Http\Resources\MainPageResource;
use App\Http\Resources\SeoResource;
use App\Http\Resources\SingleAlbumResource;
use Exception;

class AlbumController extends ApiBaseController
{
    private $mainPageInterface;
    private $albumInterface;

    public function __construct(MainPageInterface $mainPageInterface, AlbumInterface $albumInterface)
    {
        $this->mainPageInterface = $mainPageInterface;
        $this->albumInterface = $albumInterface;
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/albums",
     *     description="album page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="album page")
     * )
     */
    public function index()
    {
        try {
            $mainPage            = $this->mainPageInterface->first([['fixed_name', 'albums']]);
            $mainPage->albums    = AlbumResource::collection($this->albumInterface->index());
            $mainPage            = new MainPageAlbumsResource($mainPage);
            return $this->success($mainPage);
        } catch (Exception $e) {
            return $this->internalError($e->getMessage());
        }
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/albums/{slug}",
     *     description="single Album",
     *     @OA\Response(response="default", description="single Album page"),
     * @OA\Parameter(
     *          description="slug",
     *          in="path",
     *          name="slug",
     *          required=true,
     *          example="first",
     * ),
     *  @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     * )
    */
    public function show($slug)
    {
        try {
            $album = $this->albumInterface->showBySlug($slug);
            if(!$album){return $this->error('Not Found!!');}
            $album = new SingleAlbumResource($album);
            return $this->success($album);
        } catch (Exception $e) {
            return $this->internalError($e->getMessage());
        }
    }
}
