<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\AlbumInterface;
use App\Http\Interfaces\BlogInterface;
use App\Http\Interfaces\CityInterface;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Interfaces\PackageInterface;
use App\Http\Interfaces\SettingInterface;
use App\Http\Interfaces\SliderInterface;
use App\Http\Interfaces\TourismTypeInterface;
use App\Http\Interfaces\VideoInterface;
use App\Http\Resources\AlbumMainImageResource;
use App\Http\Resources\AlbumResource;
use App\Http\Resources\BlogResource;
use App\Http\Resources\CityResource;
use App\Http\Resources\MainPageResource;
use App\Http\Resources\PackageResource;
use App\Http\Resources\SeoResource;
use App\Http\Resources\SettingResource;
use App\Http\Resources\SliderResource;
use App\Http\Resources\TourismResource;
use App\Http\Resources\VideoResource;
use App\Models\City;
use App\Models\MainPage;
use App\Models\Setting;
use Exception;

class HomeController extends ApiBaseController
{
    private $sliderInterface;
    private $blogInterface;
    private $videoInterface;
    private $albumInterface;
    private $tourismTypeInterface;
    private $mainPageInterface;
    private $tripInterface;
    private $cityInterface;
    private $packageInterface;

    public function __construct(SliderInterface $sliderInterface ,VideoInterface $videoInterface ,BlogInterface $blogInterface,
                                AlbumInterface $albumInterface ,TourismTypeInterface $tourismTypeInterface ,
                                MainPageInterface $mainPageInterface, SettingInterface $settingInterface , CityInterface $cityInterface,
                                PackageInterface $packageInterface)
    {
        $this->sliderInterface      = $sliderInterface;
        $this->videoInterface       = $videoInterface;
        $this->blogInterface        = $blogInterface;
        $this->albumInterface       = $albumInterface;
        $this->tourismTypeInterface = $tourismTypeInterface;
        $this->mainPageInterface    = $mainPageInterface;
        $this->settingInterface     = $settingInterface;
        $this->cityInterface        = $cityInterface;
        $this->packageInterface     = $packageInterface;
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}",
     *     description="home page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="home page")
     * )
    */
    public function index()
    {
        $about=new MainPageResource($this->mainPageInterface->first([['fixed_name' , 'about']]));
        $about=[
            'description'=>$about->description,
            'image_url'=>asset('uploads/main-pages/'.@$about->image),
            'image_alt'=>$about->alt,
        ];
        try
        {
            $data["image_url"]      = asset("uploads/settings/".Setting::where('key','logo')->first()?->value);
            $data["image_alt"]      = Setting::where('key','alt')->first()?->value;
            $data['meta'] =[
                "meta_title"        => Setting::where('key','meta_title')->first()?->value ,
                "meta_description"  => Setting::where('key','meta_description')->first()?->value ,
                "meta_keyword"      => Setting::where('key','meta_keywords')->first()?->value ,
                "slug"              => 'home' ,
            ];
            $data['offers_sliders']                   = SliderResource        ::collection($this->sliderInterface->index());
            $data['about_section']                    = $about;

            $data['destination_section']['packages']  = PackageResource       ::collection($this->packageInterface->index());
            $data['destination_section']['countries'] = CityResource          ::collection($this->cityInterface->get_all(['parent_id' => null]));

            $data['tourisms_section']['tourisms']     = TourismResource       ::collection($this->tourismTypeInterface->index());

            $data['gallery_section']['description']   = $this->mainPageInterface->first([['fixed_name' , 'albums'] ])->description;
            $data['gallery_section']['images']        = AlbumMainImageResource::collection($this->albumInterface->index());

            $data['blogs']['description']   = $this->mainPageInterface->first([['fixed_name' , 'blogs'] ])->description;
            $data['blogs']['blogs']                   = BlogResource          ::collection($this->blogInterface->index());
            $data['video_section']['description']   = $this->mainPageInterface->first([['fixed_name' , 'videos'] ])->description;
            $data['video_section']['videos']                    = VideoResource         ::collection($this->videoInterface->index());

            return $this->success($data);
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }
}

