<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Interfaces\VideoInterface;
use App\Http\Resources\MainPageResource;
use App\Http\Resources\MainPageVideosResource;
use App\Http\Resources\SeoResource;
use App\Http\Resources\VideoResource;
use Exception;

class VideoController extends ApiBaseController
{
    private $videoInterface;
    public function __construct(MainPageInterface $mainPageInterface,VideoInterface $videoInterface)
    {
        $this->mainPageInterface = $mainPageInterface;
        $this->videoInterface = $videoInterface;
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/videos",
     *     description="videos page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="videos page")
     * )
    */
    public function index()
    {
        try
        {
            $mainPage         = $this->mainPageInterface->first([['fixed_name', 'videos']]);
            $mainPage->videos = VideoResource::collection($this->videoInterface->index());
            return $this->success(new MainPageVideosResource($mainPage));
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }
}
