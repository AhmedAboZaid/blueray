<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\CityInterface;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Interfaces\PackageInterface;
use App\Http\Resources\DestinationResource;
use App\Http\Resources\MainPageMediaResource;
use App\Http\Resources\MainPageResourceV2;
use App\Models\Package;

class MainPageController extends ApiBaseController
{

    public function __construct(MainPageInterface $MainPageInterface , PackageInterface $PackageInterface , CityInterface $CityInterface)
    {
        $this->MainPageInterface = $MainPageInterface;
        $this->PackageInterface = $PackageInterface;
        $this->CityInterface = $CityInterface;
    }


    /**
     * @OA\Get(
     *     path="/api/{lang}/ticketing",
     *     description="get ticketing Page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="ticketing Page")
     * )
    */
    public function ticketing()
    {
        $main_page = $this->MainPageInterface->first(whereClause:[['fixed_name' , 'ticketing']]);
        if(!$main_page){return $this->error('Not Found!!');}
        return $this->success( new MainPageResourceV2($main_page ) );
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/transportation",
     *     description="get transportation Page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="transportation Page")
     * )
    */
    public function transportation()
    {
        $main_page = $this->MainPageInterface->first(whereClause:[['fixed_name' , 'transportation']]);
        if(!$main_page){return $this->error('Not Found!!');}
        return $this->success( new MainPageResourceV2($main_page ) );
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/media",
     *     description="get media Page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="media Page")
     * )
    */
    public function media()
    {
        $main_page = $this->MainPageInterface->first(whereClause:[['fixed_name' , 'media']] , with:['childs']);
        if(!$main_page){return $this->error('Not Found!!');}
        return $this->success( new MainPageMediaResource($main_page ) );
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/destination",
     *     description="get destination with packages",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="destination Page")
     * )
    */
    public function destination()
    {
        $main_page              = $this->MainPageInterface->first(whereClause:[['fixed_name' , 'destination']] );
        if(!$main_page){return $this->error('Not Found!!');}
        $main_page->packages    = $this->PackageInterface->index();
        $main_page->cities      = $this->CityInterface->get_all([['parent_id' , null]]);
        return $this->success( new DestinationResource($main_page ) );
    }


}
