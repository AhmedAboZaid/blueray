<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Interfaces\ServiceInterface;
use App\Http\Resources\MainPageServicesResource;
use App\Http\Resources\SeoResource;
use App\Http\Resources\ServiceResource;
use Exception;

class ServiceController extends ApiBaseController
{
    public $serviceInterface;
    public function __construct(MainPageInterface $mainPageInterface, ServiceInterface $serviceInterface)
    {
        $this->mainPageInterface = $mainPageInterface;
        $this->serviceInterface = $serviceInterface;
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/services",
     *     description="get services Page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="services Page with childs")
     * )
    */
    public function index()
    {
        try {
            $mainPage            = $this->mainPageInterface->first([['fixed_name', 'services'], ['parent_id', null]] , with:['childs']);
            $mainPage->cards     = ServiceResource::collection($this->serviceInterface->index());
            $mainPage            = new MainPageServicesResource($mainPage);

            return $this->success($mainPage);
        } catch (Exception $e) {
            return $this->internalError($e->getMessage());
        }
    }
}
