<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\LayoutInterface;

class LayoutbarController extends ApiBaseController
{
    public function __construct(LayoutInterface $LayoutInterface)
    {
        $this->LayoutInterface = $LayoutInterface;
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/navbar",
     *     description="get Navbar links and languages",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="Navbar links and languages")
     * )
    */
    public function navbar()
    {
        return $this->success($this->LayoutInterface->navbar());
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/footer",
     *     description="get footer links and social madia",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="Navbar links and social madia")
     * )
    */
    public function footer()
    {
        return $this->success($this->LayoutInterface->footer());
    }
}
