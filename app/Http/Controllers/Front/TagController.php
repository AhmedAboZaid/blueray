<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\TagInterface;
use App\Http\Resources\TagResource;
use Exception;

class TagController extends ApiBaseController
{

    private $tagInterface;
    public function __construct(TagInterface $tagInterface)
    {
        $this->tagInterface = $tagInterface;
    }

    public function index()
    {
        try
        {
            return $this->success(TagResource::collection($this->tagInterface->index()));
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }
}
