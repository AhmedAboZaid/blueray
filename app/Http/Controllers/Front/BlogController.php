<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\BlogInterface;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Resources\BlogResource;
use App\Http\Resources\LatestBlogResoucre;
use App\Http\Resources\MainPageAlbumsResource;
use App\Http\Resources\MainPageBlogsResource;
use App\Http\Resources\MainPageResource;
use App\Http\Resources\SeoResource;
use App\Http\Resources\SingleBlogResource;
use App\Models\Blog;
use Exception;

class BlogController extends ApiBaseController
{
    private $mainPageInterface;
    private $blogInterface;

    public function __construct(MainPageInterface $mainPageInterface , BlogInterface $blogInterface)
    {
        $this->mainPageInterface = $mainPageInterface;
        $this->blogInterface = $blogInterface;
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/blogs",
     *     description="blog page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="blog page")
     * )
    */
    public function index()
    {
        try
        {
            $mainPage            =  $this->mainPageInterface->first([['fixed_name' , 'blogs']]);
            $mainPage->blogs     =  BlogResource::collection($this->blogInterface->index());
            $mainPage            =  new MainPageAlbumsResource($mainPage);
            return $this->success($mainPage);
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/blogs/{slug}",
     *     description="single blog",
     *     @OA\Response(response="default", description="single blog page"),
     * @OA\Parameter(
     *          description="slug",
     *          in="path",
     *          name="slug",
     *          required=true,
     *          example="first-blog",
     * ),
     *  @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     * )
    */
    public function show($slug)
    {
        try
        {
            $blog                   = $this->blogInterface->showBySlug($slug);
            if(!$blog){return $this->error('Not Found!!');}
            $blog->popular_posts    =  BlogResource::collection($this->blogInterface->random(5 , [['id','!=', $blog->id]]));
            $blog                   = new SingleBlogResource($blog);
            return $this->success($blog);
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }
}
