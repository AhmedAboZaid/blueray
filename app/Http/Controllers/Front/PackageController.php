<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\PackageInterface;
use App\Http\Resources\PackageResource;
use App\Http\Resources\PackageResourceV2;
use App\Http\Resources\TourismType_PackagesResource;
use App\Http\Resources\TripResource;
use Exception;

class PackageController extends ApiBaseController
{
    private $PackageInterface;
    public function __construct(PackageInterface $PackageInterface)
    {
        $this->PackageInterface = $PackageInterface;
    }

    public function index()
    {
        try
        {
            return $this->success(PackageResource::collection($this->PackageInterface->index()));
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/packages/{slug}",
     *     description="package details",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     * @OA\Parameter(
     *          description="packages slug",
     *          in="path",
     *          name="slug",
     *          required=true,
     *          example="religious-tourism",
     * ),
     *     @OA\Response(response="default", description="package Full details with popular packages")
     * )
    */
    public function package($slug)
    {
        try
        {
            $tourism = $this->PackageInterface->showBySlug($slug , with:['seo',"tourism_type","city","attachments","image","facilitiesInclude","facilitiesExclude","travelPlans"]);
            if(!$tourism){return $this->error('Not Found!!');}
            $tourism = new PackageResourceV2($tourism);
            return $this->success($tourism);
        }
        catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }

}
