<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Resources\MainPageResource;
use App\Http\Resources\SeoResource;
use Exception;
use Illuminate\Http\Request;

class MediaController extends ApiBaseController
{
    public function __construct(MainPageInterface $mainPageInterface, )
    {
        $this->mainPageInterface = $mainPageInterface;
    }
    public function index()
    {
        try {

            $mainPage            = new MainPageResource($this->mainPageInterface->first([['fixed_name', 'Media Center'], ['parent_id', null]]));
            $child_pages         =  MainPageResource::collection($this->mainPageInterface->all(['parent_id'=>2 ]));
            $media=[];
            foreach($child_pages as $child){
                $media[]=[
                        "id"        => $child->id,
                        "title"     => $child->title,
                        "image_url" => asset('uploads/main-pages/'.$child->image_path),
                        "image_alt" => $child->alt,
                        "slug"      => $child->seo->slug
                ];
            }
            // $services            = ServiceResource::collection($this->serviceInterface->index());
            $data['image_url']   = asset('uploads/main-pages/'.$mainPage->image);
            $data['image_alt']   = $mainPage->alt;
            $data['meta']        = new SeoResource($mainPage->seo);
            $data['title']       = $mainPage->title;
            $data['media']       = $media;

            return $this->success($data);
        } catch (Exception $e) {
            return $this->internalError($e->getMessage());
        }
    }
}


