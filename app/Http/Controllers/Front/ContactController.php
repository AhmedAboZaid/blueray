<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\BranchInterface;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Requests\BranchRequest;
use App\Http\Resources\BranchResource;
use App\Http\Resources\BranchSocialResource;
use App\Http\Resources\MainPageContactResource;
use App\Http\Resources\MainPageResource;
use App\Http\Resources\SeoResource;
use App\Models\Setting;
use Exception;

class ContactController extends ApiBaseController
{

    public function __construct(MainPageInterface $mainPageInterface, BranchInterface $branchInterface)
    {
        $this->mainPageInterface = $mainPageInterface;
        $this->branchInterface = $branchInterface;
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/contact-us",
     *     description="contact page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="contact page")
     * )
    */
    public function index()
    {
        try
        {
            // return $this->branchInterface->index();
            $mainPage          = $this->mainPageInterface->first([['fixed_name' , 'contact']]);
            if(!$mainPage){return $this->error('Not Found!!');}
            $mainPage->offices = BranchResource::collection($this->branchInterface->index());
            $mainPage->map     = Setting::where('key','map')->first()?->translate('en')->value;
            $mainPage->from_day     = Setting::where('key','from_day')->first()?->value;
            $mainPage->to_day     = Setting::where('key','to_day')->first()?->value;
            $mainPage->close_day     = Setting::where('key','close_day')->first()?->value;
            $mainPage->from_hour     = Setting::where('key','from_hour')->first()?->translate('en')->value;
            $mainPage->to_hour     = Setting::where('key','to_hour')->first()?->translate('en')->value;
            $data              = new MainPageContactResource($mainPage);
            return $this->success($data);
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }
}
