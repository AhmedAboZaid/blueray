<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\SettingInterface;
use App\Http\Resources\SettingResource;
use Exception;

class SettingController extends ApiBaseController
{
    private $settingInterface;

    public function __construct(SettingInterface $settingInterface)
    {
        $this->settingInterface = $settingInterface;
    }


    public function index()
    {
        try {
            return new SettingResource($this->settingInterface->index());
        } catch (Exception $e) {
            return $this->internalError($e->getMessage());
        }
    }
}
