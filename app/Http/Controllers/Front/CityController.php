<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\CityInterface;
use App\Http\Resources\CityResource;
use Exception;
use Illuminate\Http\Request;

class CityController extends ApiBaseController
{
    private $cityInterface;
    public function __construct(CityInterface $cityInterface)
    {
        $this->cityInterface = $cityInterface;
    }

  
    public function index()
    {
        try
        {
            return $this->success(CityResource::collection($this->cityInterface->index()));
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }
}
