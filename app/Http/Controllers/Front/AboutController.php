<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Interfaces\SectionInterface;
use App\Http\Interfaces\TeamMemberInterface;
use App\Http\Resources\AboutResource;
use App\Http\Resources\MainPageResource;
use App\Http\Resources\MainPagesResource;
use App\Http\Resources\SectionResource;
use App\Http\Resources\SeoResource;
use App\Http\Resources\TeamResource;
use Exception;

class AboutController extends ApiBaseController
{
    private $mainPageInterface;
    private $sectionInterface;
    private $teamMemberInterface;

    public function __construct(MainPageInterface $mainPageInterface ,SectionInterface $sectionInterface, TeamMemberInterface $teamMemberInterface)
    {
        $this->mainPageInterface = $mainPageInterface;
        $this->sectionInterface = $sectionInterface;
        $this->teamMemberInterface = $teamMemberInterface;
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/about",
     *     description="about page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="about page")
     * )
    */
    public function index()
    {
        try
        {
            // $mainPage->sections = SectionResource::collection($this->sectionInterface->index());
            $mainPage           = $this->mainPageInterface->first([['fixed_name' , 'About']],['childs']);
            $mainPage->team     = TeamResource   ::collection($this->teamMemberInterface->index());
             $data = new AboutResource($mainPage);
            return $this->success($data);
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }
}
