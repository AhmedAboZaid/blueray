<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Interfaces\TourismTypeInterface;
use App\Http\Resources\TourismResource;
use App\Http\Resources\TourismType_PackagesResource;
use App\Http\Resources\TourismTypeMainPageResource;
use Exception;

class TourismTypeController extends ApiBaseController
{
    private $tourismTypeInterface;
    public function __construct(TourismTypeInterface $tourismTypeInterface , MainPageInterface $MainPageInterface)
    {
        $this->tourismTypeInterface = $tourismTypeInterface;
        $this->MainPageInterface = $MainPageInterface;
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/tourisms",
     *     description="tourism-type page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Response(response="default", description="tourisms page with tourism types")
     * )
    */

    public function index()
    {
        try
        {
            // get tourisms  main page
            $main_page = $this->MainPageInterface->first(whereClause:[['fixed_name' , 'tourisms']]);
            $tourisms = TourismResource::collection($this->tourismTypeInterface->index());
            $main_page->tourisms = $tourisms;
            return $this->success(new TourismTypeMainPageResource($main_page));
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }

    /**
     * @OA\Get(
     *     path="/api/{lang}/tourisms/{slug}",
     *     description="tourism with packages page",
     *     @OA\Parameter(
     *          description="lang",
     *          in="path",
     *          name="lang",
     *          required=true,
     *          example="en",
     * ),
     *     @OA\Parameter(
     *          description="tourism type slug",
     *          in="path",
     *          name="slug",
     *          required=true,
     *          example="religious-tourism",
     * ),
     *     @OA\Response(response="default", description="tourism-type page")
     * )
    */
    public function package($slug)
    {
        try
        {
            $tourism = $this->tourismTypeInterface->showBySlug($slug , with:['packages']);
            if(!$tourism){return $this->error('Not Found!!');}
            $tourism = new TourismType_PackagesResource($tourism);
            return $this->success($tourism);
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }


}
