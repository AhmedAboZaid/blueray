<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Controllers\Controller;
use App\Http\Interfaces\SocialMediaInterface;
use App\Http\Resources\SocialMediaResource;
use Exception;
use Illuminate\Http\Request;

class SocialMediaController extends ApiBaseController
{
    private $socialMediaInterface;
    public function __construct(SocialMediaInterface $socialMediaInterface)
    {
        $this->socialMediaInterface = $socialMediaInterface;
    }

    public function index()
    {
        try
        {
            return $this->success(SocialMediaResource::collection($this->socialMediaInterface->index()));
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }

}
