<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\FormInterface;
use App\Http\Requests\StoreContact;
use App\Http\Requests\StoreBookForm;
use App\Http\Requests\StoreTicketForm;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\DataTables;

class FormController extends ApiBaseController
{
    private $formInterface;

    public function __construct(FormInterface $formInterface)
    {
        $this->formInterface = $formInterface;
    }

    /**
     * @OA\Post(
     * path="/api/contact",
     * summary="Contact Request",
     * description="Contact Request",
     * operationId="Client Request",
     * tags={"Forms"},
     * @OA\RequestBody(
     * required=true,
     * @OA\JsonContent(
     *   @OA\Property(property="name", type="string", format="text", example="Client"),
     *   @OA\Property(property="phone", type="string", format="text", example="123456789"),
     *   @OA\Property(property="email", type="email", format="text", example="client@ileadco.com"),
     *   @OA\Property(property="message", type="string", format="text", example="message"),
     *   required={"email","phone","name"}
     *   ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Contact Created successfully",
     *    @OA\JsonContent(
     *    )
     * ),
     * @OA\Response(
     * response=422,
     * description="Form Inputs Must be String",
     * @OA\JsonContent(
     *    @OA\Property(property="message", type="string", example="Sorry, Form Inputs Required. Please try again")
     *   )
     *  )
     * )
     **
    */

    public function contact(StoreContact $request)
    {
        try
        {
           $result = $this->formInterface->contact($request);
           return $this->success( $result );
        }catch(Exception $ex){
            return $this->internalError('Error Occur Try Again');
        }
    }


   /**
 * @OA\Post(
 * path="/api/ticket",
 * summary="ticket Request",
 * description="ticketing Request ",
 * operationId="ticket Request",
 * tags={"Forms"},
 * @OA\RequestBody(
 * required=true,
 * @OA\JsonContent(
 *   @OA\Property(property="ticket_type", type="string", format="text", example="round"),
 *   @OA\Property(property="destination_from", type="string", format="text", example="cairo"),
 *   @OA\Property(property="destination_to", type="string", format="text", example="alex"),
 *   @OA\Property(property="from_date", type="date", format="text", example="2023-02-02"),
 *   @OA\Property(property="to_date", type="date", format="text", example="2023-02-03"),
 *               @OA\Property(property="persons", type="array",
 *               example={
 *                  {
 *                   "name"  : "bassant",
 *                   "birthday" : "1997-02-03",
 *                 },
 *                  {
 *                   "name"  : "menna",
 *                   "birthday" : "1997-02-03",
 *                 },
 *                },
 *                  @OA\Items(
 *                      @OA\Property(property="name" , type="string",  example=""),
 *                      @OA\Property(property="birthday" , type="date", example=""),
 *                  ),
 *
 *               ),
 * ),
 * ),
 * @OA\Response(
 *    response=200,
 *    description="Ticketing Created successfully",
 *    @OA\JsonContent(
 *    )
 * ),
 * @OA\Response(
 * response=422,
 * description="Form Inputs Must be String",
 * @OA\JsonContent(
 *    @OA\Property(property="message", type="string", example="Sorry, Form Inputs Required. Please try again")
 *   )
 *  )
 * )
 **
*/
    public function bookForm(StoreBookForm $request)
    {
        try
        {
           $result = $this->formInterface->bookForm($request);
           return $this->success( $result );
        }catch(Exception $ex){
            return $this->internalError('Error Occur Try Again');
        }
    }


    /**
     * @OA\Post(
     * path="/api/booking",
     * summary="booking Request",
     * description="book package Request ",
     * operationId="booking Request",
     * tags={"Forms"},
     * @OA\RequestBody(
     * required=true,
     * @OA\JsonContent(
     *   @OA\Property(property="name", type="string", format="text", example="bassant"),
     *   @OA\Property(property="code", type="string", format="text", example="+2"),
     *   @OA\Property(property="phone", type="string", format="text", example="123456789"),
     *   @OA\Property(property="email", type="email", format="text", example="book@ileadco.com"),
     *   @OA\Property(property="package_id", type="integer", format="text", example=1),
     *   @OA\Property(property="child", type="integer", format="text", example=2),
     *   @OA\Property(property="adult", type="integer", format="text", example=3),
     *   @OA\Property(property="message", type="string", format="text", example="message"),
     *   required={"email","phone","name","package_id" }
     *   ),
     * ),
     * @OA\Response(
     *    response=200,
     *    description="Booking Created successfully",
     *    @OA\JsonContent(
     *    )
     * ),
     * @OA\Response(
     * response=422,
     * description="Form Inputs Must be String",
     * @OA\JsonContent(
     *    @OA\Property(property="message", type="string", example="Sorry, Form Inputs Required. Please try again")
     *   )
     *  )
     * )
     **
    */

    public function ticketForm(StoreTicketForm $request)
    {
        // dd($request->all());
        try
        {
           $result = $this->formInterface->ticketForm($request);
           return $this->success( $result );
        }
        catch(Exception $ex){
            return $this->internalError('Error Occur Try Again');
        }
    }

}

