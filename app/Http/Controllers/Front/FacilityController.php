<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\ApiBaseController;
use App\Http\Interfaces\FacilityInterface;
use App\Http\Resources\FacilityResource;
use Exception;

class FacilityController extends ApiBaseController
{
    private $facilityInterface;
    public function __construct(FacilityInterface $facilityInterface)
    {
        $this->facilityInterface = $facilityInterface;
    }


    public function index()
    {
        try
        {
            return $this->success(FacilityResource::collection($this->facilityInterface->index()));
        }catch(Exception $e){
            return $this->internalError($e->getMessage());
        }
    }
}
