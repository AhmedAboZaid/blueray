<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\BlogInterface;
use App\Http\Requests\BlogRequest;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    private $blogInterface;

    public function __construct(BlogInterface $blogInterface)
    {
        $this->blogInterface = $blogInterface;
    }

    public function index()
    {
        $blogs = $this->blogInterface->index();
        return view('admin.blogs.index',compact('blogs'));
    }

    public function create()
    {
        $languages = $this->blogInterface->languages();
        $tags = $this->blogInterface->tags();
        return view('admin.blogs.create',compact('languages','tags'));
    }

    public function store(BlogRequest $blogRequest)//
    {
        $this->blogInterface->store($blogRequest);
        return redirect()->route('blogs.index')->with(['success' => 'Blog Add Successfully']);
    }

    public function edit($blog)
    {
       $blog = $this->blogInterface->edit($blog);
        $languages = $this->blogInterface->languages();
        $tags = $this->blogInterface->tags();
        return view('admin.blogs.edit',compact('blog','languages','tags'));
    }

    public function update(BlogRequest $blogRequest)
    {
        $this->blogInterface->update($blogRequest);
        return  redirect()->route('blogs.index')->with(['success' => 'Blog Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->blogInterface->destroy($request);
        return  redirect()->route('blogs.index')->with(['success' => 'Blog Delete Successfully']);
    }

}
