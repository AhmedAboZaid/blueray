<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\FacilityInterface;
use App\Http\Requests\FacilityRequest;
use Illuminate\Http\Request;

class FacilityController extends Controller
{
    private $facilityInterface ;
    public function __construct(FacilityInterface $facilityInterface)
    {
        $this->facilityInterface = $facilityInterface; 
    }

    public function index()
    {
        $facilities = $this->facilityInterface->index();
        return view('admin.facility.index',compact('facilities'));
    }

    public function create()
    {
        $languages = $this->facilityInterface->languages();
        return view('admin.facility.create',compact('languages'));
    }

    public function store(FacilityRequest $facilityRequest)
    {
        $this->facilityInterface->store($facilityRequest);  
        return redirect()->route('facility.index')->with(['success' => 'Facility Add Successfully']);
    }

    public function edit($facility)
    {
        $facility = $this->facilityInterface->edit($facility);
        $languages = $this->facilityInterface->languages();
        return view('admin.facility.edit',compact('facility','languages'));
    }

    public function update(FacilityRequest $facilityRequest)
    {
        $this->facilityInterface->update($facilityRequest);
        return  redirect()->route('facility.index')->with(['success' => 'Facility Update Successfully']);
    }
    
    public function destroy(Request $request)
    {
        return  $this->facilityInterface->destroy($request);
        return  redirect()->route('facility.index')->with(['success' => 'Facility Delete Successfully']);
    }
}
