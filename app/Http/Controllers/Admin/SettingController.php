<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\SettingInterface;
use App\Http\Requests\SettingRequest;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    private $settingInterface;

    public function __construct(SettingInterface $settingInterface)
    {
        $this->settingInterface = $settingInterface;  
    }

    public function index()
    {
        $settings = $this->settingInterface->index();
        return view('admin.settings.index',compact('settings'));
    }

    public function edit()
    {
        $settings = $this->settingInterface->index();
        $languages = $this->settingInterface->languages();
        return view('admin.settings.edit',compact('settings','languages'));
    }

    public function update(SettingRequest $settingRequest)
    {
        $this->settingInterface->update($settingRequest);
        return  redirect()->route('settings.edit')->with(['success' => 'Setting Update Successfully']);
    }
}
