<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\FaqInterface;
use App\Http\Requests\FAQRequest;
use Illuminate\Http\Request;

class FAQController extends Controller
{
    private $faqInterface;

    public function __construct(FaqInterface $faqInterface)
    {
        $this->faqInterface = $faqInterface;
    }

    public function index()
    {
        $faqs = $this->faqInterface->index();
        return view('admin.faqs.index',compact('faqs'));
    }

    public function create()
    {
        $languages = $this->faqInterface->languages();
        return view('admin.faqs.create',compact('languages'));
    }

    public function store(FAQRequest $fAQRequest)
    {
        $this->faqInterface->store($fAQRequest);
        return redirect()->route('faqs.index')->with(['succes' => 'FAQS Add Successfuly']);
    }

    public function edit($faq)
    {
        $faq = $this->faqInterface->edit($faq);
        $languages = $this->faqInterface->languages();
        return view('admin.faqs.edit',compact('faq','languages'));
    }

    public function update(FAQRequest $fAQRequest)
    {
        $this->faqInterface->update($fAQRequest);
        return  redirect()->route('faqs.index')->with(['success' => 'FAQS Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->faqInterface->destroy($request);
        return  redirect()->route('faqs.index')->with(['success' => 'FAQS Delete Successfully']);
    }
}
