<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\TeamMemberInterface;
use App\Http\Requests\TeamMemberRequest;
use Illuminate\Http\Request;

class TeamMeamberController extends Controller
{
    private $teamMemberInterface;

    public function __construct(TeamMemberInterface $teamMemberInterface)
    {
        $this->teamMemberInterface = $teamMemberInterface ;

    }
    public function index()
    {
        $teamMembers = $this->teamMemberInterface->index();
        return view('admin.team_members.index',compact('teamMembers'));
    }

    public function create( )
    {
        $languages = $this->teamMemberInterface->languages();
        return view('admin.team_members.create',compact('languages'));
    }

    public function store(TeamMemberRequest $request)
    {
        $this->teamMemberInterface->store($request);
        return redirect()->route('team.members.index')->with(['success' => 'Member Add Successfully']);
    }

    public function edit($teamMember)
    {
        $teamMember = $this->teamMemberInterface->edit($teamMember);
        $languages = $this->teamMemberInterface->languages();
        return view('admin.team_members.edit',compact('teamMember','languages'));
    }

    public function update(TeamMemberRequest $request)
    {
        $this->teamMemberInterface->update($request);
        return  redirect()->route('team.members.index')->with(['success' => 'Member Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->teamMemberInterface->destroy($request);
        return  redirect()->route('team.members.index')->with(['success' => 'Member Delete Successfully']);
    }
}
