<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\ServiceInterface;
use App\Http\Requests\ServiceRequest;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    private $serviceInterface;

    public function __construct(ServiceInterface $serviceInterface)
    {
        $this->serviceInterface = $serviceInterface;   
    }

    public function index()
    {
        $services = $this->serviceInterface->index();
        return view('admin.services.index',compact('services'));
    }

    public function create()
    {
        $languages = $this->serviceInterface->languages();
        $tags = $this->serviceInterface->tags();
        return view('admin.services.create',compact('languages','tags'));
    }

    public function store(ServiceRequest $serviceRequest)
    {
        $this->serviceInterface->store($serviceRequest);
        return redirect()->route('services.index')->with(['success' => 'Service Add Successfully']);
    }

    public function edit($service)
    {
       $service = $this->serviceInterface->edit($service);
        $languages = $this->serviceInterface->languages();
        $tags = $this->serviceInterface->tags();
        return view('admin.services.edit',compact('service','languages','tags'));
    }

    public function update(ServiceRequest $serviceRequest)
    {
        $this->serviceInterface->update($serviceRequest);
        return  redirect()->route('services.index')->with(['success' => 'Service Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->serviceInterface->destroy($request);
        return  redirect()->route('services.index')->with(['success' => 'Service Delete Successfully']);
    }
}
