<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Interfaces\PackageInterface;
use App\Http\Requests\PackageRequest;

class PackageController extends Controller
{
    private $PackageInterface;

    public function __construct(PackageInterface $PackageInterface)
    {
        $this->PackageInterface = $PackageInterface;
    }

    public function index()
    {
        $packages = $this->PackageInterface->index();
        return view('admin.packages.index',compact('packages'));
    }

    public function create()
    {
        $languages      = $this->PackageInterface->languages();
        $facilities     = $this->PackageInterface->facilities();
        $countries      = $this->PackageInterface->cities(where:[['parent_id' , null]]);
        $tourism_types  = $this->PackageInterface->tourism_types();
        $tags           = $this->PackageInterface->tags();
        return view('admin.packages.create', compact('languages','tags' , 'facilities' ,'countries' ,'tourism_types' ));
    }

    public function cities($parent_id=null)
    {
        return $this->PackageInterface->cities(where:[['parent_id' , $parent_id]]);
    }
    public function store(PackageRequest $PackageRequest)
    {
        $this->PackageInterface->store($PackageRequest);
        return redirect()->route('packages.index')->with(['success' => 'Package Added Successfully']);
    }

    public function edit($package)
    {
        $package        = $this->PackageInterface->edit($package);
        $languages      = $this->PackageInterface->languages();
        $facilities     = $this->PackageInterface->facilities();
        $countries      = $this->PackageInterface->cities(where:[['parent_id' , null]]);
        $cities         = $package->country?->cities;
        $tourism_types  = $this->PackageInterface->tourism_types();
        $tags           = $this->PackageInterface->tags();

        return view('admin.packages.edit',compact('package','languages','tags' , 'facilities' ,'cities' ,'tourism_types' ,'countries' ));
    }

    public function update(PackageRequest $PackageRequest)//
    {
        $this->PackageInterface->update($PackageRequest);
        return redirect()->route('packages.index')->with(['success' => 'Package Updated Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->PackageInterface->destroy($request);
        return  redirect()->route('packages.index')->with(['success' => 'Package Delete Successfully']);
    }
}
