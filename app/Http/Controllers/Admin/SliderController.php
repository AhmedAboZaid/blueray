<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\SliderInterface;
use App\Http\Requests\SliderRequest;
use App\Models\Language;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    private $sliderInterface;

    public function __construct(SliderInterface $sliderInterface)
    {
        $this->sliderInterface = $sliderInterface ;
        
    }
    public function index()
    {
        $sliders = $this->sliderInterface->index();
        return view('admin.sliders.index',compact('sliders'));
    }

    public function create( )
    {
        $languages = $this->sliderInterface->languages();
        return view('admin.sliders.create',compact('languages'));
    }

    public function store(SliderRequest $request)
    {
        $this->sliderInterface->store($request);  
        return redirect()->route('sliders.index')->with(['success'=>'Slider Add Successfully']);
    }

    public function edit($slider)
    {
        $slider = $this->sliderInterface->edit($slider);
        $languages = $this->sliderInterface->languages();
        return view('admin.sliders.edit',compact('slider','languages'));
    }

    public function update(SliderRequest $request)
    {
        $this->sliderInterface->update($request);
        return  redirect()->route('sliders.index')->with(['success' => 'Slider Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->sliderInterface->destroy($request);
        return  redirect()->route('sliders.index')->with(['success' => 'Slider Delete Successfully']);
    }
}
