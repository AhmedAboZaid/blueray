<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\LanguageInterface;
use App\Http\Requests\StoreLanguage;
use Illuminate\Http\Request;

class LanguageController extends Controller
{
    private $languageInterface;

    public function __construct(LanguageInterface $languageInterface)
    {
        $this->languageInterface = $languageInterface;
    }

    public function index()
    {
        $languages =  $this->languageInterface->index();
        return view('admin.languages.index',compact('languages'));
    }

    public function create( )
    {
        return view('admin.languages.create');
    }

    public function store(StoreLanguage $storeLanguage)
    {
        $this->languageInterface->store($storeLanguage);
        return  redirect()->route('languages.index')->with(['success' => 'Labguage Add Successfully']);
    }

    public function edit($language)
    {
        $language = $this->languageInterface->edit($language);
        return view('admin.languages.edit',compact('language'));
    }

    public function update(StoreLanguage $storeLanguage)
    {
        $this->languageInterface->update($storeLanguage);
        return  redirect()->route('languages.index')->with(['success' => 'Labguage Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->languageInterface->destroy($request);
        return  redirect()->route('languages.index')->with(['success' => 'Labguage Delete Successfully']);
    }

    public function changeStatus(Request $request)
    {
        return  $this->languageInterface->changeStatus($request);
    }

}
