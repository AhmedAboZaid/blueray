<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\AlbumInterface;
use App\Http\Requests\AlbumRequest;
use Illuminate\Http\Request;

class AlbumController extends Controller
{
    private $albumInterface;

    public function __construct(AlbumInterface $albumInterface)
    {
        $this->albumInterface = $albumInterface;   
    }

    public function index()
    {
        $albums = $this->albumInterface->index();
        return view('admin.albums.index',compact('albums'));
    }

    public function create()
    {
        $languages = $this->albumInterface->languages();
        return view('admin.albums.create',compact('languages'));
    }

    public function store(AlbumRequest $albumRequest)
    {
        $this->albumInterface->store($albumRequest);
        return redirect()->route('albums.index')->with(['success' => 'Album Add Successfully']);
    }

    public function edit($album)
    {
       $album = $this->albumInterface->edit($album);
        $languages = $this->albumInterface->languages();
        return view('admin.albums.edit',compact('album','languages'));
    }

    public function update(AlbumRequest $albumRequest)
    {
        $this->albumInterface->update($albumRequest);
        return  redirect()->route('albums.index')->with(['success' => 'Album Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->albumInterface->destroy($request);
        return  redirect()->route('albums.index')->with(['success' => 'Album Delete Successfully']);
    }
}
