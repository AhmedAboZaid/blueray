<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\SectionInterface;
use App\Http\Requests\MainPageRequest;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    
    private $sectionInterface;

    public function __construct(SectionInterface $sectionInterface)
    {
        $this->sectionInterface = $sectionInterface;  
    }

    public function index()
    {
        $sections = $this->sectionInterface->index();
        return view('admin.sections.index',compact('sections'));
    }
    public function create()
    {
        $languages = $this->sectionInterface->languages();
        return view('admin.sections.create',compact('languages'));
    }

    public function store(MainPageRequest $request)
    {
        $this->sectionInterface->store($request);
        return  redirect()->route('section.index')->with(['success' => 'Section Add Successfully']);
    }

    public function edit($section)
    {
        $section = $this->sectionInterface->edit($section);
        $languages = $this->sectionInterface->languages();
        return view('admin.sections.edit',compact('section','languages'));
    }

    public function update(MainPageRequest $request)
    {
        $this->sectionInterface->update($request);
        return  redirect()->route('section.index')->with(['success' => 'Section Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return $this->sectionInterface->destroy($request);
        return  redirect()->route('section.index')->with(['success' => 'Section Delete Successfully']);
    }
}
