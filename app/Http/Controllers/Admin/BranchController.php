<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\BranchInterface;
use App\Http\Requests\BranchRequest;
use App\Models\Branch;
use Illuminate\Http\Request;

class BranchController extends Controller
{
    private $branchInterface;
    public function __construct(BranchInterface $branchInterface){
        $this->branchInterface = $branchInterface;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = $this->branchInterface->index();
        return view('admin.branch.index',compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = $this->branchInterface->languages();
        return view('admin.branch.create',compact('languages'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BranchRequest $request)
    {
        $this->branchInterface->store($request);
        return redirect()->route('branch.index')->with(['success' => 'Branch Added Successfully']);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function show(Branch $branch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function edit($branch)
    {
        $branch = $this->branchInterface->edit($branch);
        $languages = $this->branchInterface->languages();
        return view('admin.branch.edit',compact('branch','languages'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function update(BranchRequest $request)
    {
        
        $this->branchInterface->update($request);
        return  redirect()->route('branch.index')->with(['success' => 'branch Update Successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branch  $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        return  $this->branchInterface->destroy($request);
        return  redirect()->route('brnach.index')->with(['success' => 'Branch Delete Successfully']);
    }
}
