<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\MainPageInterface;
use App\Http\Requests\MainPageRequest;
use Illuminate\Http\Request;

class MainPageController extends Controller
{
    private $mainPageInterface;

    public function __construct(MainPageInterface $mainPageInterface)
    {
        $this->mainPageInterface = $mainPageInterface;
    }

    public function index()
    {
        $mainPages = $this->mainPageInterface->index();
        return view('admin.main_pages.index',compact('mainPages'));
    }

    public function edit($mainPage)
    {
        $mainPage = $this->mainPageInterface->edit($mainPage);
        $languages = $this->mainPageInterface->languages();
        return view('admin.main_pages.edit',compact('mainPage','languages'));
    }

    public function update(MainPageRequest $request)
    {
        $this->mainPageInterface->update($request);
        return  redirect()->route('main-pages.index')->with(['success' => 'Main Page Update Successfully']);
    }
}
