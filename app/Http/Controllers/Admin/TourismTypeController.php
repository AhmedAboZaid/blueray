<?php


namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\TourismTypeInterface;
use App\Http\Requests\TourismTypeRequest;
use App\Models\TourismType;
use Illuminate\Http\Request;

class TourismTypeController extends Controller
{
    
    private $tourismTypeInterface;

    public function __construct(TourismTypeInterface $tourismTypeInterface)
    {
        $this->tourismTypeInterface = $tourismTypeInterface;
    }

    public function index()
    {
        $tourismTypes = $this->tourismTypeInterface->index();
        return view('admin.tourism_type.index',compact('tourismTypes'));
    }

    public function create()
    {
        $languages = $this->tourismTypeInterface->languages();
        $tags = $this->tourismTypeInterface->tags();
        return view('admin.tourism_type.create',compact('languages','tags'));
    }

    public function store(TourismTypeRequest $tourismTypeRequest)
    {
        $this->tourismTypeInterface->store($tourismTypeRequest);
        return redirect()->route('tourism.type.index')->with(['success' => 'Tourism Type Added Successfully']);
    }

    public function edit($tourismType)
    {
       $tourismType = $this->tourismTypeInterface->edit($tourismType);
        $languages = $this->tourismTypeInterface->languages();
        $tags = $this->tourismTypeInterface->tags();
        return view('admin.tourism_type.edit',compact('tourismType','languages','tags'));
    }

    public function update(TourismTypeRequest $tourismTypeRequest)
    {
        $this->tourismTypeInterface->update($tourismTypeRequest);
        return redirect()->route('tourism.type.index')->with(['success' => 'Tourism Type Updated Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->tourismTypeInterface->destroy($request);
        return  redirect()->route('projects.index')->with(['success' => 'Project Delete Successfully']);
    }
}
