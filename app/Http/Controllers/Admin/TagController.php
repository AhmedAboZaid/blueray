<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\TagInterface;
use App\Http\Requests\TagRequest;
use Illuminate\Http\Request;

class TagController extends Controller
{
    private $tagInterface;

    public function __construct(TagInterface $tagInterface)
    {
        $this->tagInterface = $tagInterface ;
        
    }
    public function index()
    {
        $tags = $this->tagInterface->index();
        return view('admin.tags.index',compact('tags'));
    }

    public function create( )
    {
        $languages = $this->tagInterface->languages();
        return view('admin.tags.create',compact('languages'));
    }

    public function store(TagRequest $request)
    {
        $this->tagInterface->store($request);  
        return redirect()->route('tags.index')->with(['success' => 'Tag Add Successfully']);
    }

    public function edit($tag)
    {
        $tag = $this->tagInterface->edit($tag);
        $languages = $this->tagInterface->languages();
        return view('admin.tags.edit',compact('tag','languages'));
    }

    public function update(TagRequest $request)
    {
        $this->tagInterface->update($request);
        return  redirect()->route('tags.index')->with(['success' => 'Tag Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->tagInterface->destroy($request);
        return  redirect()->route('tags.index')->with(['success' => 'Tag Delete Successfully']);
    }
}
