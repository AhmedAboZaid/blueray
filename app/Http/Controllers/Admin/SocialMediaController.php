<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\SocialMediaInterface;
use App\Http\Requests\SocialMediaRequest;
use Illuminate\Http\Request;

class SocialMediaController extends Controller
{
    private $socialMediaInterface;

    public function __construct(SocialMediaInterface $socialMediaInterface)
    {
        $this->socialMediaInterface = $socialMediaInterface;
    }
    public function index()
    {
        $socialMedias = $this->socialMediaInterface->index();
        return view('admin.social_media.index', compact('socialMedias'));
    }

    public function create()
    {
        return view('admin.social_media.create');
    }

    public function store(SocialMediaRequest $socialMediaRequest)
    {
        $this->socialMediaInterface->store($socialMediaRequest);
        return  redirect()->route('social.media.index')->with(['success' => 'Social Media Add Successfully']);
    }

    public function edit($socialMedia)
    {
        $socialMedia = $this->socialMediaInterface->edit($socialMedia);
        return view('admin.social_media.edit',compact('socialMedia'));
    }

    public function update(socialMediaRequest $socialMediaRequest)
    {
        $this->socialMediaInterface->update($socialMediaRequest);
        return  redirect()->route('social.media.index')->with(['success' => 'Social Media Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->socialMediaInterface->destroy($request);
        return  redirect()->route('social.media.index')->with(['success' => 'Social Media Delete Successfully']);
    }


}
