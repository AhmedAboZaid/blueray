<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\TimelineInterface;
use App\Http\Requests\TimelineRequest;
use Illuminate\Http\Request;

class TimelineController extends Controller
{
    private $timelineInterface;

    public function __construct(TimelineInterface $timelineInterface)
    {
        $this->timelineInterface = $timelineInterface ;
        
    }

    public function index()
    {
        $timelines = $this->timelineInterface->index(); 
        return view('admin.timeline.index',compact('timelines'));
    }

    public function create( )
    {
        $languages = $this->timelineInterface->languages();
        return view('admin.timeline.create',compact('languages'));
    }

    public function store(TimelineRequest $request)
    {
        $this->timelineInterface->store($request);  
        return redirect()->route('timeline.index')->with(['success' => 'Timeline Add Successfully']);
    }

    public function edit($timeline)
    {
        $timeline = $this->timelineInterface->edit($timeline);
        $languages = $this->timelineInterface->languages();
        return view('admin.timeline.edit',compact('timeline','languages'));
    }

    public function update(TimelineRequest $request)
    {
        $this->timelineInterface->update($request);
        return  redirect()->route('timeline.index')->with(['success' => 'Timeline Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->timelineInterface->destroy($request);
        return  redirect()->route('timeline.index')->with(['success' => 'Timeline Delete Successfully']);
    }
}
