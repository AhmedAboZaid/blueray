<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\ProjectInterface;
use App\Http\Requests\ProjectRequest;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    private $projectInterface;

    public function __construct(ProjectInterface $projectInterface)
    {
        $this->projectInterface = $projectInterface;
    }

    public function index()
    {
        $projects = $this->projectInterface->index();
        return view('admin.projects.index',compact('projects'));
    }

    public function create()
    {
        $languages = $this->projectInterface->languages();
        $tags = $this->projectInterface->tags();
        return view('admin.projects.create',compact('languages','tags'));
    }

    public function store(ProjectRequest $projectRequest)//
    {
        $this->projectInterface->store($projectRequest);
        return redirect()->route('projects.index')->with(['success' => 'Project Added Successfully']);
    }

    public function edit($project)
    {
       $project = $this->projectInterface->edit($project);
        $languages = $this->projectInterface->languages();
        $tags = $this->projectInterface->tags();
        return view('admin.projects.edit',compact('project','languages','tags'));
    }

    public function update(ProjectRequest $projectRequest)//
    {
        $this->projectInterface->update($projectRequest);
        return redirect()->route('projects.index')->with(['success' => 'Project Updated Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->projectInterface->destroy($request);
        return  redirect()->route('projects.index')->with(['success' => 'Project Delete Successfully']);
    }

}
