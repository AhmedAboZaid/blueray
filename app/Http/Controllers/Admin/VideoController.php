<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\VideoInterface;
use App\Http\Requests\VideoRequest;
use Illuminate\Http\Request;

class VideoController extends Controller
{
    private $videoInterface;

    public function __construct(VideoInterface $videoInterface)
    {
        $this->videoInterface = $videoInterface;
    }

    public function index()
    {
        $videos = $this->videoInterface->index();
        return view('admin.videos.index',compact('videos'));
    }

    public function create()
    {
        $languages = $this->videoInterface->languages();
        return view('admin.videos.create',compact('languages'));
    }

    public function store(VideoRequest $videoRequest)
    {
        $this->videoInterface->store($videoRequest);
        return redirect()->route('videos.index')->with(['success' => 'Video Add Successfully']);
    }

    public function edit($video)
    {
       $video = $this->videoInterface->edit($video);
        $languages = $this->videoInterface->languages();
        return view('admin.videos.edit',compact('video','languages'));
    }

    public function update(VideoRequest $request)
    {
        $this->videoInterface->update($request);
        return  redirect()->route('videos.index')->with(['success' => 'Video Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->videoInterface->destroy($request);
        return  redirect()->route('videos.index')->with(['success' => 'Video Delete Successfully']);
    }
}
