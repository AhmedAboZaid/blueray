<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\CityInterface;
use App\Http\Requests\CityRequest;
use Illuminate\Http\Request;

class CityController extends Controller
{
    private $CityInterface;

    public function __construct(CityInterface $CityInterface)
    {
        $this->CityInterface = $CityInterface;
    }

    public function index()
    {
        $cities = $this->CityInterface->index();
        return view('admin.cities.index',compact('cities'));
    }

    public function create()
    {
        $languages = $this->CityInterface->languages();
        $tags = $this->CityInterface->tags();
        $countries = $this->CityInterface->get_all(['parent_id'=>null]);
        return view('admin.cities.create',compact('languages','tags','countries'));
    }

    public function store(CityRequest $CityRequest)//
    {
        $this->CityInterface->store($CityRequest);
        return redirect()->route('cities.index')->with(['success' => 'City Added Successfully']);
    }

    public function edit($City)
    {
       $city = $this->CityInterface->edit($City);
        $languages = $this->CityInterface->languages();
        $countries = $this->CityInterface->get_all(['parent_id'=>null]);
        $tags = $this->CityInterface->tags();
        return view('admin.cities.edit',compact('city','languages','tags','countries'));
    }

    public function update(CityRequest $CityRequest)//
    {

        $this->CityInterface->update($CityRequest);
        return redirect()->route('cities.index')->with(['success' => 'City Updated Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->CityInterface->destroy($request);
        return  redirect()->route('cities.index')->with(['success' => 'City Delete Successfully']);
    }
}
