<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Interfaces\ClientInterface;
use App\Http\Requests\ClientRequest;
use Illuminate\Http\Request;

class ClientController extends Controller
{
    private $clientInterface;

    public function __construct(ClientInterface $clientInterface)
    {
        $this->clientInterface = $clientInterface ;
        
    }
    public function index()
    {
        $clients = $this->clientInterface->index();
        return view('admin.clients.index',compact('clients'));
    }

    public function create( )
    {
        $languages = $this->clientInterface->languages();
        return view('admin.clients.create',compact('languages'));
    }

    public function store(ClientRequest $request)
    {
        $this->clientInterface->store($request);  
        return redirect()->route('clients.index')->with(['success' => 'Client Add Successfully']);
    }

    public function edit($client)
    {
        $client = $this->clientInterface->edit($client);
        $languages = $this->clientInterface->languages();
        return view('admin.clients.edit',compact('client','languages'));
    }

    public function update(ClientRequest $request)
    {
        $this->clientInterface->update($request);
        return  redirect()->route('clients.index')->with(['success' => 'Client Update Successfully']);
    }

    public function destroy(Request $request)
    {
        return  $this->clientInterface->destroy($request);
        return  redirect()->route('clients.index')->with(['success' => 'Client Delete Successfully']);
    }
}
