<?php

namespace App\Http\Traits;

trait UploadFile 
{
    public function uploadMultiImage($image,$folder,$code = '')
    {
        $image_name = time()."_$code.".$image->getClientOriginalExtension();
        $destination = $folder;
        $image->move($destination,$image_name);
        return  $image_name ;
    }


    public function uploadImage($image,$folder)
    {
        $image_name = time().".".$image->getClientOriginalExtension();
        $destination = $folder;
        $image->move($destination,$image_name);
        return  $image_name ;
    }

}