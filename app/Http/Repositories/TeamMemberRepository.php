<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\TeamMemberInterface;
use App\Http\Traits\UploadFile;
use App\Models\Language;
use App\Models\TeamMember;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TeamMemberRepository implements TeamMemberInterface
{
    use UploadFile;
    
    public function index()
    {
        $teamMembers = TeamMember::all();
        return $teamMembers ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();   
    }

    public function store(Request $request)
    { 
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $teamMember=TeamMember::create($inputs);
            $teamMember->image()->create($inputs['mainImg']);
            DB::commit();
            return $teamMember;   

       }catch(Exception $e){
           DB::rollback();
           return $e;
       }
    }

    public function edit($teamMember)
    {
        $teamMember =TeamMember::findOrFail($teamMember);
        return $teamMember;
    }
    
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $teamMember = TeamMember::findOrFail($request->id);
            $inputs = $request->all();
            $teamMember->update($inputs);
            $teamMember->image->update($inputs['mainImg']);
            DB::commit();
            return $teamMember;
        }catch(Exception $e){
            DB::rollback();
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $teamMember = TeamMember::findOrFail($request->id);
        $teamMember->image()->delete();
        $teamMember->delete();
        return response()->json('success');
    }

}