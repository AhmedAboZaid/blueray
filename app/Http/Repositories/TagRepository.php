<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\TagInterface;
use App\Models\Language;
use App\Models\Tag;
use Exception;
use Illuminate\Http\Request;

class TagRepository implements TagInterface
{
    
    public function index()
    {
        $tags = Tag::all();
        return $tags ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();   
    }

    public function store(Request $request)
    {
        try {
            $inputs = $request->all();
            
            $tag=Tag::create($inputs);
            return $tag;   

       }catch(Exception $e){
           dd($e);
           return $e;
       }
    }

    public function edit($tag)
    {
        $tag =Tag::findOrFail($tag);
        return $tag;
    }
    
    public function update(Request $request)
    {
        try {
            $tag = Tag::findOrFail($request->id);
            $tag ->update($request->all());
            return $tag;
        }catch(Exception $e){
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $tag = Tag::findOrFail($request->id);
        $tag->delete();
        return response()->json('success');
    }

}