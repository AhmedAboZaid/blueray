<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\SliderInterface;
use App\Http\Traits\UploadFile;
use App\Models\Language;
use App\Models\Slider;
use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SliderRepository implements SliderInterface
{
    use UploadFile;

    public function index()
    {
        $sliders = Slider::all();
        return $sliders ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $inputs     = $request->except(['_token' , 'profile_avatar_remove' , 'image' ]);
            $slider     = Slider::create($inputs);
            $slider->image()->create($request->image);
            DB::commit();
            return $slider;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function edit($slider)
    {
        $slider =Slider::findOrFail($slider);
        return $slider;
    }

    public function update(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $slider     = Slider::findOrFail($request->id);
            $inputs     = $request->except(['_token' , 'profile_avatar_remove' , 'image' , 'id' ]);
            @$slider->image->update($request->image);
            $slider->update($inputs);
            DB::commit();
            return $slider;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $slider = Slider::findOrFail($request->id);
        @$slider->image->delete();
        $slider->delete();
        return response()->json('success');
    }

}
