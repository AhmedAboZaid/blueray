<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\FacilityInterface;
use App\Models\Facility;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;

class FacilityRepository implements FacilityInterface
{
    public function index()
    {
        $facilities = Facility::all();
        return $facilities ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function store(Request $request)
    {
        try
        {
            $inputs = $request->except('_token');
            $facility = Facility::create($inputs);
            return $facility;
        }
        catch(Exception $e)
        {
            return $e;
        }
    }

    public function edit($facility)
    {
        $facility =Facility::findOrFail($facility);
        return $facility;
    }

    public function update(Request $request)
    {
        try {
            $facility =Facility::findOrFail($request->id);
            $inputs = $request->except('_token');
            $facility->update($inputs);
            return $facility;
        }
        catch(Exception $e)
        {
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $facility =Facility::findOrFail($request->id);
        $facility->delete();
        return response()->json('success');
    }

}
