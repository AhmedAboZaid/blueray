<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\SocialMediaInterface;
use App\Models\SocialMedia;
use Illuminate\Http\Request;

class SocialMediaRepository implements SocialMediaInterface
{
    
    public function index()
    {
        $socialMedias = SocialMedia::all();
        return $socialMedias ;
    }

    public function store(Request $request)
    {
       $socialMedia = SocialMedia::create($request->all());
      return $socialMedia;
    }

    public function edit($socialMedia)
    {
       return $socialMedia =SocialMedia::findOrFail($socialMedia);     
    }
    
    public function update(Request $request)
    {
        $socialMedia = SocialMedia::findOrFail($request->id);
        $socialMedia->update($request->all());
        return $socialMedia;
    }

    public function destroy(Request $request)
    {
        $socialMedias = SocialMedia::findOrFail($request->id);
        $socialMedias->delete();
        return response()->json('success');
    }

}