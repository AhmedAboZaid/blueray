<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\CityInterface;
use App\Http\Traits\UploadFile;
use App\Models\Attachment;
use App\Models\Language;
use App\Models\City;
use App\Models\Tag;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CityRepository implements CityInterface
{
    use UploadFile;

    public function index()
    {
        $cities = City::all();
        return $cities ;
    }
    public function get_all($where=[],$relations=[])
    {
        $cities = City::where($where)->with($relations)->get();
        return $cities ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }
    public function tags()
    {
        return  Tag::all();
    }

    

    public function store(Request $request)
    {

        try
        {
            DB::beginTransaction();
            $languages = Language::where('isActive',1)->get();

            $mainImg    = $request->image;
            $inputs     = $request->except(['_token' , 'profile_avatar_remove' , 'image' , 'tags' , 'attachments' , 'seo']);
            $city       = City::create($inputs);
            foreach($languages as $language)
            {
                $mainImg["$language->code"]['alts']  = $mainImg["$language->code"]['alts']  ?? $inputs["$language->code"]['title'];
            }
            $city->image()->create($mainImg);

            DB::commit();
            return $city;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function edit($city)
    {
        $city   =   City::with('attachments')->with('tags')->findOrFail($city);
        return $city;
    }

    public function update(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $city       = City::findOrFail($request->id);
            $languages  = Language::where('isActive',1)->get();

            $mainImg    = $request->image;
            // $tags       = $request->tags ? $request->tags : [];
            // $attachments= $request->attachments ? $request->attachments : [];
            $inputs     = $request->except(['_token' , 'profile_avatar_remove' , 'image' , 'tags' , 'attachments' , 'seo' , 'id']);

            foreach($languages as $language)
            {
                $mainImg["$language->code"]['alts']  = $mainImg["$language->code"]['alts']  ?? $inputs["$language->code"]['title'];
            }

            $city->image ? $city->image->update($mainImg) : $city->image()->create($mainImg) ;
            $city->update($inputs);

            $pastIds    = $city->attachments()->pluck('id')->toArray();
            $remainIds  = [];

            // foreach($attachments as $key => $attachment)
            // {
            //     $attachment['id']   = @$attachment['id'] ? $attachment['id'] : 0 ;
            //     $attach             = Attachment::find($attachment['id']);
            //     if($attach)
            //     {
            //         $remainIds[]        = $attachment['id'];
            //         $alts               = [] ;
            //         foreach($languages as $language)
            //         {
            //             $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts']  ?? $inputs["$language->code"]['title']];
            //         }
            //         $alts['file'] = @$attachment['file'];
            //         $attach->update(array_merge($alts));
            //     }
            //     else
            //     {
            //         $alts = [] ;
            //         foreach($languages as $language)
            //         {
            //             $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts']  ?? $inputs["$language->code"]['title']];
            //         }
            //         $alts['file'] = @$attachment['file'];
            //         $city->attachments()->create($alts);
            //     }
            // }

            $deleteIds = array_diff($pastIds, $remainIds);
            @$city->attachments()->whereIn('id',$deleteIds)->delete();

            // $city->tags()->sync($tags);

            DB::commit();
            return $city;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }
    public function destroy(Request $request)
    {
        $city = City::with('attachments')->findOrFail($request->id);
        foreach ($city->attachments as $key => $attach)
        {
            @unlink($attach->url);
        }
        @$city->seo()->delete();
        @$city->image()->delete();
        @$city->tags()->detach();
        @$city->attachments()->delete();
        @$city->delete();
        return response()->json('success');
    }

}
