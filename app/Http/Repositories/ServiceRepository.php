<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\ServiceInterface;
use App\Http\Traits\UploadFile;
use App\Models\Attachment;
use App\Models\Service;
use Illuminate\Support\Facades\File;
use App\Models\Language;
use App\Models\Tag;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ServiceRepository implements ServiceInterface
{
    use UploadFile;

    public function index()
    {
        $services = Service::get();
        return $services ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function tags()
    {
        return  Tag::all();
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $languages = Language::where('isActive',1)->get();
            $inputs = $request->except('_token');
            foreach ($languages as $language) {
                $inputs['mainImg'][$language->code]['alts'] =  $inputs['mainImg'][$language->code]['alts'] ??  $inputs[$language->code]['title'];
            }
            $serviceInputs = $request->except(['_token','mainImg','attachments','seo','tags']);
            $service = Service::create($serviceInputs);
            $service->image()->create($inputs['mainImg']);
            DB::commit();
            return $service;
        }catch(Exception $e){
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function edit($service)
    {
        $service =Service::findOrFail($service);
        return $service;
    }

    public function update(Request $request)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();
            $service = Service::findOrFail($request->id);
            $languages = Language::where('isActive',1)->get();
            $inputs = $request->except('_token');
            foreach ($languages as $language) {
                $inputs['mainImg'][$language->code]['alts'] =  $inputs['mainImg'][$language->code]['alts'] ??  $inputs[$language->code]['title'];
            }
            $serviceInputs = $request->except(['_token','mainImg']);
            $service->update($serviceInputs);
            $service->image->update($inputs['mainImg']);
            DB::commit();
            return $service;
        }catch(Exception $e){
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $service = Service::findOrFail($request->id);

        @$service->image()->delete();
        
        $service->delete();
        return response()->json('success');
    }

}
