<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\BlogInterface;
use App\Http\Traits\UploadFile;
use App\Models\Attachment;
use App\Models\Blog;
use Illuminate\Support\Facades\File;
use App\Models\Language;
use App\Models\Tag;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BlogRepository implements BlogInterface
{
    use UploadFile;

    public function index()
    {
        $blogs = Blog::all();
        return $blogs ;
    }
    public function random($limit=1000 , $where=[])
    {
        return Blog::inRandomOrder()->where($where)->limit($limit)->get();
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function tags()
    {
        return  Tag::all();
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $languages  = Language::where('isActive',1)->get();

            $mainImg    = $request->mainImg;
            $tags       = $request->tags ? $request->tags : [];
            $attachments= $request->attachments ? $request->attachments : [];;
            $seo        = $request->seo;
            $inputs     = $request->except(['_token' ,'mainImg' , 'tags' , 'attachments' , 'seo']);

            $blog       = Blog::create($inputs);
            $blog->seo()->create($seo);

            foreach($languages as $language){
                $mainImg[$language->code]['alts'] = $mainImg[$language->code]['alts'] ?? $inputs[$language->code]['title'];
            }

            $blog->image()->create($mainImg);
            // foreach($attachments as $key => $attachment)
            // {
            //     $alts = [] ;
            //     foreach($languages as $language)
            //     {
            //         $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts'] ?? $inputs["$language->code"]['title']];
            //     }
            //     $alts['file'] = @$attachment['file'];
            //     $blog->attachments()->create($alts);
            // }
            $blog->tags()->sync($tags??[]);
            DB::commit();
            return $blog;
        }
        catch(Exception $e){
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function edit($blog)
    {
        $blog =Blog::with('attachments')->with('tags')->findOrFail($blog);
        return $blog;
    }

    public function showBySlug($slug)
    {
        $blog = Blog::whereHas('seo.translations',function($query) use ($slug){$query->where('slug',$slug);})->first();
        return $blog;
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $blog       = Blog::with('attachments')->findOrFail($request->id);
            $languages  = Language::where('isActive',1)->get();

            $mainImg    = $request->mainImg;
            $tags       = $request->tags ? $request->tags : [];
            $attachments= $request->attachments ? $request->attachments : [] ;
            $seo        = $request->seo;
            $inputs     = $request->except(['_token' , 'mainImg' , 'tags' , 'attachments' , 'seo','id']);

            $blog->update($inputs);
            @$blog->seo->update($seo);
            foreach($languages as $language){
                $mainImg[$language->code]['alts'] = $mainImg[$language->code]['alts'] ?? $inputs[$language->code]['title'];
            }
            @$blog->image->update($mainImg);

            $pastIds = $blog->attachments()->pluck('id')->toArray();
            $remainIds = [];

            // foreach($attachments as $key => $attachment)
            // {
            //     $attachment['id']   = @$attachment['id'] ? $attachment['id'] : 0 ;
            //     $attach             = Attachment::find($attachment['id']);
            //     if($attach)
            //     {
            //         $remainIds[]        = $attachment['id'];
            //         $alts           = [] ;
            //         foreach($languages as $language)
            //         {
            //             $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts'] ?? $inputs["$language->code"]['title']];
            //         }
            //         $alts['file'] = @$attachment['file'] ?? $attach->file;
            //         $attach->update(array_merge($alts));
            //     }
            //     else
            //     {
            //         $alts = [] ;
            //         foreach($languages as $language)
            //         {
            //             $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts'] ?? $inputs["$language->code"]['title']];
            //         }
            //         $alts['file'] = @$attachment['file'];
            //         $blog->attachments()->create($alts);
            //     }
            // }

            // $deleteIds = array_diff($pastIds, $remainIds);
            // @$blog->attachments()->whereIn('id',$deleteIds)->delete();

            $blog->tags()->sync($tags);
            DB::commit();
            return $blog;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $blog = Blog::with('attachments')->findOrFail($request->id);
        foreach ($blog->attachments as $key => $attach)
        {
            @unlink($attach->url);
        }
        @$blog->seo()->delete();
        @$blog->image()->delete();
        @$blog->tags()->detach();
        @$blog->attachments()->delete();
        @$blog->delete();
        return response()->json('success');
    }
}
