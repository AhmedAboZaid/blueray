<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\AlbumInterface;
use App\Http\Traits\UploadFile;
use App\Models\Album;
use App\Models\Attachment;
use Illuminate\Support\Facades\File;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AlbumRepository implements AlbumInterface
{
    use UploadFile;

    public function index()
    {
        $albums = Album::all();
        return $albums ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $languages = Language::where('isActive',1)->get();
            $inputs = $request->except(['_token']);
            $albumInputs = $request->except(['_token','mainImg','attachments','seo']);
            $album = Album::create($albumInputs);
            $album->seo()->create($inputs['seo']);
            $main_image = $inputs['mainImg'];
            foreach($languages as $language){
                $main_image["$language->code"]['alts']  = @$main_image["$language->code"]['alts'] ?? @$inputs["$language->code"]['title'];
            }
            $album->image()->create($main_image);

            foreach(@$inputs['attachments'] ?? [] as $key => $attachment)
            {
                $alts = [] ;
                foreach($languages as $language)
                {
                    $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts']  ?? $inputs["$language->code"]['title'] ];
                }
                $alts['file'] = @$attachment['file'];
                $album->attachments()->create($alts);
            }
            DB::commit();
            return $album;
        }catch(Exception $e){
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function edit($album)
    {
        $album =Album::with('attachments')->findOrFail($album);
        return $album;
    }

    public function update(Request $request)
    {
        try {

            DB::beginTransaction();
            $album       = Album::with('attachments')->findOrFail($request->id);
            $languages   = Language::where('isActive',1)->get();
            $inputs = $request->except(['_token']);
            $albumInputs = $request->except(['_token','mainImg','attachments','seo']);
            $album->update($albumInputs);
            $album->seo->update($inputs['seo']);
            $album->image->update($inputs['mainImg']);

            $pastIds    = $album->attachments()->pluck('id')->toArray();
            $remainIds  = [];
            if(@$inputs['attachments'] )
            {
                foreach($inputs['attachments'] as $attachment)
                {
                    $attachment['id']   = $attachment['id'] ? $attachment['id'] :0 ;
                    $attach             = Attachment::find($attachment['id']);
                    if($attach)
                    {
                        $remainIds[]        = $attachment['id'];
                        $alts           = [] ;
                        foreach($languages as $language)
                        {
                            $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts']  ?? $inputs["$language->code"]['title'] ];
                        }
                        if(@$attachment['file']){
                            $alts['file'] = @$attachment['file'];
                        }
                        $attach->update(array_merge($alts));
                    }
                    else
                    {
                        $alts = [] ;
                        foreach($languages as $language)
                        {
                            $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts']  ?? $inputs["$language->code"]['title'] ];
                        }
                        $alts['file'] = @$attachment['file'];
                        $album->attachments()->create($alts);
                    }
                }
            }
            $deleteIds = array_diff($pastIds, $remainIds);
            @$album->attachments()->whereIn('id',$deleteIds)->delete();
            DB::commit();
            return $album;
        }catch(Exception $e){
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $album = Album::findOrFail($request->id);
        foreach ($album->attachments as $attach)
        {
            @unlink($attach->url);
        }
        @$album->seo()->delete();
        @$album->attachments()->delete();
        $album->image()->delete();
        $album->delete();
        return response()->json('success');
    }

    public function showBySlug($slug)
    {
        $album = Album::whereHas('seo.translations',function($query) use ($slug){$query->where('slug',$slug);})->first();
        return $album;
    }

}
