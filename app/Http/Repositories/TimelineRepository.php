<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\TimelineInterface;
use App\Models\Language;
use App\Models\Timeline;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TimelineRepository implements TimelineInterface
{
    
    public function index()
    {
        $timelines = Timeline::all();
        return $timelines ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();   
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->all();
            $inputs['date'] = date_create($request->date)->format('Y-m-d H:i');
            
            $timeline=Timeline::create($inputs);
            DB::commit();
            return $timeline;   

       }catch(Exception $e){
           DB::rollback();
           return $e;
       }
    }

    public function edit($timeline)
    {
        $timeline =Timeline::findOrFail($timeline);
        return $timeline;
    }
    
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $timeline = Timeline::findOrFail($request->id);
            $inputs = $request->all();
            $inputs['date'] = date_create($request->date)->format('Y-m-d H:i');
            $timeline ->update($inputs);
            DB::commit();
            return $timeline;   

       }catch(Exception $e){
           DB::rollback();
           return $e;
       }
    }

    public function destroy(Request $request)
    {
        $timeline = Timeline::findOrFail($request->id);
        $timeline->delete();
        return response()->json('success');
    }

}