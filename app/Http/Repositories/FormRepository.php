<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\FormInterface;
use App\Models\Ticket;
use App\Models\Contact;
use App\Models\Book;
use App\Models\CorporateForm;
use App\Mail\CorporateFormMail;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;



class FormRepository implements FormInterface
{
    public function contact(Request $request)
    {
        try
        {
            Contact::create($request->all());
            return 'Your Request Sent Successfully';
        }
        catch(Exception $ex)
        {
            return 'Error Occur Try Again';
        }
    }
    public function bookForm(Request $request)
    {
        try
        {
            Book::create($request->all());
            return 'Your Request Sent Successfully';
        }
        catch(Exception $ex)
        {
            return 'Error Occur Try Again';
        }
    }
    public function ticketForm(Request $request)
    {
        try
        {
            // dd($request->all());
            $ticket = Ticket::create($request->all());

            if ($request->ticket_type != 'multiple' ) {
                $ticket->destinations()->create([
                    'from' => $request->destination_from,
                    'to'   => $request->destination_to,
                ]);


            }

            $ticket->persons()->createMany($request->persons ?? []);
            return 'Your Request Sent Successfully';


        }
        catch(Exception $ex)
        {
            dd($ex);
            return 'Error Occur Try Again';
        }
    }


}
