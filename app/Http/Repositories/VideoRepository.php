<?php

namespace App\Http\Repositories;

use App\Http\Interfaces\VideoInterface;
use App\Models\Language;
use App\Models\Video;
use Exception;
use Illuminate\Http\Request;

class VideoRepository implements VideoInterface
{

    public function index()
    {
        $videos = Video::all();
        return $videos;
    }

    public function languages()
    {
        return Language::where('isActive', 1)->get();
    }

    public function store(Request $request)
    {
        $languages = Language::where('isActive', 1)->pluck('code')->toArray();
        try {
            $inputs = $request->all();
            foreach ($languages as $code) {
                $inputs[$code]['alt'] =  $inputs[$code]['alt'] ??  $inputs[$code]['title'];
            }
            $video = Video::create($inputs);
            $video->seo()->create($inputs['seo']);
            return $video;
        } catch (Exception $e) {
            dd($e);
            return $e;
        }
    }

    public function edit($video)
    {
        $video = Video::findOrFail($video);
        return $video;
    }

    public function update(Request $request)
    {
        $languages = Language::where('isActive', 1)->pluck('code')->toArray();
        try {
            $video = Video::findOrFail($request->id);
            $inputs = $request->all();

            foreach ($languages as $code) {
                $inputs[$code]['alt'] =  $inputs[$code]['alt'] ??  $inputs[$code]['title'];
            }
            
            $video->update($inputs);
            $video->seo->update($inputs['seo']);
            return $video;
        } catch (Exception $e) {
            dd($e);
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $video = Video::findOrFail($request->id);
        @$video->seo()->delete();
        $video->delete();
        return response()->json('success');
    }
}
