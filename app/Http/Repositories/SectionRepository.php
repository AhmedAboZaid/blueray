<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\SectionInterface;
use App\Http\Traits\UploadFile;
use App\Models\Language;
use App\Models\MainPage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class SectionRepository implements  SectionInterface
{
    use UploadFile;
    
    public function index()
    {
        $sections = MainPage::where('parent_id',1)->where('fixed_name',null)->get();
        return $sections ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();   
    }
    
    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->except('_token');
            $languages = Language::where('isActive',1)->pluck('code')->toArray();
            foreach($languages as $code){
                if($request->hasFile($code.'.image')){
                    $inputs[$code]['image'] = $this->uploadMultiImage($request[$code]['image'],'uploads/sections',$code);   
                }
            }
            $section = MainPage::create($inputs);
            $section->seo()->create($inputs['seo']);
            DB::commit();
            return $section;

        }catch(Exception $e){
            DB::rollback();
            return $e;
        }
        
    }

    public function edit($section)
    {
       return $section =MainPage::findOrFail($section);     
    }
    
    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $section = MainPage::findOrFail($request->id);
            $inputs = $request->all();
            $languages = Language::where('isActive',1)->pluck('code')->toArray();
            foreach($languages as $code){
                if($request->hasFile($code.'.image')){
                    if(File::exists("uploads/sections/".$section->{'image:'.$code})){
                        unlink("uploads/sections/".$section->{'image:'.$code});
                    }
                    $inputs[$code]['image'] = $this->uploadMultiImage($request[$code]['image'],'uploads/sections',$code);   
                }
            }
            $section->update($inputs);
            $section->seo->update($inputs['seo']);
            DB::commit();
            return $section;

        }catch(Exception $e){
            DB::rollback();
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $section = MainPage::findOrFail($request->id);
        $section->seo()->delete();
        $section->delete();
        return response()->json('success');
    }



}