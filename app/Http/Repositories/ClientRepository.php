<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\ClientInterface;
use App\Http\Traits\UploadFile;
use App\Models\Client;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;

class ClientRepository implements ClientInterface
{
    use UploadFile;

    public function index()
    {
        $clients = Client::all();
        return $clients ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function store(Request $request)
    {
        try
        {
            $inputs     =   $request->except(['_token' , 'profile_avatar_remove' , 'image' , 'id' ]);
            $client     =   Client::create($inputs);
            @$client->image()->create($request->image);
            return $client;
        }
        catch(Exception $e)
        {
            dd($e);
            return $e;
        }
    }

    public function edit($client)
    {
        $client =Client::findOrFail($client);
        return $client;
    }

    public function update(Request $request)
    {
        try {
            $client     = Client::findOrFail($request->id);
            $inputs     = $request->except(['_token' , 'profile_avatar_remove' , 'image' , 'id' ]);
            @$client->image->update($request->image);
            $client->update($inputs);
            return $client;
        }
        catch(Exception $e)
        {
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $client = Client::findOrFail($request->id);
        @$client->image->delete();
        $client->delete();
        return response()->json('success');
    }

}
