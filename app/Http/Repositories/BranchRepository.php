<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\BranchInterface;
use App\Models\Branch;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;

class BranchRepository implements BranchInterface
{
    public function index()
    {
       $branches = branch::all();
        return $branches;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function store(Request $request)
    {
        // dd($request->all());
        try
        {
            $inputs = $request->except('_token');
            $branch = Branch::create($inputs);
            $branch->image()->create($inputs['mainImg']);
            return $branch;
        }
        catch(Exception $e)
        {
            dd($e);
            return $e;
        }
    }

    public function edit($branch)
    {
        $branch =Branch::with('image')->findOrFail($branch);
        return $branch;
    }

    public function update(Request $request)
    {
        // dd($request->all());
        try {
            $branch =Branch::findOrFail($request->id);
            $inputs = $request->except('_token');
            // dd($inputs);
            $branch->update($inputs);

            $branch->image ? $branch->image->update($inputs['mainImg']) :$branch->image()->create($inputs['mainImg']) ;
            return $branch;
        }
        catch(Exception $e)
        {
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $branch =Branch::findOrFail($request->id);
        $branch->delete();
        return response()->json('success');
    }

}
