<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\FaqInterface;
use App\Models\Faq;
use App\Models\Language;
use Exception;
use Illuminate\Http\Request;

class FaqRepository implements FaqInterface
{
    
    public function index()
    {
        $faqs = Faq::all();
        return $faqs ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();   
    }

    public function store(Request $request)
    {
        try {
            $inputs = $request->all();
            
            $FAQ=Faq::create($inputs);
            return $FAQ;   

       }catch(Exception $e){
           dd($e);
           return $e;
       }
    }

    public function edit($FAQ)
    {
        $FAQ =Faq::findOrFail($FAQ);
        return $FAQ;
    }
    
    public function update(Request $request)
    {
        try {
            $FAQ = Faq::findOrFail($request->id);
            $FAQ ->update($request->all());
            return $FAQ;
        }catch(Exception $e){
            dd($e);
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $FAQ = Faq::findOrFail($request->id);
        $FAQ->delete();
        return response()->json('success');
    }

}