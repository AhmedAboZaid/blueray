<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\MainPageInterface;
use App\Http\Traits\UploadFile;
use App\Models\Language;
use App\Models\MainPage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class MainPageRepository implements MainPageInterface
{
    use UploadFile;

    public function index()
    {
        $mainPages = MainPage::get();
        return $mainPages ;
    }

    public function first($whereClause=[],$with=[])
    {
        $mainPage = MainPage::where($whereClause)->with($with)->first();
        return $mainPage ;
    }
    public function all($whereClause=[],$with=[])
    {
        $mainPage = MainPage::where($whereClause)->with($with)->get();
        return $mainPage ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function edit($mainPage)
    {
       return $mainPage =MainPage::findOrFail($mainPage);
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $mainPage = MainPage::findOrFail($request->id);
            $inputs = $request->all();

            $languages = Language::where('isActive',1)->pluck('code')->toArray();
            foreach($languages as $code)
            {
                $inputs[$code]['alt'] =  $inputs[$code]['alt'] ??  $inputs[$code]['title'];
                if($request->hasFile($code.'.image'))
                {
                    if(File::exists("uploads/main-pages/".$mainPage->{'image:'.$code})){
                        @unlink("uploads/main-pages/".$mainPage->{'image:'.$code});
                    }
                    $inputs[$code]['image'] = $this->uploadMultiImage($request[$code]['image'],'uploads/main-pages',$code);
                }
            }
            $mainPage->update($inputs);
            $mainPage->seo->update($inputs['seo']);
            DB::commit();
            return $mainPage;

        }catch(Exception $e){
            dd($e);
            DB::rollback();
            return $e;
        }
    }



}
