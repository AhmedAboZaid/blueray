<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\PackageInterface;
use App\Http\Traits\UploadFile;
use App\Models\Attachment;
use App\Models\City;
use App\Models\Facility;
use App\Models\Language;
use App\Models\Package;
use App\Models\Trip;
use App\Models\Tag;
use App\Models\TourismType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PackageRepository implements PackageInterface
{
    use UploadFile;

    public function index()
    {
        $cities = Package::all();
        return $cities ;
    }

    public function first($whereClause=[],$with=[])
    {
        $mainPage = Package::where($whereClause)->with($with)->first();
        return $mainPage ;
    }
    public  function showBySlug($slug ,$with=[] ){
        return Package::whereHas('seo' , function($q) use($slug){
            return $q->whereTranslationLike('slug',$slug);
        })->with($with)->first();
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function cities($where=[])
    {
        return City::where($where)->get();
    }

    public function tourism_types()
    {
        return TourismType::get();
    }
    public function tags()
    {
        return  Tag::all();
    }
    public function facilities()
    {
        return  Facility::all();
    }

    public function store(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $inputs     = $request->except(['_token' , 'profile_avatar_remove' , 'image' , 'tags' , 'attachments' , 'seo' ,'languages','travelPlans']);
            $languages  = $this->languages();
            $mainImg    = $request->image;
            foreach($languages as $language){
                $mainImg["$language->code"]['alts']  = @$mainImg["$language->code"]['alts'] ?? @$inputs["$language->code"]['title'];
            }
            $tags       = $request->tags ? $request->tags : [];
            $attachments= $request->attachments ? $request->attachments : [];;
            $seo        = $request->seo;

            $Trip       = Package::create($inputs);

            $Trip->languages()->sync($request->languages ?? []);
            $Trip->travelPlans()->createMany($request->travelPlans ?? []);

            $Trip->seo()->create($seo);
            $Trip->image()->create($mainImg);
            $Trip->facilitiesInclude()->sync($request->facilities_enclude ?? []);
            $Trip->facilitiesExclude()->sync($request->facilities_exclude ?? []);
            $Trip->tags()->sync($tags);

            foreach (Attachment::handleRequest($attachments) ?? [] as $key => $attach)
            {
                $Trip->attachments()->create($attach);
            }

            DB::commit();
            return $Trip;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function edit($Trip)
    {
        $Trip   =   Package::with('attachments')->with('tags')->findOrFail($Trip);
        return $Trip;
    }

    public function update(Request $request)
    {
        try
        {
            //dd($request->all());
            DB::beginTransaction();
            $Trip       = Package::with('attachments')->findOrFail($request->id);

            $mainImg    = $request->image;
            $tags       = $request->tags ? $request->tags : [];
            $attachments= $request->attachments ? $request->attachments : [];
            $seo        = $request->seo;
            $inputs     = $request->except(['_token' , 'profile_avatar_remove' , 'image' , 'tags' , 'attachments' , 'seo' , 'id','languages','travelPlans']);

            // dd($request->all());

            $Trip->update($inputs);

            @$Trip->travelPlans()->delete();

            $Trip->languages()->sync($request->languages ?? []);
            $Trip->travelPlans()->createMany($request->travelPlans ?? []);

            @$Trip->seo->update($seo);
            @$Trip->image->update($mainImg);
            $Trip->facilitiesInclude()->sync($request->facilities_enclude ?? []);
            $Trip->facilitiesExclude()->sync($request->facilities_exclude ?? []);
            $Trip->tags()->sync($tags);

            $pastIds    = $Trip->attachments()->pluck('id')->toArray();
            $remainIds  = [];

            foreach($attachments as $key => $attachment)
            {
                $attachment['id']   = @$attachment['id'] ? $attachment['id'] : 0 ;
                $attach             = Attachment::find($attachment['id']);
                if($attach)
                {
                    $remainIds[]        = $attachment['id'];
                    $attach->update(Attachment::handleRequest($attachment));
                }
                else
                {
                    $Trip->attachments()->create(Attachment::handleRequest($attachment));
                }
            }

            $deleteIds = array_diff($pastIds, $remainIds);
            @$Trip->attachments()->whereIn('id',$deleteIds)->delete();

            DB::commit();
            return $Trip;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $Trip = Package::with('attachments')->findOrFail($request->id);
        foreach ($Trip->attachments as $key => $attach)
        {
            @unlink($attach->url);
        }
        @$Trip->seo()->delete();
        @$Trip->image()->delete();
        @$Trip->tags()->detach();
        @$Trip->facilitiesInclude()->detach();
        @$Trip->facilitiesExclude()->detach();
        @$Trip->attachments()->delete();
        @$Trip->delete();
        return response()->json('success');
    }

}
