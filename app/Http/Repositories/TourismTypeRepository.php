<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\TourismTypeInterface;
use App\Http\Traits\UploadFile;
use App\Models\Attachment;
use App\Models\Blog;
use App\Models\Language;
use App\Models\Tag;
use App\Models\TourismType;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class TourismTypeRepository implements TourismTypeInterface
{
    use UploadFile;

    public function index()
    {
        $tourismTypes = TourismType::all();
        return $tourismTypes;
    }
    public function first($whereClause=[],$with=[])
    {
        $mainPage = TourismType::where($whereClause)->with($with)->first();
        return $mainPage ;
    }
    public  function showBySlug($slug , $with = []){
        return TourismType::whereHas('seo' , function($q) use($slug){
            return $q->whereTranslationLike('slug',$slug);
        })->with($with)->first();
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }

    public function tags()
    {
        return  Tag::all();
    }

    public function store(Request $request)
    {
        try {
            DB::beginTransaction();
            $languages  = Language::where('isActive',1)->get();
            $inputs = $request->except('_token');
            $tourismTypeInputs = $request->except(['_token','mainImg','icon','attachments','seo','tags']);
            $tourismType = TourismType::create($tourismTypeInputs);
            $tourismType->seo()->create($inputs['seo']);
            $main_image = $inputs['mainImg'];
            foreach($languages as $language){
                $main_image["$language->code"]['alts']  = @$main_image["$language->code"]['alts'] ?? $inputs["$language->code"]['title'];
            }
            $tourismType->image()->create($main_image);
            $tourismType->icon()->create($inputs['icon']);
            // $tourismType->tags()->sync($inputs['tags']);

            // foreach($inputs['attachments'] as $attachment)
            // {
            //     $alts = [] ;
            //     foreach($languages as $language){
            //         $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts'] ?? $inputs["$language->code"]['title']];
            //     }
            //     $alts['file'] = @$attachment['file'];
            //     $tourismType->attachments()->create($alts);
            // }
            DB::commit();
            return $tourismType;
        }
        catch(Exception $e){
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function edit($tourismType)
    {
        $tourismType =TourismType::with('attachments')->with('tags')->findOrFail($tourismType);
        return $tourismType;
    }

    public function update(Request $request)
    {
        // dd($request->all());
        try {
            DB::beginTransaction();
            $tourismType       = TourismType::with('attachments')->findOrFail($request->id);
            $languages  = Language::where('isActive',1)->get();
            $inputs = $request->except('_token');
            $tourismTypeInputs = $request->except(['_token','mainImg','attachments','seo','tags','icon']);
            $tourismType->update($tourismTypeInputs);
            $tourismType->seo->update($inputs['seo']);
            $tourismType->image->update($inputs['mainImg']);
            $tourismType->icon ? $tourismType->icon->update($inputs['icon']) : $tourismType->icon()->create($inputs['icon']);
            $tourismType->tags()->sync($inputs['tags'] ?? []);

            // $pastIds = $tourismType->attachments()->pluck('id')->toArray();
            // $remainIds = [];
            // foreach($inputs['attachments'] as  $attachment)
            // {
            //     $attachment['id']   = @$attachment['id'] ? $attachment['id'] : 0 ;
            //     $attach             = Attachment::find($attachment['id']);
            //     if($attach)
            //     {
            //         $remainIds[]        = $attachment['id'];
            //         $alts           = [] ;
            //         foreach($languages as $language)
            //         {
            //             $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts'] ?? $inputs["$language->code"]['title']];
            //         }
            //         if(@$attachment['file']){
            //             $alts['file'] = @$attachment['file'];
            //         }
            //         $attach->update(array_merge($alts));
            //     }
            //     else
            //     {
            //         $alts = [] ;
            //         foreach($languages as $language)
            //         {
            //             $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts'] ?? $inputs["$language->code"]['title']];
            //         }
            //         $alts['file'] = @$attachment['file'];
            //         $tourismType->attachments()->create($alts);
            //     }
            // }
            // $deleteIds = array_diff($pastIds, $remainIds);
            // @$tourismType->attachments()->whereIn('id',$deleteIds)->delete();
            DB::commit();
            return $tourismType;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function destroy(Request $request)
    {
        $tourismType = TourismType::with('attachments')->findOrFail($request->id);
        foreach ($tourismType->attachments as $attach)
        {
            @unlink($attach->url);
        }
        @$tourismType->seo()->delete();
        @$tourismType->image()->delete();
        @$tourismType->tags()->detach();
        @$tourismType->attachments()->delete();
        @$tourismType->delete();
        return response()->json('success');
    }
}
