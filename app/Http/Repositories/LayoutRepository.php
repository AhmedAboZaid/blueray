<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\LayoutInterface;
use App\Http\Resources\LanguageResource;
use App\Http\Resources\MainPageNavbarResource;
use App\Http\Resources\SocialMediaResource;
use App\Models\Language;
use App\Models\MainPage;
use App\Models\Setting;
use App\Models\SocialMedia;
use App\Models\Tag;

class LayoutRepository implements LayoutInterface
{

    public function navbar()
    {
        $home       = MainPage::hydrate( [["id" => 0, "title" => "Home", "slug" => ""]] );
        $main_pages = $home->merge(MainPage::whereNull('parent_id')->get());
        return [
            "logo_url"  => asset("uploads/settings/".Setting::where('key','logo')->first()?->value),
            "logo_alt"  => Setting::where('key','alt')->first()?->value,
            "lang"      => LanguageResource::collection($this->languages()),
            "navs"      => MainPageNavbarResource::collection($main_pages),
        ];
    }


    public function footer()
    {
        return [
            "image_url"   => asset("uploads/settings/".Setting::where('key','logo')->first()?->value),
            "image_alt"   => Setting::where('key','alt')->first()?->value,
            "description" => Setting::where('key','description')->first()?->value ?? "Blue Ray company is aiming to satisfy their clients by offering a variety of adventure experiences that our clients desire.",
            "phone"       => Setting::where('key','phone')->first()?->value ?? "(+965) 2232-2221",
            "email"       => Setting::where('key','email')->first()?->value ?? "sales.b@blueraytourism.com",
            "address"     => Setting::where('key','address')->first()?->value ?? "2752 Willison Street , Eagan, United State",
            "copy_rights" => Setting::where('key','copy_rights')->first()?->value ?? "Copyright © 2022 Blueray All rights reserves",
            "socials"     => SocialMediaResource::collection(SocialMedia::get())
        ];
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }


}
