<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\SettingInterface;
use App\Http\Traits\UploadFile;
use App\Models\Language;
use App\Models\Setting;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\DB;

class SettingRepository implements SettingInterface
{
    use UploadFile;
    
    public function index()
    {
        $settings = Setting::all();
        return $settings ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();   
    }

    public function update(Request $request)
    {
        try {
            DB::beginTransaction();
            $inputs = $request->except('_token','profile_avatar_remove');
            foreach($inputs as $key=>$value)
            {     
                $setting   = Setting::where('key',$key)->first();
                if($request->hasFile("$key.*.value"))
                {
                    foreach($request->$key as $code=>$file)
                    {
                        if(File::exists("uploads/settings/".$setting->{'value:'.$code}))
                        {
                            @unlink("uploads/settings/".$setting->{'value:'.$code});
                        }
                        $value[$code]['value'] = $this->uploadMultiImage($file['value'],'uploads/settings',$code);   
                    }
                }
                $setting->update($value);
            } 
              DB::commit();
             return $setting;

        }catch(Exception $e){
            DB::rollback();
            return $e;
        }
    }
}