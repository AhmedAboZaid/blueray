<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\ProjectInterface;
use App\Http\Traits\UploadFile;
use App\Models\Attachment;
use Illuminate\Support\Facades\File;
use App\Models\Language;
use App\Models\Project;
use App\Models\Tag;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProjectRepository implements ProjectInterface
{
    use UploadFile;

    public function index()
    {
        $projects = Project::all();
        return $projects ;
    }

    public function languages()
    {
        return Language::where('isActive' , 1)->get();
    }
    public function tags()
    {
        return  Tag::all();
    }

    public function store(Request $request)
    {
        try
        {
            // dd($request->all());
            DB::beginTransaction();
            $languages = Language::where('isActive',1)->get();

            $mainImg    = $request->mainImg;
            $tags       = $request->tags ? $request->tags : [];
            $attachments= $request->attachments ? $request->attachments : [];;
            $seo        = $request->seo;
            $inputs     = $request->except(['_token' , 'profile_avatar_remove' , 'mainImg' , 'tags' , 'attachments' , 'seo']);
            $project    = Project::create($inputs);

            $project->seo()->create($seo);
            $project->image()->create($mainImg);

            foreach($attachments as $key => $attachment)
            {
                $alts = [] ;
                foreach($languages as $language)
                {
                    $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts'] ?? $inputs["$language->code"]['title']];
                }
                $alts['file'] = @$attachment['file'];
                $project->attachments()->create($alts);
            }
            $project->tags()->sync($tags);
            DB::commit();
            return $project;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }

    public function edit($project)
    {
        $project =Project::with('attachments')->with('tags')->findOrFail($project);
        return $project;
    }

    public function update(Request $request)
    {
        try
        {
            DB::beginTransaction();
            $project    = Project::with('attachments')->findOrFail($request->id);
            $languages  = Language::where('isActive',1)->get();

            $mainImg    = $request->mainImg;
            $tags       = $request->tags ? $request->tags : [];
            $attachments= $request->attachments ? $request->attachments : [];
            $seo        = $request->seo;
            $inputs     = $request->except(['_token' , 'profile_avatar_remove' , 'mainImg' , 'tags' , 'attachments' , 'seo' , 'id']);

            // dd($request->all());

            @$project->seo->update($seo);
            @$project->image->update($mainImg);

            $project->update($inputs);

            $pastIds    = $project->attachments()->pluck('id')->toArray();
            $remainIds  = [];

            foreach($attachments as $key => $attachment)
            {
                $attachment['id']   = @$attachment['id'] ? $attachment['id'] : 0 ;
                $attach             = Attachment::find($attachment['id']);
                if($attach)
                {
                    $remainIds[]        = $attachment['id'];
                    $alts           = [] ;
                    foreach($languages as $language)
                    {
                        $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts'] ?? $inputs["$language->code"]['title']];
                    }
                    if(@$attachment['file']){
                        $alts['file'] = @$attachment['file'];
                    }
                    $attach->update(array_merge($alts));
                }
                else
                {
                    $alts = [] ;
                    foreach($languages as $language)
                    {
                        $alts["$language->code"]  = ['alts' => $attachment[$language->code .'alts'] ?? $inputs["$language->code"]['title']];
                    }
                    $alts['file'] = @$attachment['images'];
                    $project->attachments()->create($alts);
                }
            }

            $deleteIds = array_diff($pastIds, $remainIds);
            @$project->attachments()->whereIn('id',$deleteIds)->delete();

            $project->tags()->sync($tags);

            DB::commit();
            return $project;
        }
        catch(Exception $e)
        {
            dd($e);
            DB::rollback();
            return $e;
        }
    }
    public function destroy(Request $request)
    {
        $project = Project::with('attachments')->findOrFail($request->id);
        foreach ($project->attachments as $key => $attach)
        {
            @unlink($attach->url);
        }
        @$project->seo()->delete();
        @$project->image()->delete();
        @$project->tags()->detach();
        @$project->attachments()->delete();
        @$project->delete();
        return response()->json('success');
    }

}
