<?php
namespace App\Http\Repositories;

use App\Http\Interfaces\LanguageInterface;
use App\Http\Traits\UploadFile;
use App\Models\Language;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;

class LanguageRepository implements LanguageInterface
{
    use UploadFile;
    
    public function index()
    {
        $languages = Language::all();
        return $languages ;
    }

    public function store(Request $request)
    {
        if(isset($request->isActive)) {
            $isActive =  $request->isActive = 1;
        } else {
              $isActive = $request->isActive = 0;
          } 
        $flag = ($request->flag) ? $this->uploadImage($request->flag,'uploads/languages') : null;
       $language = Language::create([
                    'language' => $request->language,
                    'code'     =>$request->code,
                    'flag'     => $flag,
                    'isActive' => $isActive,
                ]);
        return $language;
    }

    public function edit($language)
    {
       return $language =Language::findOrFail($language);     
    }
    
    public function update(Request $request)
    {
        $language = Language::findOrFail($request->id);
        $inputs = $request->all();
        if($request->hasFile('flag') || $request->flag != ''){
            if(File::exists("uploads/languages/".$language->flag)){
                unlink("uploads/languages/".$language->flag);
            }
            $inputs['isActive'] =$request->isActive ? 1 :0;
            $inputs['code']     =$request->code;
            $inputs['flag'] = $this->uploadImage($request->flag,'uploads/languages');
            $language->update($inputs);
        }else{
            $inputs['isActive'] =$request->isActive ? 1 :0;
            $language->update($inputs);
        }
        return $language;
    }

    public function destroy(Request $request)
    {
        $language = Language::findOrFail($request->id);
        $language->delete();
        return response()->json('success');
    }

    public function changeStatus(Request $request)
    {
        $language = Language::find($request->id);
        ($language->isActive==1) ? $language->isActive=0 : $language->isActive=1 ;
        $language->save();
        return response()->json('success');
    }

}