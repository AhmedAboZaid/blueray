<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
class TourismType extends Model implements TranslatableContract
{
    use HasFactory, Translatable, SoftDeletes;

    protected $guarded =[];
    protected $translatedAttributes=['title','description'];
    protected $casts =[
        'created_at'=>'datetime:Y-m-d h:i',
    ];

    public function packages()
    {
        return $this->hasMany(Package::class);
    }
    public function seo()
    {
        return $this->morphOne(seo::class , 'seoable');
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable')->where('is_main',0);
    }
    public function image()
    {
        return $this->morphOne(Attachment::class, 'attachmentable')->withDefault([
            'is_main' => 1
        ]);
    }
    public function icon()
    {
        return $this->morphOne(Attachment::class, 'attachmentable')->where([
            'is_main' => 2
        ]);
    }
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

}
