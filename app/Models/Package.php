<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Translatable;

class Package extends Model
{
    use HasFactory, SoftDeletes, Translatable;
    protected $fillable =['tourism_type_id','city_id' , 'rate','nights'];
    protected $casts =[
        'created_at'=>'datetime:Y-m-d h:i',
    ];
    public $translatedAttributes = ['title','description' , 'departure','travel_plan_description'];

    public function tourism_type()
    {
        return $this->belongsTo(TourismType::class,'tourism_type_id');
    }
    public function city()
    {
        return $this->belongsTo(City::class,'city_id');
    }

    public function getCountryAttribute()
    {
        return $this->city?->country ?? $this->city;
    }

    public function seo()
    {
        return $this->morphOne(seo::class , 'seoable');
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable')->where('is_main' , 0);
    }

    public function image()
    {
        return $this->morphOne(Attachment::class, 'attachmentable')->where('is_main' , 1);
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function facilitiesInclude()
    {
        return $this->belongsToMany(Facility::class , 'package_facility_include');
    }
    public function facilitiesExclude()
    {
        return $this->belongsToMany(Facility::class , 'package_facility_exclude');
    }

    public function languages()
    {
        return $this->belongsToMany(Language::class , 'package_languages');
    }

    public function getTourSlugAttribute()
    {
        return $this->tourism_type?->seo?->slug;
    }

    /**
     * Get all of the TravelPlans for the Package
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function travelPlans() // Departure
    {
        return $this->hasMany(TravelPlan::class, 'package_id');
    }

   

}

