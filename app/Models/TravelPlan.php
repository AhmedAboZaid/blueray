<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
class TravelPlan extends Model implements TranslatableContract
{
    use HasFactory, Translatable;
    protected $fillable =['package_id','number'];
    protected $translatedAttributes=['title','description'];
    protected $casts =[
        'created_at'=>'datetime:Y-m-d h:i',
    ];

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id');
    }
}
