<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class seoTranslation extends Model
{
    use HasFactory;
    public $timestamps  = false;
    protected $fillable = ['meta_description' ,'meta_title' , 'meta_keywords' , 'slug'];

    function SetMetaKeyWordsAttribute($meta_keywords)
    {
        $explode_meta_keywords = explode(',', str_replace(' ', '_', $meta_keywords));
        $meta_keywords_array = [];
        foreach ($explode_meta_keywords as $explode_meta_keyword) {
            if (substr($explode_meta_keyword, 0, 1) == '#') {
                $meta_keywords_array[] = $explode_meta_keyword;
            } else {
                $meta_keywords_array[] = '#' . $explode_meta_keyword;
            }
        }
        $meta_keywords                     = collect($meta_keywords_array)->implode(',');
        $this->attributes['meta_keywords'] =  $meta_keywords;
    }
}
