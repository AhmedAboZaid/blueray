<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class Album extends Model implements TranslatableContract
{
    use HasFactory, Translatable, SoftDeletes;

    protected $guarded = [];   
    public $translatedAttributes = ['title', 'description' ];

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable')->where('is_main',0);
    }
    public function image()
    {
        return $this->morphOne(Attachment::class, 'attachmentable')->withDefault([
            'is_main' => 1
        ]);
    }
    public function seo()
    {
        return $this->morphOne(seo::class , 'seoable');
    }

}
