<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Facility extends Model implements TranslatableContract
{
    use HasFactory , Translatable ,SoftDeletes;

    protected $fillable = ['id'];
    public $translatedAttributes = ['name' ];
    protected $casts =[
        'created_at'=>'datetime:Y-m-d h:i',
    ];

    public function tripsInclude()
    {
        return $this->belongsToMany(Trip::class , 'trip_facility_include');
    }
    public function tripsExclude()
    {
        return $this->belongsToMany(Trip::class , 'trip_facility_excludec');
    }

}
