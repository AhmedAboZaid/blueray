<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketDestination extends Model
{
    use HasFactory;

    protected $fillable = ['id','from','to','ticket_id'];

    /**
     * Get the ticket that owns the TicketDestination
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
    */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class ,'ticket_id');
    }


}
