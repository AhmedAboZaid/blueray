<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class City extends Model implements TranslatableContract
{
    use HasFactory , Translatable ,SoftDeletes;
    protected $fillable=['parent_id'];
    public $translatedAttributes = ['title', 'description' ];

    protected $casts =[
        'created_at'=>'datetime:Y-m-d h:i',
    ];

    /**
     * Get the parent that owns the City
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(City::class, 'parent_id');
    }

    public function country()
    {
        return $this->belongsTo(City::class, 'parent_id');
    }

    public function cities()
    {
        return $this->hasMany(City::class, 'parent_id');
    }

    public function attachments()
    {
        return $this->morphMany(Attachment::class, 'attachmentable')->where('is_main' , 0);
    }
    public function image()
    {
        return $this->morphOne(Attachment::class, 'attachmentable')->where('is_main' , 1);
    }

    public function trips()
    {
        return $this->hasMany(Trip::class);
    }
    public function seo()
    {
        return $this->morphOne(seo::class , 'seoable');
    }
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }
}
     