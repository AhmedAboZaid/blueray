<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SocialMedia extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = ['name', 'url'];

    public function getIconAttribute()
    {
        $icons = [
            "facebook"   => "FaFacebook",
            "instagram"  => "FaInstagram",
            "youtube"    => "FaYoutube",
            "linkedin"   => "FaLinkedin",
            "twitter"    => "FaTwitter",
            "whatsapp"   => "FaWhatsapp",
            "blogger"    => "FaBloggerB",
            "tiktok"     => "FaTiktok",
            "snapchat"   => "FaSnapchat",
            "pinterest"  => "FaPinterest",
            "reddit"     => "FaReddit",
        ];
        return @$icons[strtolower( $this->name )] ?? $this->name;

    }

}
