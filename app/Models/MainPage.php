<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class MainPage extends Model implements TranslatableContract
{
    use HasFactory, Translatable, SoftDeletes;

    protected $fillable = ['fixed_name','parent_id'];
    public $translatedAttributes = ['title', 'description' ,'image', 'alt'];
    public function seo()
    {
        return $this->morphOne(seo::class , 'seoable');
    }
    /**
     * Get all of the childs for the MainPage
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function parent()
    {
        return $this->belongsTo(MainPage::class, 'parent_id');
    }

    public function childs()
    {
        return $this->hasMany(MainPage::class, 'parent_id');
    }

    public function getImageUrlAttribute()
    {
        return asset("uploads/main-pages/".$this->image);
    }
}

