<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Tag extends Model implements TranslatableContract
{
    use HasFactory, SoftDeletes, Translatable;

    protected $guarded = [];
    public $translatedAttributes = ['title'];

    public function blogs()
    {
        return $this->morphedByMany(Blog::class, 'taggable');
    }
}
