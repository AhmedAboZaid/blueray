<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use HasFactory;
    protected $table = 'booking';

    protected $fillable = ['name','phone','code','email','child','adult','message','package_id'];

    public function packages()
    {
        return $this->hasMany(Package::class, 'package_id');
    }
}
