<?php

namespace App\Models;

use Astrotomic\Translatable\Contracts\Translatable as ContractsTranslatable;
use Astrotomic\Translatable\Translatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Branch extends Model implements ContractsTranslatable
{
    use HasFactory, Translatable;

    protected $fillable = [
        'phone',
        'fax',
        'email',
        'facebook',
        'instagram',
        'twitter',
        'whatsapp',
    ];
    public $translatedAttributes = ['name','address'];

    // public function socialMedia(){
    //     return $this->morphMany(SocialMedia::class, '');
    // }
    public function image()
    {
         return $this->morphOne(Attachment::class, 'attachmentable')->where('is_main' , 1);
    }
}
