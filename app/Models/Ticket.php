<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;

    protected $fillable = ['from_date','to_date','ticket_type'];

   /**
    * Get all of the destinations for the Ticket
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function destinations()
   {
       return $this->hasMany(TicketDestination::class, 'ticket_id');
   }

   /**
    * Get all of the persons for the Ticket
    *
    * @return \Illuminate\Database\Eloquent\Relations\HasMany
    */
   public function persons()
   {
       return $this->hasMany(TicketPerson::class,'ticket_id');
   }
}
