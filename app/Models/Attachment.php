<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Attachment extends Model implements TranslatableContract
{
    use HasFactory, SoftDeletes, Translatable;

    protected $fillable = ['file','is_main'];
    public $translatedAttributes = ['alts'];

    public function attachmentable()
    {
        return $this->morphTo();
    }
    public function getUrlAttribute()
    {
        return asset('/uploads/attachments/'.$this->file);
    }
    public function setFileAttribute($file)
    {
        if($file && $this->file)
        {
            // @unlink(public_path("uploads/attachments/".$this->file));
        }
        if(is_object($file))
        {
            $this->attributes['file'] =  $this->uploadImage($file,'uploads/attachments');
        }
        else
        {
            $this->attributes['file'] =  $file;
        }
    }

    public function uploadImage($image,$folder)
    {
        $rand       =   $this->id ? $this->id : Attachment::max('id') + rand(1,20);
        $image_name =  time().'_' . $rand . '.' . $image->getClientOriginalExtension();
        $destination = $folder;
        $image->move(public_path($destination),$image_name);
        return  $image_name ;
    }

    public static function handleRequest($attachments)
    {
        $result     = [];
        $languages  = Language::where('isActive',1)->get();

        if(is_array($attachments))
        {
            if(!@$attachments[0] || !is_array(@$attachments[0]))
            {
                $alts = [] ;
                foreach($languages as $language)
                {
                    $alts["$language->code"]  = ['alts' => @$attachments[$language->code .'alts']];
                }
                if(@$attachments['file'])
                {
                    $alts['file'] = @$attachments['file'];
                }
                $result= $alts;
            }
            else
            {
                foreach ($attachments as $key => $attachment)
                {
                    $alts = [] ;
                    foreach($languages as $language)
                    {
                        $alts["$language->code"]  = ['alts' => @$attachment[$language->code .'alts']];
                    }
                    if(@$attachment['file'])
                    {
                        $alts['file'] = @$attachment['file'];
                    }
                    $result[] = $alts;
                }
            }
        }
        return $result;
    }
}
