<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketPerson extends Model
{
    use HasFactory;
    protected $fillable = ['name','birthday','ticket_id'];

    /**
     * Get the ticket that owns the TicketPerson
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id');
    }
}
