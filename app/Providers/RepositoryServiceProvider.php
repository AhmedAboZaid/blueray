<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'App\Http\Interfaces\LanguageInterface',
            'App\Http\Repositories\LanguageRepository',
        );

        $this->app->bind(
            'App\Http\Interfaces\SliderInterface',
            'App\Http\Repositories\SliderRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\MainPageInterface',
            'App\Http\Repositories\MainPageRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\SocialMediaInterface',
            'App\Http\Repositories\SocialMediaRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\FaqInterface',
            'App\Http\Repositories\FaqRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\VideoInterface',
            'App\Http\Repositories\VideoRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\ClientInterface',
            'App\Http\Repositories\ClientRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\TagInterface',
            'App\Http\Repositories\TagRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\TeamMemberInterface',
            'App\Http\Repositories\TeamMemberRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\TimelineInterface',
            'App\Http\Repositories\TimelineRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\SectionInterface',
            'App\Http\Repositories\SectionRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\AlbumInterface',
            'App\Http\Repositories\AlbumRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\BlogInterface',
            'App\Http\Repositories\BlogRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\ProjectInterface',
            'App\Http\Repositories\ProjectRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\CityInterface',
            'App\Http\Repositories\CityRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\PackageInterface',
            'App\Http\Repositories\PackageRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\ServiceInterface',
            'App\Http\Repositories\ServiceRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\SettingInterface',
            'App\Http\Repositories\SettingRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\TourismTypeInterface',
            'App\Http\Repositories\TourismTypeRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\FacilityInterface',
            'App\Http\Repositories\FacilityRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\BranchInterface',
            'App\Http\Repositories\BranchRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\LayoutInterface',
            'App\Http\Repositories\LayoutRepository',
        );
        $this->app->bind(
            'App\Http\Interfaces\FormInterface',
            'App\Http\Repositories\FormRepository',
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
