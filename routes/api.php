<?php

use App\Http\Controllers\Front\ServiceController;
use App\Http\Controllers\Front\AboutController;
use App\Http\Controllers\Front\AlbumController;
use App\Http\Controllers\Front\BlogController;
use App\Http\Controllers\Front\BranchController;
use App\Http\Controllers\Front\CityController;
use App\Http\Controllers\Front\ContactController;
use App\Http\Controllers\Front\FacilityController;
use App\Http\Controllers\Front\HomeController;
use App\Http\Controllers\Front\LayoutbarController;
use App\Http\Controllers\Front\MainPageController;
use App\Http\Controllers\Front\SettingController;
use App\Http\Controllers\Front\SliderController;
use App\Http\Controllers\Front\SocialMediaController;
use App\Http\Controllers\Front\TagController;
use App\Http\Controllers\Front\TourismTypeController;
use App\Http\Controllers\Front\PackageController;
use App\Http\Controllers\Front\VideoController;
use App\Http\Controllers\Front\FormController;
use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['prefix' => LaravelLocalization::setLocale(request()->segment(2)) ?: config('app.locale') ], function ()
{
    // Route::get('/slider',[SliderController::class,'index']);
    // Route::get('/social-media',[SocialMediaController::class,'index']);
    // Route::get('/cities',[CityController::class,'index']);
    // Route::get('/facilities',[FacilityController::class,'index']);
    // Route::get('/tags',[TagController::class,'index']);
    // Route::get('/tourism-type',[TourismTypeController::class,'index']);
    // Route::get('/videos',[VideoController::class,'index']);
    // Route::get('/blogs',[BlogController::class,'index']);
    // Route::get('/blog/{slug}',[BlogController::class,'show']);
    // Route::get('/albums'            ,   [AlbumController::class,'index']);
    // Route::get('/albums/{slug}'     ,   [AlbumController::class,'show']);
    // Route::get('/setting',[SettingController::class,'index']);
    // Route::get('/contact-us',[ContactController::class,'index']);
    // Route::get('/trips',[TripController::class,'index']);
    // Route::get('/branch',[BranchController::class,'index']);


    // api front
    Route::get('/'                  ,          [HomeController::class,'index']);
    Route::get('/navbar'            ,          [LayoutbarController::class,'navbar']);
    Route::get('/footer'            ,          [LayoutbarController::class,'footer']);
    Route::get('/ticketing'         ,          [MainPageController::class,'ticketing']);
    Route::get('/tourisms'          ,          [TourismTypeController::class,'index']);
    Route::get('/transportation'    ,          [MainPageController::class,'transportation']);
    Route::get('/tourisms/{slug}'   ,          [TourismTypeController::class,'package']);
    Route::get('/packages/{slug}'   ,          [PackageController::class,'package']);
    Route::get('/services'          ,          [ServiceController::class,'index']);
    Route::get('/about'             ,          [AboutController::class,'index']);
    Route::get('/contact-us'        ,          [ContactController::class,'index']);
    Route::get('/media'             ,          [MainPageController::class,'media']);
    Route::get('/albums'            ,          [AlbumController::class,'index']);
    Route::get('/albums/{slug}'     ,          [AlbumController::class,'show']);
    Route::get('/videos'            ,          [VideoController::class,'index']);
    Route::get('/blogs'             ,          [BlogController::class,'index']);
    Route::get('/blogs/{slug}'      ,          [BlogController::class,'show']);
    Route::get('/destination'       ,          [MainPageController::class,'destination']);


});
Route::post('/contact'          ,          [FormController::class,'contact']);
Route::post('/booking'          ,          [FormController::class,'bookForm']);
Route::post('/ticket'          ,          [FormController::class,'ticketForm']);
