<?php

use App\Http\Controllers\Admin\AlbumController;
use App\Http\Controllers\Admin\BlogController;
use App\Http\Controllers\Admin\BranchController;
use App\Http\Controllers\Admin\CityController;
use App\Http\Controllers\Admin\ClientController;
use App\Http\Controllers\Admin\FacilityController;
use App\Http\Controllers\Admin\FAQController;
use App\Http\Controllers\Admin\LanguageController;
use App\Http\Controllers\Admin\MainPageController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\ProjectController;
use App\Http\Controllers\Admin\SectionController;
use App\Http\Controllers\Admin\ServiceController;
use App\Http\Controllers\Admin\SettingController;
use App\Http\Controllers\Admin\SliderController;
use App\Http\Controllers\Admin\SocialMediaController;
use App\Http\Controllers\Admin\TagController;
use App\Http\Controllers\Admin\TeamMeamberController;
use App\Http\Controllers\Admin\TimelineController;
use App\Http\Controllers\Admin\TourismTypeController;
use App\Http\Controllers\Admin\VideoController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();
Route::get('/', function () {
    return auth::check() ? view('admin.dashboard') : view('auth.login');
});

Route::get('admin', function () {
    return view('admin.dashboard');
})->middleware('auth');


Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    /** Languages */
    Route::group(['prefix' => 'languages'], function() {
        Route::get('/',[LanguageController::class,'index'])->name('languages.index');
        Route::get('create',[LanguageController::class,'create'])->name('languages.create');
        Route::post('store',[LanguageController::class,'store'])->name('languages.store');
        Route::get('edit/{language}',[LanguageController::class,'edit'])->name('languages.edit');
        Route::post('update',[LanguageController::class,'update'])->name('languages.update');
        Route::post('delete',[LanguageController::class,'destroy'])->name('languages.delete');
        Route::post('status',[LanguageController::class,'changeStatus'])->name('languages.status');
    });
    /** sliders */
    Route::group(['prefix' => 'sliders'], function() {
        Route::get('/',[SliderController::class,'index'])->name('sliders.index');
        Route::get('create',[SliderController::class,'create'])->name('sliders.create');
        Route::post('store',[SliderController::class,'store'])->name('sliders.store');
        Route::get('edit/{slider}',[SliderController::class,'edit'])->name('sliders.edit');
        Route::post('update',[SliderController::class,'update'])->name('sliders.update');
        Route::post('delete',[SliderController::class,'destroy'])->name('sliders.delete');
    });
    /** Main Pages */
    Route::group(['prefix' => 'main-pages'], function() {
        Route::get('/',[MainPageController::class,'index'])->name('main-pages.index');
        Route::get('edit/{mainPage}',[MainPageController::class,'edit'])->name('main-pages.edit');
        Route::post('update',[MainPageController::class,'update'])->name('main-pages.update');
    });
    /** Social Media */
    Route::group(['prefix' => 'social-media'], function() {
        Route::get('/',[SocialMediaController::class,'index'])->name('social.media.index');
        Route::get('create',[SocialMediaController::class,'create'])->name('social.media.create');
        Route::post('store',[SocialMediaController::class,'store'])->name('social.media.store');
        Route::get('edit/{socialMedia}',[SocialMediaController::class,'edit'])->name('social.media.edit');
        Route::post('update',[SocialMediaController::class,'update'])->name('social.media.update');
        Route::post('delete',[SocialMediaController::class,'destroy'])->name('social.media.delete');
    });


    /** tags */
    Route::group(['prefix' => 'tags'], function() {
        Route::get('/',[TagController::class,'index'])->name('tags.index');
        Route::get('create',[TagController::class,'create'])->name('tags.create');
        Route::post('store',[TagController::class,'store'])->name('tags.store');
        Route::get('edit/{tag}',[TagController::class,'edit'])->name('tags.edit');
        Route::post('update',[TagController::class,'update'])->name('tags.update');
        Route::post('delete',[TagController::class,'destroy'])->name('tags.delete');
    });
    /** About */
    Route::group(['prefix' => 'about/'], function() {
        /** team members */
        Route::group(['prefix' => 'team-members'], function() {
            Route::get('/',[TeamMeamberController::class,'index'])->name('team.members.index');
            Route::get('create',[TeamMeamberController::class,'create'])->name('team.members.create');
            Route::post('store',[TeamMeamberController::class,'store'])->name('team.members.store');
            Route::get('edit/{teamMember}',[TeamMeamberController::class,'edit'])->name('team.members.edit');
            Route::post('update',[TeamMeamberController::class,'update'])->name('team.members.update');
            Route::post('delete',[TeamMeamberController::class,'destroy'])->name('team.members.delete');
        });

        /** timeline */
        Route::group(['prefix' => 'timeline'], function() {
            Route::get('/',[TimelineController::class,'index'])->name('timeline.index');
            Route::get('create',[TimelineController::class,'create'])->name('timeline.create');
            Route::post('store',[TimelineController::class,'store'])->name('timeline.store');
            Route::get('edit/{timeline}',[TimelineController::class,'edit'])->name('timeline.edit');
            Route::post('update',[TimelineController::class,'update'])->name('timeline.update');
            Route::post('delete',[TimelineController::class,'destroy'])->name('timeline.delete');
        });

        /** Sections */
        Route::group(['prefix' => 'sections'], function() {
            Route::get('/',[SectionController::class,'index'])->name('section.index');
            Route::get('create',[SectionController::class,'create'])->name('section.create');
            Route::post('store',[SectionController::class,'store'])->name('section.store');
            Route::get('edit/{section}',[SectionController::class,'edit'])->name('section.edit');
            Route::post('update',[SectionController::class,'update'])->name('section.update');
            Route::post('delete',[SectionController::class,'destroy'])->name('section.delete');
        });
    });
    /** Media Center */
    Route::group(['prefix' => 'media-center/'], function() {
        /** videos */
        Route::group(['prefix' => 'videos'], function() {
            Route::get('/',[VideoController::class,'index'])->name('videos.index');
            Route::get('create',[VideoController::class,'create'])->name('videos.create');
            Route::post('store',[VideoController::class,'store'])->name('videos.store');
            Route::get('edit/{video}',[VideoController::class,'edit'])->name('videos.edit');
            Route::post('update',[VideoController::class,'update'])->name('videos.update');
            Route::post('delete',[VideoController::class,'destroy'])->name('videos.delete');
        });
        /** Albums */
        Route::group(['prefix' => 'albums'], function() {
            Route::get('/',[AlbumController::class,'index'])->name('albums.index');
            Route::get('create',[AlbumController::class,'create'])->name('albums.create');
            Route::post('store',[AlbumController::class,'store'])->name('albums.store');
            Route::get('edit/{album}',[AlbumController::class,'edit'])->name('albums.edit');
            Route::post('update',[AlbumController::class,'update'])->name('albums.update');
            Route::post('delete',[AlbumController::class,'destroy'])->name('albums.delete');
        });
         /** Blogs */
        Route::group(['prefix' => 'blogs'], function() {
            Route::get('/',[BlogController::class,'index'])->name('blogs.index');
            Route::get('create',[BlogController::class,'create'])->name('blogs.create');
            Route::post('store',[BlogController::class,'store'])->name('blogs.store');
            Route::get('edit/{blog}',[BlogController::class,'edit'])->name('blogs.edit');
            Route::post('update',[BlogController::class,'update'])->name('blogs.update');
            Route::post('delete',[BlogController::class,'destroy'])->name('blogs.delete');
        });
    });

    /** Cities */
    Route::group(['prefix'          => 'cities']    , function() {
        Route::get('/'              ,[CityController::class,'index'])    ->name('cities.index');
        Route::get('create'         ,[CityController::class,'create'])   ->name('cities.create');
        Route::post('store'         ,[CityController::class,'store'])    ->name('cities.store');
        Route::get('edit/{project}' ,[CityController::class,'edit'])     ->name('cities.edit');
        Route::post('update'        ,[CityController::class,'update'])   ->name('cities.update');
        Route::post('delete'        ,[CityController::class,'destroy'])  ->name('cities.delete');
    });

    /** packages */
    Route::group(['prefix'          => 'packages']    , function() {
        Route::get('/'              ,[PackageController::class,'index'])    ->name('packages.index');
        Route::get('create'         ,[PackageController::class,'create'])   ->name('packages.create');
        Route::get('cities/{parent_id?}',[PackageController::class,'cities'])   ->name('packages.cities');
        Route::post('store'         ,[PackageController::class,'store'])    ->name('packages.store');
        Route::get('edit/{package}' ,[PackageController::class,'edit'])     ->name('packages.edit');
        Route::post('update'        ,[PackageController::class,'update'])   ->name('packages.update');
        Route::post('delete'        ,[PackageController::class,'destroy'])  ->name('packages.delete');
    });
    /** services */
    Route::group(['prefix' => 'services'], function() {
        Route::get('/',[ServiceController::class,'index'])->name('services.index');
        Route::get('create',[ServiceController::class,'create'])->name('services.create');
        Route::post('store',[ServiceController::class,'store'])->name('services.store');
        Route::get('edit/{service}',[ServiceController::class,'edit'])->name('services.edit');
        Route::post('update',[ServiceController::class,'update'])->name('services.update');
        Route::post('delete',[ServiceController::class,'destroy'])->name('services.delete');
    });
    /** Settings */
    Route::group(['prefix' => 'settings'], function() {
        Route::get('/',[SettingController::class,'index'])->name('settings.index');
        Route::get('edit',[SettingController::class,'edit'])->name('settings.edit');
        Route::post('update',[SettingController::class,'update'])->name('settings.update');
    });

    /** tourism type */
    Route::group(['prefix' => 'tourism-type'], function() {
        Route::get('/',[TourismTypeController::class,'index'])->name('tourism.type.index');
        Route::get('create',[TourismTypeController::class,'create'])->name('tourism.type.create');
        Route::post('store',[TourismTypeController::class,'store'])->name('tourism.type.store');
        Route::get('edit/{tourismType}',[TourismTypeController::class,'edit'])->name('tourism.type.edit');
        Route::post('update',[TourismTypeController::class,'update'])->name('tourism.type.update');
        Route::post('delete',[TourismTypeController::class,'destroy'])->name('tourism.type.delete');
    });
    /** facility */
    Route::group(['prefix' => 'facility'], function() {
        Route::get('/',[FacilityController::class,'index'])->name('facility.index');
        Route::get('create',[FacilityController::class,'create'])->name('facility.create');
        Route::post('store',[FacilityController::class,'store'])->name('facility.store');
        Route::get('edit/{facility}',[FacilityController::class,'edit'])->name('facility.edit');
        Route::post('update',[FacilityController::class,'update'])->name('facility.update');
        Route::post('delete',[FacilityController::class,'destroy'])->name('facility.delete');
    });
    /** branches */
    Route::group(['prefix' => 'branch'], function() {
        Route::get('/',[BranchController::class,'index'])->name('branch.index');
        Route::get('create',[BranchController::class,'create'])->name('branch.create');
        Route::post('store',[BranchController::class,'store'])->name('branch.store');
        Route::get('edit/{branch}',[BranchController::class,'edit'])->name('branch.edit');
        Route::post('update',[BranchController::class,'update'])->name('branch.update');
        Route::post('delete',[BranchController::class,'destroy'])->name('branch.delete');
    });

});
