$("#pac-input").focusin(function() {
    $(this).val('');
});
$('#latitude').val('');
$('#longitude').val('');
// This example adds a search box to a map, using the Google Place Autocomplete
// feature. People can enter geographical searches. The search box will return a
// pick list containing a mix of places and predicted search terms.
// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
    // let latitude   = 30.013496494032935 ;
    // let longitude  = 31.254035310710268;
    let latitude   = +(typeof project_center_lat != 'undefined' ? project_center_lat : 30.013496494032935);
    let longitude  = +(typeof project_center_lng != 'undefined' ? project_center_lng : 31.254035310710268);
    console.log(">>>>" , latitude , longitude );
    var zoom = 12 ;
    var map2 ;
    var marker ;
    var markers =[];

function initAutocomplete() {
     map2 = new google.maps.Map(document.getElementById('map'), {
        center: {lat: latitude, lng: longitude },
        zoom: 5,
        mapTypeId: 'roadmap',
    });

    // move pin and current location
    infoWindow = new google.maps.InfoWindow;
    geocoder = new google.maps.Geocoder();
    // geocoder = new google.maps.Polygon();  // MA
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
                lat: latitude,
                lng: longitude
            };
            map2.setCenter(pos);
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(pos),
                map: map2,
                title: 'موقعك الحالي'
            });

            markers.push(marker);
            marker.addListener('click', function() {
                geocodeLatLng(geocoder, map2, infoWindow,marker);
            });
            // to get current position address on load
            google.maps.event.trigger(marker, 'click');
        }, function() {
            handleLocationError(true, infoWindow, map2.getCenter());
        });
    } else {
        // Browser doesn't support Geolocation
        handleLocationError(false, infoWindow, map2.getCenter());
    }
    // var geocoder = new google.maps.Geocoder(); // MA

    if(typeof map_without_click == 'undefined'){
        google.maps.event.addListener(map2, 'click', function(event) {
            SelectedLatLng = event.latLng;
            geocoder.geocode({
                'latLng': event.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        deleteMarkers();
                        addMarkerRunTime(event.latLng);
                        SelectedLocation = results[0].formatted_address;

                        splitLatLng(String(event.latLng));
                        $("#pac-input").val(results[0].formatted_address);
                    }
                }
            });
        });
    }
    function geocodeLatLng(geocoder, map2, infowindow,markerCurrent) {
        var latlng = {lat: markerCurrent.position.lat(), lng: markerCurrent.position.lng()};
        $('#latitude').val(markerCurrent.position.lat());
        $('#longitude').val(markerCurrent.position.lng());
        geocoder.geocode({'location': latlng}, function(results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    map2.setZoom(11);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: map2
                    });
                    markers.push(marker);
                    infowindow.setContent(results[0].formatted_address);
                    SelectedLocation = results[0].formatted_address;
                    $("#pac-input").val(results[0].formatted_address);
                    infowindow.open(map2, marker);
                } else {
                    window.alert('No results found');
                }
            } else {
                window.alert('Geocoder failed due to: ' + status);
            }
        });
        SelectedLatLng =(markerCurrent.position.lat(),markerCurrent.position.lng());
        // initAutocomplete();
    }
    function addMarkerRunTime(location) {
        marker = new google.maps.Marker({
            position: location,
            map: map2
        });
        markers.push(marker);
    }
    function setMapOnAll(map2) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map2);
        }
    }
    function clearMarkers() {
        setMapOnAll(null);
    }
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }
    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    $("#pac-input").val("أبحث هنا ");
    var searchBox = new google.maps.places.SearchBox(input);
    map2.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);
    // Bias the SearchBox results towards current map's viewport.
    map2.addListener('bounds_changed', function() {
        searchBox.setBounds(map2.getBounds());
    });
    // var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();
        if (places.length == 0) {
            return;
        }
        // Clear out the old markers.
        markers.forEach(function(marker) {
            marker.setMap(null);
        });
        markers = [];
        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
            if (!place.geometry) {
                console.log("Returned place contains no geometry");
                return;
            }
            var icon = {
                url: place.icon,
                size: new google.maps.Size(100, 100),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(17, 34),
                scaledSize: new google.maps.Size(25, 25)
            };
            // Create a marker for each place.
            markers.push(new google.maps.Marker({
                map: map2,
                icon: icon,
                title: place.name,
                position: place.geometry.location
            }));
            $('#latitude').val(place.geometry.location.lat());
            $('#longitude').val(place.geometry.location.lng());
            if (place.geometry.viewport) {
                // Only geocodes have viewport.
                bounds.union(place.geometry.viewport);
            } else {
                bounds.extend(place.geometry.location);
            }
        });
        map2.fitBounds(bounds);
    });
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
    infoWindow.setPosition(pos);
    infoWindow.setContent(browserHasGeolocation ?
        'Error: The Geolocation service failed.' :
        'Error: Your browser doesn\'t support geolocation.');
    infoWindow.open(map2);
}
var latlong = [];
function splitLatLng(latLng){
    var newString = latLng.substring(0, latLng.length-1);
    var newString2 = newString.substring(1);
    var trainindIdArray = newString2.split(',');
    var lat = parseFloat(trainindIdArray[0]);
    var lng = parseFloat(trainindIdArray[1]);
    let newPosition = {
        lat,
        lng
    }
    latlong.push(newPosition);

    $("#latitude").val(lat);
    $("#longitude").val(lng);

    drow();
}

function drow()
{
    $("#tbody").html('');
    var html,lat_input,lng_input ;
    var id =0;
    latlong.forEach(function(element){
        id++;
        lat_input = '<input type="hidden" name ="lat[]" value="'+ element.lat+'" />';
        lng_input = '<input type="hidden" name ="lng[]" value="'+ element.lng+'" />';
        html +='<tr  onclick="showPoint('+id+')" id="'+id+'"> <td>'+id+lat_input+lng_input+'</td><td>'+ element.lat+'</td><td>'+ element.lng+'</td><td><button onclick="deleteElement('+id+')" class="btn_delete btn btn-danger"><i class="fa-solid fa-trash-can"></i></button></td></tr>';
    });
    $("#tbody").append(html);
    setTimeout(() => {
        showPoint(id);

    }, 100);
}

function deleteElement(id)
{
    $("#" + id).remove();
    latlong.splice(id-1, 1);
    drow();
}

let flightPath =0 ;
var newMarker =0;
function showPoint(id)
{
    try {
        var object = latlong[id-1];

        if(flightPath != 0)
        {
            flightPath.setMap(null);
        }
        flightPath = new google.maps.Polygon({
            path: latlong,
            geodesic: true,
            strokeColor: 'green',
            strokeOpacity: 1,
            strokeWeight: 1,
            fillColor: 'green',
            fillOpacity: .2,
        });
        setMarker(object);

    } catch (error) {
        console.log('showPoint' , error);
    }
}
function setMarker(object)
{
    try {
        if(marker){
            marker.setMap(null);
        }
        flightPath.setMap(map2);

        if(newMarker != 0)
        {
            newMarker.setMap(null);
        }

        newMarker = new google.maps.Marker({
            position: {lat: object.lat, lng: object.lng },
        });
        newMarker.setMap(map2);
    } catch (error) {
        console.log('setMarker' ,error);
    }
}


