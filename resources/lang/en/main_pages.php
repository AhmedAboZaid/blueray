<?php

return [

    'main_pages' => 'Main Pages',
    'edit' => 'Edit',
    'image' => 'Image',
    'alt' => 'Alt',
    'btn' => 'BTN',
    'title' =>'Title',
    'description' =>'Description',
    'actions' => 'Actions',
    'submit'=>'Update',
    'in'=>'in',
    'edit_main_page' =>'Edit Main Page',
    'actions' => 'Actions',
    'fixed_name' => 'Fixed Name',
    'meta_title' =>'Meta Title',
    'meta_keywords' =>'Meta Keywords',
    'meta_description' =>'Meta Descriptions',
    'slug' =>'Slug',

   

];
