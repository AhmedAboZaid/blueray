<?php

return [

    'sliders' => 'Sliders',
    'new_slider' => 'Add New Slider',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'image' => 'Image',
    'alt' => 'Alt',
    'btn' => 'link',
    'title' =>'Title',
    'short_description' =>'Short Description',
    'actions' => 'Actions',
    'submit'=>'Save',
    'update'=>'Update',
    'in'=>'in',
    'edit_slider' =>'Edit Slider',
    'actions' => 'Actions',

   

];
