<?php

return [

    'languages' => 'Languages',
    'new_language' => 'New Language',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'language' => 'Language',
    'flag' => 'Flag',
    'status' => 'Status',
    'actions' => 'Actions',
    'active' =>'Active',
    'submit'=>'Save',
    'update'=>'Update',
    'update_language'=>'Update Language',
    'code' =>'Code',

];
