<?php

return [

    'main_pages' => 'الصفحات الرئيسية',
    'edit' => 'تعديل',
    'image' => 'صورة',
    'alt' => 'وصف الصورة',
    'title' =>'العنوان',
    'description' =>'وصف ',
    'submit'=>'حفظ',
    'in'=>'ب',
    'edit_main_page' =>'تعديل صفحة رئيسية',
    'actions' => 'العمليات',
    'fixed_name' => 'اسم ثابت',
    'meta_title' =>'Meta Title',
    'meta_keywords' =>'Meta Keywords',
    'meta_description' =>'Meta Descriptions',
    'slug' =>'Slug',

 



];
