<?php

return [

    'languages' => 'اللغات',
    'new_language' => 'أضف لغة جديدة',
    'edit' => 'تعديل',
    'delete' =>'حذف',
    'language' => 'اللغة',
    'flag' => 'العلم',
    'status' => 'الحاله',
    'actions' => 'العمليات',
    'active' =>'فعال',
    'submit'=>'حفظ',
    'update_language'=>'تعديل اللغة',
    'code' =>'الكود',




];
