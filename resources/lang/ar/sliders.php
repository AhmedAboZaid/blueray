<?php

return [

    'sliders' => 'غلاف',
    'new_slider' => 'أضف غلاف جديد',
    'edit' => 'تعديل',
    'delete' =>'حذف',
    'image' => 'صورة',
    'alt' => 'وصف الصورة',
    'btn' => 'اضف لينك',
    'title' =>'العنوان',
    'short_description' =>'وصف قصير',
    'active' =>'فعال',
    'submit'=>'حفظ',
    'in'=>'ب',
    'edit_slider' =>'تعديل الغلاف',
    'actions' => 'العمليات',
 



];
