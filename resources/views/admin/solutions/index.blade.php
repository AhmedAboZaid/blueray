@extends('layouts.admin_main_page')

@section('page_title','Languages')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
		<!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="icon-xl la la-language text-primary"></i>
                    </span>                         
                    <h3 class="card-label">Languages</h3>
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{route('languages.create')}}" class="btn btn-primary font-weight-bolder">
                        <i class="la la-plus"></i>
                        New Record
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table table-separate table-head-custom collapsed" id="Lang_table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Language</th>
                            <th>Flag</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($languages as $language )
                        <tr>
                            <td>{{$language->id}}</td>
                            <td>{{$language->language}}</td>
                            <td> 
                                @if ($language->flag)
                                <img  style="width: 40px; height: 40px;" src="{{asset('uploads/languages/'.$language->flag)}}">
                                @else
                                <img  style="width: 40px; height: 40px;" src="{{asset('uploads/languages/none.png')}}">
                                    
                                @endif
                            </td>
                            <td>
                                @if ($language->isActive === 1)
                                    <label class="badge badge-success"> 
                                        Active
                                    </label>
                                @else
                                    <label class="badge badge-danger">
                                        Not Active
                                    </label>
                                @endif
                            </td>
                            <td>
                                <a href="{{route('languages.edit',$language->id)}}" class="btn btn-light btn-circle btn-sm btn-shadow mr-2">
                                    <i class="flaticon2-gear text-primary"></i>
                                    Edit 
                                </a>

                                <a href="{{route('languages.delete',$language->id)}}" class="btn btn-danger btn-circle btn-sm btn-shadow mr-2" onclick="deleteData()" >
                                    <i class="flaticon2-trash"></i> 
                                    Delete 
                                </a>
                            </td>
                        </tr>
                        @endforeach
                           
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>Language</th>
                            <th>Flag</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                    </tfoot>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

{{-- <script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script> --}}

    <script>
        var Lang_table_responsive = function() {

        var Lang_table = function() {
            var table = $('#Lang_table');

            // begin first table
            table.DataTable({
                responsive: true,
                columnDefs: [
                    {
                        targets: 2,
                        orderable: false,
                    
                    },
                    {
                        targets: 4,
                        orderable: false,
                    }
			    ]
                
            });
        };

        return {
            //main function to initiate the module
            init: function() {
                Lang_table();
            }
        };
        }();

        jQuery(document).ready(function() {
            Lang_table_responsive.init();
        });

    </script>
@endsection