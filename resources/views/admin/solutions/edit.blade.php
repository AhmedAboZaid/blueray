@extends('layouts.admin_main_page')

@section('page_title','Languages')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
		<!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="flaticon2-bell text-primary"></i>
                    </span>
                    <h3 class="card-label">Add New Language</h3>
                </div>
                <div class="card-toolbar">
                </div>
            </div>
            <div class="card-body">
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
              @endif
                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <!--begin::Form-->
                <form class="form" method="POST" action="{{route('languages.update')}}" enctype="multipart/form-data">
                    @csrf

                    <input type="hidden" name="id" value="{{$language->id}}">
                    <div class="card-body">

                        <div class="form-group">
                            <label>Language</label>
                            <div class="input-group">
                                <input type="text" name="language" value="{{$language->language}}" class="form-control @error('language') is-invalid @enderror" placeholder="Enter Laguage Name" />
                            </div>
                            @error('language')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label>Flag</label>
                            <div class="input-group">
                                <input type="file" name="flag" class="form-control @error('flag') is-invalid @enderror"  placeholder="Enter Language Flag" aria-describedby="basic-addon2" />
                            </div>
                            @error('flag')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                            <img style="width: 100px;height: 100px;" src="{{asset('uploads/languages/'. $language->flag) }}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Active Or Not</label>
                        <div class="checkbox-list">
                            <label class="checkbox">
                                @if ($language->isActive == 1)
                                <input type="checkbox" checked class="form-check-input" name="isActive" id="isActive">
                                @else
                                <input type="checkbox" class="form-check-input" name="isActive" id="isActive">
                                @endif
                            <span></span>Active</label>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    </div>
                </form>
                <!--end::Form-->

            </div>
        </div>
        <!--end::Card-->
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
@endsection