@extends('layouts.admin_main_page')

@section('page_title','Main Pages')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <form class="form" method="POST" action="{{route('main-pages.update')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$mainPage->id}}">
        <div class="container-fluid">
            <div class="row">
                @foreach ($languages as $key => $language )
                    <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="icon-xl fas fa-file text-primary"></i>
                                    </span>
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            {{ $mainPage->{'title:'.$language->code} }}
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>
                                        {{__('main_pages.title')}}
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[title]" required  value="{{ $mainPage->{'title:'.$language->code} }}" class="form-control @error('title') is-invalid @enderror" placeholder="Enter Title Value" />
                                    </div>
                                    @error("$language->code.title")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-md-12">
                                    <label >
                                        {{__('main_pages.description')}}
                                    <div class="card-body">
                                        <textarea class="editor @error('description') is-invalid @enderror" id="editor{{30+$key}}" name="{{$language->code}}[description]" id="">{{ $mainPage->{'description:'.$language->code} }}
                                        </textarea>
                                    </div>
                                    @error("$language->code.description")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="row col-md-12">
                                    <div class="form-group col-md-6">
                                        <label for="{{$language->code}}[alt]">
                                            {{__('main_pages.alt')}}
                                        </label>
                                        <div class="input-group">
                                            <input type="text" name="{{$language->code}}[alt]" required value="{{ $mainPage->{'alt:'.$language->code} }}"  class="form-control @error('alt') is-invalid @enderror" placeholder="Enter Alt Value" />
                                        </div>
                                        @error("$language->code.alt")
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label >Image</label>
                                        <div class="col-lg-9 col-xl-6">
                                            <div class="image-input image-input-outline image-input-circle" id="kt_image_{{30+$key}}" >
                                                <div class="image-input-wrapper" style="background-image: url({{asset('uploads/main-pages/'. $mainPage->{'image:'.$language->code}) }})"></div>
                                                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                    <i class="fa fa-pen icon-sm text-muted" style="    margin-left: 30px;"></i>
                                                    <input type="file" name="{{$language->code}}[image]"  class="form-control @error('image') is-invalid @enderror" accept=".png, .jpg, .jpeg" />
                                                    @error('image')
                                                      <small class="form-text text-danger">{{$message}}</small>
                                                    @enderror
                                                    <input type="hidden" name="profile_avatar_remove" />
                                                </label>
                                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col">
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            Seo Section
                                        </span>
                                    </h3>
                                    <hr>
                                    <div class="row col-md-12">

                                        <div class="form-group col-md-6">
                                            <label>
                                                {{__('main_pages.meta_title')}}
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][meta_title]" required value="{{ $mainPage->seo->{'meta_title:'.$language->code} }}"  class="form-control @error('meta_title') is-invalid @enderror" placeholder="Enter Meta Title Value" />
                                            </div>
                                            @error("seo.$language->code.meta_title")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>
                                                {{__('main_pages.slug')}}
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][slug]" required value="{{ $mainPage->seo->{'slug:'.$language->code} }}"  class="form-control @error('slug') is-invalid @enderror" placeholder="Enter slug Value" />
                                            </div>
                                            @error("seo.$language->code.slug")
                                            <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>
                                                {{__('main_pages.meta_keywords')}}
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][meta_keywords]" required value="{{ $mainPage->seo->{'meta_keywords:'.$language->code} }}"  class="form-control @error('meta_keywords') is-invalid @enderror" placeholder="Enter Meta Keywords Value" />
                                            </div>
                                            @error("seo.$language->code.meta_keywords")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>
                                                {{__('main_pages.meta_description')}}
                                            </label>
                                            <div class="input-group">
                                                <textarea class="form-control @error('meta_description') is-invalid @enderror" name="seo[{{$language->code}}][meta_description]" id="">{{ $mainPage->seo->{'meta_description:'.$language->code} }}
                                                </textarea>
                                            </div>
                                            @error("seo.$language->code.meta_description")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-md-5"></div>
                <div class="col-md-7">
                    <button type="submit" class="btn btn-primary mr-2">
                        {{__('main_pages.submit')}}
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

<script>
        @foreach ($languages as $key =>$lang )
            new KTImageInput('kt_image_'+"{{30+$key}}");
        @endforeach
</script>

<script>
    @foreach ($languages as $key =>$lang )
    $('.editor'.{{30+$key}}).each(function(e){
                CKEDITOR.replace( this.id,  {
                    height: 150,
                    baseFloatZIndex: 10005,
                    removeButtons: 'PasteFromWord'
                });
            });
    @endforeach
</script>
@endsection
