@extends('layouts.admin_main_page')

@section('page_title','Main Pages')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
		<!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="icon-xl fas fa-pager text-primary"></i>
                    </span>
                    <h3 class="card-label">
                         {{__('main_pages.main_pages')}}
                    </h3>
                </div>
                <div id="successMessage" style="text-align:center; width:40%" class="mt-5">
                    @if(Session::has('success'))
                        <p class="alert alert-success">{{ Session::get('success') }}</p>
                    @endif
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table table-separate table-head-custom collapsed" id="Lang_table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{__('main_pages.fixed_name')}}</th>
                            <th>{{__('main_pages.title')}}</th>
                            <th>{{__('main_pages.image')}}</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($mainPages->where('parent_id' , '!=' , 1) as $mainPage )
                        <tr>
                            <td>{{$mainPage->id}}</td>
                            <td>{{$mainPage->fixed_name}}</td>
                            <td>{{$mainPage->title}} </td>
                            <td>
                                <img  style="width: 60px; height: 60px;" src="{{asset('uploads/main-pages/'.$mainPage->image)}}">
                            </td>
                            <td>
                                <a href="{{route('main-pages.edit',$mainPage->id)}}" class="btn btn-light btn-circle btn-sm btn-shadow mr-2">
                                    <i class="flaticon2-gear text-warning"></i>
                                    Edit
                                </a>
                            </td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

{{-- <script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script> --}}

    <script>
        var Lang_table_responsive = function() {

        var Lang_table = function() {
            var table = $('#Lang_table');

            // begin first table
            table.DataTable({
                responsive: true,
                columnDefs: [
                    {
                        targets: 2,
                        orderable: false,

                    },
                    {
                        targets: 4,
                        orderable: false,
                    }
			    ]

            });
        };

        return {
            //main function to initiate the module
            init: function() {
                Lang_table();
            }
        };
        }();

        jQuery(document).ready(function() {
            Lang_table_responsive.init();
        });
    </script>

@endsection
