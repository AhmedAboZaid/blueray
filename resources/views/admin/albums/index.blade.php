@extends('layouts.admin_main_page')

@section('page_title','Albums')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
		<!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="far fa-images text-primary"></i>
                    </span>                         
                    <h3 class="card-label">
                         Albums
                    </h3> 
                </div>
                <div id="successMessage" style="text-align:center; width:40%" class="mt-5">
                    @if(Session::has('success'))
                        <p class="alert alert-success">{{ Session::get('success') }}</p>
                    @endif
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{route('albums.create')}}" class="btn btn-primary font-weight-bolder">
                        <i class="la la-plus"></i>
                        Create Album
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table table-separate table-head-custom collapsed" id="Lang_table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Main Image</th>
                            <th>Title</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($albums as $album ) 

                        <tr>
                            <td>{{$album->id}}</td>
                            <td>
                                <img  style="width: 40px; height: 40px;" src="{{$album->image->url}}">
                            </td>
                            <td>{{$album->title}} </td>
                            <td>
                                <a href="{{route('albums.edit',$album->id)}}" class="btn btn-light btn-circle btn-sm btn-shadow mr-2">
                                    <i class="flaticon2-gear text-warning"></i>
                                    Edit
                                </a>
                                <a href="#" class="btn btn-danger btn-circle btn-sm btn-shadow mr-2" onclick="deleteData('{{ route('albums.delete')}}' ,{{$album->id}})" >
                                    <i class="flaticon2-trash"></i> 
                                    Delete 
                                </a>
                            </td>
                        </tr>
                        @endforeach
                           
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

{{-- <script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script> --}}

    <script>
        var Lang_table_responsive = function() {

        var Lang_table = function() {
            var table = $('#Lang_table');

            // begin first table
            table.DataTable({
                responsive: true,
                columnDefs: [
                    {
                        targets: 1,
                        orderable: false,
                    
                    },
                    {
                        targets: 3,
                        orderable: false,
                    }
			    ]
                
            });
        };

        return {
            //main function to initiate the module
            init: function() {
                Lang_table();
            }
        };
        }();

        jQuery(document).ready(function() {
            Lang_table_responsive.init();
        });
    </script>
@endsection