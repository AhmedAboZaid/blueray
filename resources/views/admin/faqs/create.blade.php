@extends('layouts.admin_main_page')

@section('page_title','FAQS')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
<form class="form" method="POST" action="{{route('faqs.store')}}" >
    @csrf
    <div class="container-fluid">
        <div class="row">		    
            @foreach ($languages as $key => $language )
            <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <div class="card-title">
                            <span class="card-icon">
                                <i class="far fa-question-circle text-primary"></i>
                            </span>
                            <h3 class="card-label">
                                <span class="font-weight-bolder text-info mb-4 text-hover-state-dark"> 
                                    FAQS {{ $language->language }}
                                </span>
                            </h3>
                        </div>                    
                    </div>                        
                    <div class="card-body">                                                                
                        <div class="form-group">
                            <label>
                                Questions
                            </label>
                            <div class="input-group">
                                <input type="text" name="{{$language->code}}[questions]" required  class="form-control @error('questions') is-invalid @enderror" placeholder="Enter questions Value" /></div>
                            @error("$language->code.questions")
                                <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>
                                Answers                                       
                            </label>
                            <div class="input-group">
                                <textarea class="form-control @error('answers') is-invalid @enderror" required  name="{{$language->code}}[answers]" >
                                </textarea>
                            </div>
                            @error("$language->code.answers")
                                <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                    </div>                        
                </div>                        
            </div>                
            @endforeach    
            <div class="form-group col-md-6">
                <label>
                    Parent
                </label>
                <div class="input-group">
                    <input type="text" name="Pid" required  class="form-control @error('Pid') is-invalid @enderror" placeholder="Enter Pid Value" />
                </div>
                @error("Pid")
                    <small class="form-text text-danger">{{$message}}</small>
                @enderror
            </div>  
            <div class="col-md-12 text-center">                    
                <button type="submit" class="btn btn-primary mr-2">
                    Save
                </button>
            </div>
        </div>
    </div>
</form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
@endsection