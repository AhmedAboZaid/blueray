@extends('layouts.admin_main_page')

@section('page_title','Projects')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <form class="form" method="POST" action="{{route('projects.update')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$project->id}}">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-body">
                            <div class="form-group">
                                <label >
                                    Main Iamge
                                </label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="image-input image-input-outline image-input-circle" id="kt_image_3" >
                                        <div class="image-input-wrapper" style="background-image: url({{asset(@$project->image->url) }})"></div>
                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input type="file" name="mainImg[file]" accept=".png, .jpg, .jpeg" />
                                            <input type="hidden" value="1" name="mainImg[is_main]"  />
                                            <input type="hidden" name="profile_avatar_remove" />
                                        </label>
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                    </div>
                                    @error("mainImg.file")
                                    <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <br>

                                <div class="row col-md-12">
                                    @foreach ($languages as $language)
                                        <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                            <label for="{{$language->code}}[alts]">
                                            Alt {{ $language->language }}
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="mainImg[{{$language->code}}][alts]" value="{{ @$project->image->{'alts:'.$language->code} }}" class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                            </div>
                                            @error("mainImg.$language->code.alts")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        @endforeach
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                @foreach ($languages as $key => $language )
                    <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="far fa-images text-primary"></i>
                                    </span>
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            project {{ $language->language }}
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>
                                      Title
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[title]" required  value="{{ $project->{'title:'.$language->code} }}"  class="form-control @error('title') is-invalid @enderror" placeholder="Enter Title Value" />
                                    </div>
                                    @error("$language->code.title")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label >
                                        Description
                                    </label>
                                    <div class="input-group">
                                        <textarea class="form-control @error('description') is-invalid @enderror" name="{{$language->code}}[description]" id="">{{ $project->{'description:'.$language->code} }} </textarea>
                                    </div>
                                    @error("$language->code.description")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>

                                <div class="col">
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            Seo Section
                                        </span>
                                    </h3>
                                    <hr>
                                    <div class="row col-md-12">
                                        <div class="form-group col-md-6">
                                            <label>
                                                Meta Title
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][meta_title]" required value="{{ @$project->seo->{'meta_title:'.$language->code} }}" class="form-control @error('meta_title') is-invalid @enderror" placeholder="Enter Meta Title Value" />
                                            </div>
                                            @error("seo.$language->code.meta_title")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>
                                              Slug
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][slug]" required value="{{ @$project->seo->{'slug:'.$language->code} }}" class="form-control @error('slug') is-invalid @enderror" placeholder="Enter slug Value" />
                                            </div>
                                            @error("seo.$language->code.slug")
                                            <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>
                                               Meta Keywords
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][meta_keywords]" required value="{{ @$project->seo->{'meta_keywords:'.$language->code} }}" class="form-control @error('meta_keywords') is-invalid @enderror" placeholder="Enter Meta Keywords Value" />
                                            </div>
                                            @error("seo.$language->code.meta_keywords")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>
                                                Meta Description
                                            </label>
                                            <div class="input-group">
                                                <textarea class="form-control @error('meta_description') is-invalid @enderror" required name="seo[{{$language->code}}][meta_description]" id=""> {{ @$project->seo->{'meta_description:'.$language->code} }}</textarea>
                                            </div>
                                            @error("seo.$language->code.meta_description")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach


                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="fas fa-hashtag text-primary"></i>
                                </span>
                                <h3 class="card-label">
                                    <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                     Tags
                                    </span>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="checkbox-list">
                                    @foreach ($tags as $tag )
                                        <label class="checkbox checkbox-success">
                                        <input type="checkbox" name="tags[]" {{ $project->tags->contains($tag->id) ? 'checked' : '' }} value="{{$tag->id}}"/>
                                        <span></span>{{$tag->title}}</label>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="far fa-images text-primary"></i>
                                </span>
                                <h3 class="card-label">
                                    <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                        Attactments
                                    </span>
                                </h3>
                            </div>
                        </div>
                        <div class="row card-body">
                            <div id="kt_repeater_1">
                                <div class="form-group row">
                                    <div data-repeater-list="attachments" class="col-md-12">
                                        <div data-repeater-item="attachments" class="form-group row align-items-center">
                                            <div class="col-md-6">
                                                <label >
                                                    Attachment Iamge
                                                </label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <input type="hidden" name="id" value="0"/>
                                                    <div class="image-input image-input-outline image-input-circle" id="kt_image_66" >
                                                        <div class="image-input-wrapper" style="background-image:url({{asset('uploads/attachments/') }})"></div>
                                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                                            <input type="file" name="file" required accept=".png, .jpg, .jpeg" />
                                                            <input type="hidden" name="profile_avatar_remove" />
                                                        </label>
                                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row col-md-12">
                                                @foreach ($languages as $language)
                                                    <div class="form-group col-md-6">
                                                        <label for="{{$language->code}}[alts]">
                                                        Alt {{ $language->language }}
                                                        </label>
                                                        <div class="input-group">
                                                            <input type="text" name="[{{$language->code}}[alts]]"  class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                                        </div>
                                                        @error("$language->code.alts")
                                                            <small class="form-text text-danger">{{$message}}</small>
                                                        @enderror
                                                    </div>
                                                    @endforeach
                                            </div>
                                            <div class="col-md-4">
                                                <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger first-delete-btn">
                                                <i class="la la-trash-o"></i>Delete</a>
                                            </div>
                                        </div>


                                        @foreach ($project->attachments as $key=> $attachment)
                                        <div data-repeater-item="attachments" class="form-group row align-items-center">
                                            <input type="hidden" name="id" value="{{$attachment->id}}"/>
                                                <div class="col-md-6">
                                                    <label >
                                                        Attachment Iamge
                                                    </label>
                                                    <div class="col-lg-9 col-xl-6">
                                                        <div class="image-input image-input-outline image-input-circle" id="kt_image_{{70+$key}}" >
                                                            <div class="image-input-wrapper" style="background-image:url({{asset($attachment->url) }})"></div>
                                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                                <input type="file" name="file"  accept=".png, .jpg, .jpeg" />
                                                                <input type="hidden" name="profile_avatar_remove" />
                                                            </label>
                                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                            </span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row col-md-12">
                                                    @foreach ($languages as $language)
                                                        <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                                            <label for="{{$language->code}}[alts]">
                                                            Alt {{ $language->language }}
                                                            </label>
                                                            <div class="input-group">
                                                                <input type="text" name="[{{$language->code}}[alts]]" value="{{ $attachment->{'alts:'.$language->code} }}" class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                                            </div>
                                                            @error("$language->code.alts")
                                                                <small class="form-text text-danger">{{$message}}</small>
                                                            @enderror
                                                        </div>
                                                        @endforeach
                                                </div>
                                                <div class="col-md-4">
                                                    <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                                    <i class="la la-trash-o"></i>Delete</a>
                                                </div>
                                            </div>
                                            @endforeach

                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary" onclick="changeKeyImageId()">
                                    <i class="la la-plus"></i>Add</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5"></div>
                <div class="col-md-7">
                    <button type="submit" class="btn btn-primary mr-2">
                        Update
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/form-repeater.js')}}"></script>

<script>
    var counter = 1;
    var avatar6 = new KTImageInput('kt_image_6');
    function changeKeyImageId(){
        var num = counter + 66 ;
        setTimeout(() => {
            $('#kt_image_66').attr('id','kt_image_'+num);
            new KTImageInput('kt_image_'+num);
            new KTImageInput('kt_image_66');
            counter =counter + 1;
        },1000);
     }

     setTimeout(() => {
         $('.first-delete-btn').click();

     }, 500);

     var attach_count = "{{ $project->attachments->count() }}";
     for (let index = 0; index < attach_count ; index++) {
        var new_count = 70 + index;
        console.log(new_count);
        new KTImageInput('kt_image_' + new_count);
     }
</script>
@endsection
