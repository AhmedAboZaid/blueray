@extends('layouts.admin_main_page')

@section('page_title','Create Package')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <form class="form" method="POST" action="{{route('packages.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-body">

                            <div class="form-group">
                                <label >
                                    Main Iamge
                                </label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="image-input image-input-outline image-input-circle" id="kt_image_3" >
                                        <div class="image-input-wrapper" style="background-image: url({{asset('uploads/projects/') }})"></div>
                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input type="file" name="image[file]" required accept=".png, .jpg, .jpeg" />
                                            <input type="hidden" value="1" name="image[is_main]"  />
                                        </label>
                                        @error("image.file")
                                        <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                    </div>
                                </div>

                                <br>
                                <div class="row col-md-12 pt-3">
                                    @foreach ($languages as $language)
                                        <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                            <label for="{{$language->code}}[alts]">
                                            Alt {{ $language->language }}
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="image[{{$language->code}}][alts]" value='{{ old("image.$language->code.alts") }}' class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                            </div>
                                            @error("image.$language->code.alts")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        @endforeach
                                </div>
                            </div>

                            <div class="row">

                                <div class="form-group col-md-6">
                                    <label >Country</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <select class="form-control input-lg select2 " id="country_id" name="country_id" required >
                                            <option value="" >Choose Country</option>
                                            @foreach ($countries as $country)
                                            <option value="{{ $country->id }}">{{ $country->title }}</option>
                                            @endforeach
                                        </select>
                                        @error('country_id')
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label >City</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <select class="form-control input-lg select2" id="city_id" name="city_id" required >
                                            <option value="" >Choose City</option>
                                        </select>
                                        @error('city_id')
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Nights</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <input type="number" min="0" class="form-control input-lg" value="{{ old('nights') }}" name="nights" placeholder="Enter Nights" required />
                                        @error('nights')
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label >Tourism Type</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <select class="form-control input-lg select2" id="kt_select2_1" name="tourism_type_id" required >
                                            <option disabled value="" >Choose Tourism Type</option>
                                            @foreach ($tourism_types as $tourism_type)
                                            <option value="{{ $tourism_type->id }}" {{ $tourism_type->id == old('tourism_type_id') ? 'selected' :'' }} >{{ $tourism_type->title }}</option>
                                            @endforeach
                                        </select>
                                        @error('tourism_type_id')
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>
                                </div>

                                <div class="form-group col-md-6">
                                    <label >Languages</label>
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <select class="form-control input-lg " multiple id="languages" name="languages[]" >
                                            <option value="" >Choose Package Languages</option>
                                            @foreach ($languages as $key => $language )
                                            <option value="{{ $language->id }}" {{ in_array($language->id , old('languages')??[]) }}>{{ $language->language }}</option>
                                            @endforeach
                                        </select>
                                        @error('languages')
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>
                                </div>


                            </div>

                        </div>
                    </div>
                </div>



                @foreach ($languages as $key => $language )
                    <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="fas fa-car fa-fw text-primary"></i>
                                    </span>
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            Package {{ $language->language }}
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>
                                      Title
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[title]" required value='{{ old("$language->code.title") }}' class="form-control @error('title') is-invalid @enderror" placeholder="Enter Title Value" />
                                    </div>
                                    @error("$language->code.title")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label >
                                        Description
                                    </label>
                                    <div class="input-group">
                                        <textarea class="form-control @error('description') is-invalid @enderror editor" required name="{{$language->code}}[description]" id="{{$language->code}}_description">{{ old("$language->code.description") }}</textarea>
                                    </div>
                                    @error("$language->code.description")
                                    <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>

                                <div class="col">
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            Seo Section
                                        </span>
                                    </h3>
                                    <hr>
                                    <div class="row col-md-12">
                                        <div class="form-group col-md-6">
                                            <label>
                                                Meta Title
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][meta_title]"  value='{{ old("seo.$language->code.meta_title") }}' class="form-control @error('meta_title') is-invalid @enderror" placeholder="Enter Meta Title Value" />
                                            </div>
                                            @error("seo.$language->code.meta_title")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>
                                              Slug
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][slug]"  value='{{ old("seo.$language->code.slug") }}' class="form-control @error('slug') is-invalid @enderror" placeholder="Enter slug Value" />
                                            </div>
                                            @error("seo.$language->code.slug")
                                            <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>
                                               Meta Keywords
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][meta_keywords]"  value='{{ old("seo.$language->code.meta_keywords") }}' class="form-control @error('meta_keywords') is-invalid @enderror" placeholder="Enter Meta Keywords Value" />
                                            </div>
                                            @error("seo.$language->code.meta_keywords")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>
                                                Meta Description
                                            </label>
                                            <div class="input-group">
                                                <textarea class="form-control @error('meta_description') is-invalid @enderror" name="seo[{{$language->code}}][meta_description]" id="">{{ old("seo.$language->code.meta_description") }}</textarea>
                                            </div>
                                            @error("seo.$language->code.meta_description")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                                <div class="row col-md-12">
                                    <label >
                                        Travel Plan Description
                                    </label>
                                    <div class="input-group">
                                        <textarea class="form-control @error('description') is-invalid @enderror editor" required name="{{$language->code}}[travel_plan_description]" id="{{$language->code}}_travel_plan_description">{{ old("$language->code.travel_plan_description") }}</textarea>
                                    </div>
                                    @error("$language->code.travel_plan_description")
                                    <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>



                            </div>
                        </div>
                    </div>

                @endforeach

                {{--           Travel Plan Details       --}}
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="far fa-images text-primary"></i>
                                </span>
                                <h3 class="card-label">
                                    <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                        Travel Plan Details
                                    </span>
                                </h3>
                            </div>
                        </div>
                        <div class="row card-body">
                            <div id="travelPlans" class="w-100">
                                <div class="form-group">
                                    <div data-repeater-list="travelPlans" class="">
                                        <div data-repeater-item="travelPlan" class="form-group row col-md-12 align-items-center">


                                            <div class=" row col-md-11 col-12">
                                                <div class="col-md-4 col-12">
                                                    <div class="form-group">
                                                        <label>Number</label>
                                                        <div class="input-group">
                                                            <input type="number" min="1" name="number" required value='{{ @old("travelPlan")['number'] }}' class="form-control @error('number') is-invalid @enderror" placeholder="Enter number Value" />
                                                        </div>
                                                        @error("travelPlan.number")
                                                            <small class="form-text text-danger">{{$message}}</small>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="row col-md-12">
                                                    @foreach ($languages as $language)
                                                        <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">

                                                            <div class="row">

                                                                <div class="form-group col-md-12">
                                                                    <label>Title ({{ $language->code }})</label>
                                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                                        <input type="text" name="{{ $language->code }}_title" class="form-control " required placeholder="Enter Title" />
                                                                        @error("travelPlan.$language->code.title")
                                                                            <small class="form-text text-danger">{{$message}}</small>
                                                                        @enderror
                                                                    </div>
                                                                </div>

                                                                <div class="form-group">
                                                                    <label> Description ({{ $language->code }})</label>
                                                                    <div class="input-group">
                                                                        <textarea class="form-control @error('description') is-invalid @enderror editor" required name="{{$language->code}}_description" id="travelPlan_{{$language->code}}_description">{{ @old("travelPlan")[$language->code]['description'] }}</textarea>
                                                                    </div>
                                                                    @error("travelPlan.$language->code.description")
                                                                    <small class="form-text text-danger">{{$message}}</small>
                                                                    @enderror
                                                                </div>


                                                            </div>

                                                        </div>
                                                        @endforeach
                                                </div>
                                            </div>

                                            <div class="col-md-1 col-4">
                                                <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                                <i class="la la-trash-o"></i>Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary" onclick="changeKeyImageId()">
                                    <i class="la la-plus"></i>Add</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--           End Travel Plan Details       --}}

                {{--           tags       --}}
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="fas fa-hashtag text-primary"></i>
                                </span>
                                <h3 class="card-label">
                                    <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                     Tags
                                    </span>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="checkbox-list">
                                    @foreach ($tags as $tag)
                                    <label class="checkbox checkbox-success">
                                    @php
                                        $old_tags = old("tags") ? old("tags") : [];

                                    @endphp
                                    <input type="checkbox" name="tags[]" value="{{$tag->id}}" {{ in_array($tag->id , $old_tags ) ? 'checked' : '' }} />
                                    <span></span>{{$tag->title}}</label>
                                @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--         End tags       --}}

                {{--           Facilities       --}}
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="fas fa-hashtag text-primary"></i>
                                </span>
                                <h3 class="card-label">
                                    <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                        Facilities
                                    </span>
                                </h3>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="form-group">

                                <div class="row my-2 border-bottom">
                                    <div class="col-4">
                                        <h3>Facility</h3>
                                    </div>
                                    <div class="col-2">
                                        <h3>{{ __('Enclude') }}</h3>
                                    </div>

                                    <div class="col-2">
                                       <h3>{{ __('Exclude') }}</h3>
                                    </div>
                                </div>

                                <div class="checkbox-list">
                                    @foreach ($facilities as $facility)
                                        @php
                                            $facilities_enclude = old("facilities_enclude") ? old("facilities_enclude") : [];
                                            $facilities_exclude = old("facilities_exclude") ? old("facilities_exclude") : [];
                                        @endphp

                                        <div class="row my-2">
                                            <div class="col-4">
                                                {{ $facility->name }}
                                            </div>
                                            <div class="col-2 ">
                                                <label class="checkbox checkbox-success">
                                                    <input type="checkbox" name="facilities_enclude[]" value="{{$facility->id}}" {{ in_array($facility->id , $facilities_enclude ) ? 'checked' : '' }} />
                                                    <span></span>
                                                </label>
                                            </div>

                                            <div class="col-2 ">
                                                <label class="checkbox checkbox-success">
                                                    <input type="checkbox" name="facilities_exclude[]" value="{{$facility->id}}" {{ in_array($facility->id , $facilities_exclude ) ? 'checked' : '' }} />
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--         End Facilities       --}}


                {{--           Attachments       --}}
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="far fa-images text-primary"></i>
                                </span>
                                <h3 class="card-label">
                                    <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                        Attactments
                                    </span>
                                </h3>
                            </div>
                        </div>
                        <div class="row card-body">
                            <div id="kt_repeater_2">
                                <div class="form-group row">
                                    <div data-repeater-list="attachments" class="col-md-12">
                                        <div data-repeater-item="attachments" class="form-group row align-items-center">
                                            <div class="col-md-6">
                                                <label >
                                                    Attachment Iamge
                                                </label>
                                                <div class="col-lg-9 col-xl-6">
                                                    <div class="image-input image-input-outline image-input-circle" id="kt_image_6" >
                                                        <div class="image-input-wrapper" style="background-image: url()"></div>
                                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                                            <input type="file" name="file" required accept=".png, .jpg, .jpeg" />
                                                        </label>
                                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                        </span>
                                                    </div>

                                                    @error("file")
                                                        <small class="form-text text-danger">{{$message}}</small>
                                                    @enderror
                                                </div>
                                            </div>
                                            <div class="row col-md-12">
                                                @foreach ($languages as $language)
                                                    <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                                        <label for="{{$language->code}}[alts]">
                                                        Alt {{ $language->language }}
                                                        </label>
                                                        <div class="input-group">
                                                            <input type="text" name="[{{$language->code}}[alts]]" value='{{ old("attachments.$language->code.alts") }}' class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                                        </div>
                                                        @error("$language->code.alts")
                                                            <small class="form-text text-danger">{{$message}}</small>
                                                        @enderror
                                                    </div>
                                                    @endforeach
                                            </div>
                                            <div class="col-md-4">
                                                <a href="javascript:;" data-repeater-delete="" class="btn btn-sm font-weight-bolder btn-light-danger">
                                                <i class="la la-trash-o"></i>Delete</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <a href="javascript:;" data-repeater-create="" class="btn btn-sm font-weight-bolder btn-light-primary" onclick="changeKeyImageId()">
                                    <i class="la la-plus"></i>Add</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--           End Attachments       --}}


                <div class="col-md-5"></div>
                <div class="col-md-7">
                    <button type="submit" class="btn btn-primary mr-2">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/form-repeater.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>
<script>
    let counter = 0;
    // $('#durationList').repeater({
    //     initEmpty: true,

    //     defaultValues: {
    //         'text-input': 'foo'
    //     },
    //     show: function () {
    //         $(this).slideDown();
    //     },
    //     hide: function (deleteElement) {
    //         $(this).slideUp(deleteElement);
    //     }
    // });

    $('#travelPlans').repeater({
        initEmpty: true,

        defaultValues: {
            'text-input': 'foo'
        },
        show: function () {
            @foreach ($languages as $language)
                lang_code = "{{ $language->code }}";
                counter ++;
                $('#travelPlan_'+lang_code+'_description').attr('id' , 'travelPlan_'+lang_code+'_description_'+counter);
                CKEDITOR.replace('travelPlan_'+lang_code+'_description_'+counter ,  {
                    height: 180,
                    baseFloatZIndex: 10005,
                    removeButtons: 'PasteFromWord'
                });
            @endforeach

            $(this).slideDown();
        },

        hide: function (deleteElement) {
            $(this).slideUp(deleteElement);
        }
    });

    counter = 1;
    var avatar6 = new KTImageInput('kt_image_6');
    function changeKeyImageId(){
        var num = counter + 6 ;
        setTimeout(() => {
            $('#kt_image_6').attr('id','kt_image_'+num);
            new KTImageInput('kt_image_'+num);
            new KTImageInput('kt_image_6');
            counter =counter + 1;
        },1000);
    }

    $('.select2').select2();
    $("#languages").select2({
        placeholder: "Select Languages",
        allowClear: true
    });
    $('#country_id').change(function(){
        let parent_id = $(this).val();
        let city = null;
        let response = "";
        $.ajax({
            type: "GET",
            url: "{{ route('packages.cities')}}/" + parent_id,
            success: function (data) {
                for(let i=0; i < data.length; i++){
                    city = data[i];
                    response += '<option value="'+ city.id +'" >'+ city.title +'</option>'
                }
                $('#city_id').html(response);
                $('#city_id').select2();
            }
        });
    });

    $('.editor').editor();

</script>
@endsection
