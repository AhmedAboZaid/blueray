@extends('layouts.admin_main_page')

@section('page_title','Languages')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
		<!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="icon-xl la la-language text-primary"></i>
                    </span>                         
                    <h3 class="card-label">
                        {{__('languages.languages')}}
                    </h3>
                </div>
                <div id="successMessage" style="text-align:center; width:40%" class="mt-5">
                    @if(Session::has('success'))
                        <p class="alert alert-success">{{ Session::get('success') }}</p>
                    @endif
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{route('languages.create')}}" class="btn btn-primary font-weight-bolder">
                        <i class="la la-plus"></i>
                        {{__('languages.new_language')}}
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table table-separate table-head-custom collapsed" id="Lang_table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{__('languages.language')}}</th>
                            <th>{{__('languages.code')}}</th>
                            <th>{{__('languages.flag')}}</th>
                            <th>{{__('languages.status')}}</th>
                            <th>{{__('languages.actions')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($languages as $language )
                        <tr>
                            <td>{{$language->id}}</td>
                            <td>{{$language->language}}</td>
                            <td>{{$language->code}}</td>
                            <td> 
                                @if ($language->flag)
                                <img  style="width: 40px; height: 40px;" src="{{asset('uploads/languages/'.$language->flag)}}">
                                @else
                                <img  style="width: 40px; height: 40px;" src="{{asset('uploads/languages/none.png')}}">
                                    
                                @endif
                            </td>
                            <td>
                                @if($language->isActive === 1)
                                <span class="badge badge-success  cursor-pointer" onclick="changeStatus('{{ route('languages.status')}}' ,{{ $language->id }} )">Active</span>
                                @else
                                <span class="badge badge-danger  cursor-pointer" onclick="changeStatus('{{ route('languages.status')}}' ,{{ $language->id }} )">Not-Active</span>
                               @endif                          
                            </td>
                            <td>
                                <a href="{{route('languages.edit',$language->id)}}" class="btn btn-light btn-circle btn-sm btn-shadow mr-2">
                                    <i class="flaticon2-gear text-warning"></i>
                                    {{__('languages.edit')}} 
                                </a>

                                <a href="#" class="btn btn-danger btn-circle btn-sm btn-shadow mr-2" onclick="deleteData('{{ route('languages.delete')}}' ,{{$language->id}})" >
                                    <i class="flaticon2-trash"></i> 
                                    {{__('languages.delete')}}  
                                </a>
                               
                            </td>
                        </tr>
                        @endforeach
                           
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

{{-- <script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script> --}}

    <script>
        var Lang_table_responsive = function() {

        var Lang_table = function() {
            var table = $('#Lang_table');

            // begin first table
            table.DataTable({
                responsive: true,
                columnDefs: [
                    {
                        targets: 3,
                        orderable: false,
                    
                    },
                    {
                        targets: 5,
                        orderable: false,
                    }
			    ]
                
            });
        };

        return {
            //main function to initiate the module
            init: function() {
                Lang_table();
            }
        };
        }();

        jQuery(document).ready(function() {
            Lang_table_responsive.init();
        });

    </script>
@endsection