@extends('layouts.admin_main_page')

@section('page_title','Languages')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
		<!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="icon-xl la la-language text-primary"></i>
                    </span>
                    <h3 class="card-label">
                        {{__('languages.new_language')}}
                    </h3>
                </div>
                <div class="card-toolbar">
                </div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger col-md-4">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <!--begin::Form-->
                <form class="form" method="POST" action="{{route('languages.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="card-body">
                        <div class="form-group col-md-6">
                            <label>
                                {{__('languages.language')}}
                            </label>
                            <div class="input-group">
                                <input type="text" name="language" class="form-control @error('language') is-invalid @enderror" required placeholder="Enter Laguage Name" />
                            </div>
                            @error('language')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="form-group col-md-6">
                            <label>
                                {{__('languages.code')}}
                            </label>
                            <div class="input-group">
                                <input type="text" name="code" class="form-control @error('code') is-invalid @enderror" required placeholder="Enter Laguage Code" />
                            </div>
                            @error('code')
                            <small class="form-text text-danger">{{$message}}</small>
                            @enderror
                        </div>
                        <div class="form-group ">
                            <label >
                                {{__('languages.flag')}}
                            </label>
                            <br>
                            <div class="image-input image-input-outline image-input-circle" id="kt_image_3">
                                <div class="image-input-wrapper" style="background-image: url('{{ asset('uploads/languages/') }}')"></div>
                                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                    <input type="file" name="flag" required/>
                                    @error('flag')
                                         <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    <input type="hidden" name="profile_avatar_remove" />
                                </label>
                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                </span>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-form-label">
                                {{__('languages.status')}}
                            </label>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <input data-switch="true" type="checkbox" checked="checked" name="isActive" data-on-text="Active" data-handle-width="50" data-off-text="Not Active" data-on-color="success" />
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">
                            {{__('languages.submit')}}
                        </button>
                    </div>
                </form>
                <!--end::Form-->

            </div>
        </div>
        <!--end::Card-->
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

@endsection