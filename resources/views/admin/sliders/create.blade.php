@extends('layouts.admin_main_page')

@section('page_title','Sliders')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
        <form class="form" method="POST" action="{{route('sliders.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="container-fluid">
                <div class="row">
                    @foreach ($languages as $key => $language )
                    <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="icon-xl la la-sliders-h text-primary"></i>
                                    </span>
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            {{__('sliders.sliders')}} {{ $language->language }}
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">

                                <div class="form-group">
                                    <label>
                                        {{__('sliders.title')}}
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[title]"  required value="{{ @old($language->code)['title'] }}"  class="form-control @error('title') is-invalid @enderror" placeholder="Enter Title Value" />
                                    </div>
                                    @error("$language->code.title")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>

                                <div class="form-group ">
                                    <label >
                                        {{__('sliders.short_description')}}
                                    </label>
                                    <div class="input-group">
                                        <textarea rows="5"  class="form-control @error('short_description') is-invalid @enderror" required   name="{{$language->code}}[short_description]" >{{ @old($language->code)['short_description'] }}</textarea>
                                    </div>
                                    @error("$language->code.short_description")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>


                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-md-12">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-body">
                                <div class="form-group col-md-5">
                                    <label>
                                        {{__('sliders.btn')}}
                                    </label>
                                    <div class="input-group">
                                        <input type="text"  value="{{ @old('btn') }}" name="btn" class="form-control @error('btn') is-invalid @enderror" placeholder="Enter BTN Value" />
                                    </div>
                                    @error('btn')
                                    <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label >
                                        {{__('sliders.image')}}
                                    </label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="image-input image-input-outline image-input-circle" id="kt_image_3" >
                                            <div class="image-input-wrapper" style="background-image: url()"></div>
                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                <input type="file" name="image[file]" required accept=".png, .jpg, .jpeg" />
                                                <input type="hidden" name="profile_avatar_remove" />
                                            </label>
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                        </div>
                                        @error("image.$language->code.alts")
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>

                                    <br>

                                    <div class="row col-md-12">
                                        @foreach ($languages as $language)
                                            <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                                <label for="{{$language->code}}[alts]">
                                                Alt {{ $language->language }}
                                                </label>
                                                <div class="input-group">
                                                    <input type="text" name="image[{{$language->code}}][alts]" value="{{ @old('image')[$language->code]['alts'] }}" class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                                </div>
                                                @error("$language->code.alts")
                                                    <small class="form-text text-danger">{{$message}}</small>
                                                @enderror
                                            </div>
                                            @endforeach
                                    </div>


                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
                    <div class="col-md-7">
                        <button type="submit" class="btn btn-primary mr-2">
                            {{__('sliders.submit')}}
                        </button>
                    </div>
                </div>
            </div>
        </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

<script>
    @foreach ($languages as $key =>$lang )
    $('.editor'.{{30+$key}}).each(function(e){
                CKEDITOR.replace( this.id,  {
                    height: 150,
                    baseFloatZIndex: 10005,
                    removeButtons: 'PasteFromWord'
                });
            });
    @endforeach
</script>
@endsection
