@extends('layouts.admin_main_page')

@section('page_title','Clients')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
        <form class="form" method="POST" action="{{route('clients.store')}}" enctype="multipart/form-data">
            @csrf
            <div class="container-fluid">
                <div class="row">
                    @foreach ($languages as $key => $language )
                    <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="fas fa-person-booth text-primary"></i>
                                    </span>
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                          Client {{ $language->language }}
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>
                                       Title
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[title]"  required  class="form-control @error('title') is-invalid @enderror" placeholder="Enter Title Value" />
                                    </div>
                                    @error("$language->code.title")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-md-12">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-body">
                                <div class="form-group">
                                    <label >
                                        Image
                                    </label>
                                    <div class="col-lg-9 col-xl-6">
                                        <div class="image-input image-input-outline image-input-circle" id="kt_image_3" >
                                            <div class="image-input-wrapper" style="background-image: url({{asset('uploads/clients/') }})"></div>
                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                <input type="file" name="image[file]" required accept=".png, .jpg, .jpeg" />
                                            </label>
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                        </div>

                                        @error("image.file")
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror

                                    </div>

                                    <br>

                                    <div class="row col-md-12">
                                        @foreach ($languages as $language)
                                            <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                                <label for="{{$language->code}}[alts]">
                                                Alt {{ $language->language }}
                                                </label>
                                                <div class="input-group">
                                                    <input type="text" name="image[{{$language->code}}][alts]" class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                                </div>
                                                @error("image.$language->code.alts")
                                                    <small class="form-text text-danger">{{$message}}</small>
                                                @enderror
                                            </div>
                                            @endforeach
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
                    <div class="col-md-7">
                        <button type="submit" class="btn btn-primary mr-2">
                          Save
                        </button>
                    </div>
                </div>
            </div>
        </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

<script>
    @foreach ($languages as $key =>$lang )
    $('.editor'.{{30+$key}}).each(function(e){
                CKEDITOR.replace( this.id,  {
                    height: 150,
                    baseFloatZIndex: 10005,
                    removeButtons: 'PasteFromWord'
                });
            });
    @endforeach
</script>
@endsection
