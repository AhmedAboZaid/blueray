@extends('layouts.admin_main_page')

@section('page_title','Social Media')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
		<!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="icon-xl la la-language text-primary"></i>
                    </span>
                    <h3 class="card-label">
                        Social Media
                    </h3>
                </div>
                <div class="card-toolbar">
                </div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger col-md-4">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <!--begin::Form-->
                <form class="form" method="POST" action="{{route('social.media.store')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-custom gutter-b example example-compact">
                                <div class="card-body row">
                                    <div class="form-group col-md-6">
                                        <label>Social Media</label>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <select class="form-control input-lg" id="kt_select2_1" name="name" required>
                                                <option value="">Choose Social Media</option>
                                                <option value="facebook">Facebook</option>
                                                <option value="instagram">Instagram</option>
                                                <option value="youtube">YouTube</option>
                                                <option value="linkedin">Linkedin</option>
                                                <option value="twitter">Twitter</option>
                                                <option value="whatsapp">Whatsapp</option>
                                                <option value="blogger">Blogger</option>
                                                <option value="tiktok">Tiktok</option>
                                                <option value="snapchat">Snapchat</option>
                                                <option value="pinterest">Pinterest</option>
                                                <option value="reddit">Reddit</option>
                                            </select>
                                            @error('name')
                                                <small class="form-text text-danger">{{ $message }}</small>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group form-group col-md-6">
                                        <label>
                                            URL
                                        </label>
                                        <div class="input-group">
                                            <input type="text" value="{{old('url')}}" name="url" class="form-control @error('url') is-invalid @enderror"
                                                required placeholder="Enter Social Media Url" />
                                        </div>
                                        @error('url')
                                            <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">
                            {{__('languages.submit')}}
                        </button>
                    </div>
                </form>
                <!--end::Form-->

            </div>
        </div>
        <!--end::Card-->
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

@endsection
