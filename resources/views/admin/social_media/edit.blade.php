@extends('layouts.admin_main_page')

@section('page_title','Social Media')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
		<!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fas fa-icons text-primary"></i>
                    </span>
                    <h3 class="card-label">
                        Social Media
                    </h3>
                </div>
                <div class="card-toolbar">
                </div>
            </div>
            <div class="card-body">
                @if ($errors->any())
                <div class="alert alert-danger col-md-4">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <!--begin::Form-->
                <form class="form" method="POST" action="{{route('social.media.update')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="card card-custom gutter-b example example-compact">
                                <input type="hidden" name="id" value="{{ $socialMedia->id }}">
                                <div class="card-body row">
                                    <div class="form-group col-md-6">
                                        <label>Social Media</label>
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <select class="form-control input-lg" id="kt_select2_1" name="name">
                                                <option {{ $socialMedia->name == 'facebook' ? 'selected' : '' }} value="facebook">
                                                    Facebook
                                                </option>
                                                <option {{ $socialMedia->name == 'instagram' ? 'selected' : '' }} value="instagram">
                                                    Instagram
                                                </option>
                                                <option {{ $socialMedia->name == 'youtube' ? 'selected' : '' }} value="youtube">
                                                    YouTube
                                                </option>
                                                <option {{ $socialMedia->name == 'linkedin' ? 'selected' : '' }} value="linkedin">
                                                    Linkedin
                                                </option>
                                                <option {{ $socialMedia->name == 'twitter' ? 'selected' : '' }} value="twitter">
                                                    Twitter
                                                </option>
                                                <option {{ $socialMedia->name == 'whatsapp' ? 'selected' : '' }} value="whatsapp">
                                                    Whatsapp
                                                </option>
                                                <option {{ $socialMedia->name == 'blogger' ? 'selected' : '' }} value="blogger">
                                                    Blogger
                                                </option>
                                                <option {{ $socialMedia->name == 'tiktok' ? 'selected' : '' }} value="tiktok">
                                                    Tiktok
                                                </option>
                                                <option {{ $socialMedia->name == 'snapchat' ? 'selected' : '' }} value="snapchat">
                                                    Snapchat
                                                </option>
                                                <option {{ $socialMedia->name == 'pinterest' ? 'selected' : '' }} value="pinterest">
                                                    Pinterest
                                                </option>
                                                <option {{ $socialMedia->name == 'reddit' ? 'selected' : '' }} value="reddit">
                                                    Reddit
                                                </option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group col-md-6">
                                        <label>
                                            URL
                                        </label>
                                        <div class="input-group">
                                            <input type="text" name="url" class="form-control @error('url') is-invalid @enderror"
                                                value="{{ $socialMedia->url }}" placeholder="Enter Social Media Url" />
                                        </div>
                                        @error('url')
                                            <small class="form-text text-danger">{{ $message }}</small>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">
                           Update
                        </button>
                    </div>
                </form>
                <!--end::Form-->

            </div>
        </div>
        <!--end::Card-->
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

@endsection
