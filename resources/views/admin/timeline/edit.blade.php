@extends('layouts.admin_main_page')

@section('page_title','Timeline')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <form class="form" method="POST" action="{{route('timeline.update')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$timeline->id}}">
        <div class="container-fluid">
            <div class="row">		    
                @foreach ($languages as $key => $language )
                <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="far fa-calendar-times text-primary"></i>
                                </span>
                                <h3 class="card-label">
                                    <span class="font-weight-bolder text-info mb-4 text-hover-state-dark"> 
                                        Timeline {{ $language->language }}
                                    </span>
                                </h3>
                            </div>                    
                        </div>                        
                        <div class="card-body"> 
                            <div class="form-group">
                                <label>
                                    Title                                         
                                </label>
                                <div class="input-group">
                                    <input type="text" name="{{$language->code}}[title]"   value="{{ $timeline->{'title:'.$language->code} }}"  required class="form-control @error('title') is-invalid @enderror" placeholder="Enter title Value" />
                                </div>
                                @error("$language->code.title")
                                    <small class="form-text text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label >
                                    Description
                                </label>
                                <div class="card-body">
                                    <textarea class="form-control @error('description') is-invalid @enderror"  required id="" name="{{$language->code}}[description]" >{{ $timeline->{'description:'.$language->code} }}</textarea>
                                </div>
                                @error("$language->code.description")
                                    <small class="form-text text-danger">{{$message}}</small>
                                @enderror
                            </div>

                        
                        </div>                        
                    </div>                        
                </div>                
                @endforeach 
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">                     
                        <div class="card-body">   
                            <div class="form-group col-md-5">
                                <label>Date</label>
                                <div class="col-md-12">
                                    <div class="input-group input-group-solid date" id="kt_datetimepicker_3" data-target-input="nearest">
                                        <input type="text" name="date" value="{{$timeline->date}}" class="form-control form-control-solid datetimepicker-input" placeholder="Select date &amp; time" data-target="#kt_datetimepicker_3" />
                                        <div class="input-group-append" data-target="#kt_datetimepicker_3" data-toggle="datetimepicker">
                                            <span class="input-group-text">
                                                <i class="ki ki-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>                        
                    </div>                        
                </div>     
                <div class="col-md-5"></div>
                <div class="col-md-7">                    
                    <button type="submit" class="btn btn-primary mr-2">
                        Submit
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

@endsection