@extends('layouts.admin_main_page')

@section('page_title','Tags')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
        <form class="form" method="POST" action="{{route('tags.update')}}" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{$tag->id}}">
            <div class="container-fluid">
                <div class="row">		    
                    @foreach ($languages as $language )
                    <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="fas fa-hashtag text-primary"></i>
                                    </span>
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark"> 
                                           Tag {{ $language->language }}
                                        </span>
                                    </h3>
                                </div>                    
                            </div>                        
                            <div class="card-body">                                                                
                                <div class="form-group">
                                    <label>
                                        Title                                        
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[title]" value="{{ $tag->{'title:'.$language->code} }}" class="form-control @error('title') is-invalid @enderror" placeholder="Enter Title Value" />
                                    </div>
                                    @error("$language->code.title")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>                        
                        </div>                        
                    </div>                
                    @endforeach    
                    <div class="col-md-5"></div>
                    <div class="col-md-7">                    
                        <button type="submit" class="btn btn-primary mr-2" id="btnUpdate">
                           Update
                        </button>
                    </div>
                </div>
            </div>
        </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

@endsection