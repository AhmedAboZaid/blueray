@extends('layouts.admin_main_page')

@section('page_title','Settings')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
		<!--begin::Card-->
        <div class="card card-custom">
            <div class="card-header">
                <div class="card-title">
                    <span class="card-icon">
                        <i class="fas fa-cog text-primary"></i>
                    </span>                         
                    <h3 class="card-label">
                         Settings
                    </h3>
                </div>
                <div id="successMessage" style="text-align:center; width:40%" class="mt-5">
                    @if(Session::has('success'))
                        <p class="alert alert-success font-weight-bold">{{ Session::get('success') }}</p>
                    @endif
                </div>
                <div class="card-toolbar">
                    <!--begin::Button-->
                    <a href="{{route('settings.edit')}}" class="btn btn-light btn-circle btn-sm btn-shadow mr-2 font-weight-bolder">
                        <i class="flaticon2-gear text-warning"></i>
                        Edit
                    </a>
                    <!--end::Button-->
                </div>
            </div>
            <div class="card-body">
                <!--begin: Datatable-->
                <table class="table table-separate table-head-custom collapsed" id="Lang_table">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Setting Key</th>
                            <th>Setting Value</th>
                        </tr>
                    </thead>
                    <tbody>
                      @foreach ($settings as $setting ) 
                        <tr>
                            <td>{{$setting->id}}</td>
                            <td>{{$setting->key}} </td>
                            <td>
                                @if ($setting->flag == 1)
                                {{$setting->value}}       
                                @elseif ($setting->flag == 3 )
                                {{$setting->value}}
                                @else
                                <img  style="width: 60px; height: 60px;" src="{{asset('uploads/settings/'.$setting->value)}}">            
                                @endif
                            </td>
                        </tr>
                        @endforeach
                           
                    </tbody>
                </table>
                <!--end: Datatable-->
            </div>
        </div>
        <!--end::Card-->
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

{{-- <script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script> --}}

    <script>
        var Lang_table_responsive = function() {

        var Lang_table = function() {
            var table = $('#Lang_table');

            // begin first table
            table.DataTable({
                responsive: true,
                columnDefs: [
                    {
                        targets: 3,
                        orderable: false,                  
                    },
			    ]
                
            });
        };

        return {
            //main function to initiate the module
            init: function() {
                Lang_table();
            }
        };
        }();

        jQuery(document).ready(function() {
            Lang_table_responsive.init();
        });

    </script>

@endsection