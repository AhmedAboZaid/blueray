@extends('layouts.admin_main_page')

@section('page_title','Settings')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form class="form" method="POST" action="{{route('settings.update')}}" enctype="multipart/form-data">
        @csrf
        <div class="container-fluid">
            <div class="row">		    
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">   
                        <div class="card-body">  
                            <div class="row col-md-12">
                                @foreach ($languages as $key => $language )  
                                <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                    <h3 class="card-label">
                                            <i class="fas fa-cog text-primary"></i>
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark"> 
                                            Setting {{$language->language}}
                                        </span>
                                    </h3>
                                    @foreach ($settings  as $setting ) 
                                    @if ($setting->flag == 1)
                                        <label>
                                            {{$setting->key}}                                      
                                        </label>
                                        <div class="input-group">
                                            <input type="text" class="form-control" id="key" name="{{$setting->key}}[{{$language->code}}][value]" value="{{ $setting->{'value:'.$language->code} }}">
                                        </div>
                                        @error("$language->code.value")
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                        <br><br>
                                    @elseif ($setting->flag == 2)
                                        <label >{{$setting->key}}</label>
                                        <div class="input-group col-md-12">
                                            <div class="image-input image-input-outline image-input-circle" id="kt_image_{{30+$key}}" >
                                                <div class="image-input-wrapper" style="background-image: url({{asset('uploads/settings/'. $setting->{'value:'.$language->code}) }})"></div>
                                                <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                    <i class="fa fa-pen icon-sm text-muted" style="    margin-left: 30px;"></i>
                                                    <input type="file" name="{{$setting->key}}[{{$language->code}}][value]"  class="form-control @error('value') is-invalid @enderror" accept=".png, .jpg, .jpeg" />
                                                    @error('value') 
                                                    <small class="form-text text-danger">{{$message}}</small>
                                                    @enderror
                                                    <input type="hidden" name="profile_avatar_remove" />
                                                </label>
                                                <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                    <i class="ki ki-bold-close icon-xs text-muted"></i>
                                                </span>
                                            </div>
                                        </div>
                                    @endif                 
                                    @endforeach
                                </div>  
                                @endforeach  
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row col-md-12">
                                <div class="form-group col-md-12">
                                    @foreach ($settings as $setting)
                                        @if ($setting->flag == 3)
                                            <label >
                                                {{$setting->key}}
                                            </label>
                                            <div class="input-group">
                                                <textarea class="form-control @error('value') is-invalid @enderror" required name="{{$setting->key}}[value]" id="">{{ $setting->value }} </textarea>
                                            </div>
                                            @error("value")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                            <br><br>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>                        
                </div>                
                <div class="col-md-5"></div>
                <div class="col-md-7">                    
                    <button type="submit" class="btn btn-primary mr-2">
                       Update
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

<script>
        @foreach ($languages as $key =>$lang )
            new KTImageInput('kt_image_'+"{{30+$key}}");
        @endforeach
</script>

<script>
    @foreach ($languages as $key =>$lang )
    $('.editor'.{{30+$key}}).each(function(e){
                CKEDITOR.replace( this.id,  {
                    height: 150,
                    baseFloatZIndex: 10005,
                    removeButtons: 'PasteFromWord'
                });
            });
    @endforeach
</script>
@endsection
