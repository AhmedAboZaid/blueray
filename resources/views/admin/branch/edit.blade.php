@extends('layouts.admin_main_page')

@section('page_title','Branch')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
        <form class="form" method="POST" action="{{route('branch.update')}}" enctype="multipart/form-data">
            @csrf
         <input type="hidden" name="id" value="{{$branch->id}}">
            <div class="container-fluid">
                <div class="row">
                    @foreach ($languages as $key => $language )
                    <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="far fa-gem text-primary"></i>
                                    </span>
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            Branch {{ $language->language }}
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <div class="row card-body">
                                <div class="form-group col-6">
                                    <label>
                                       Name
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[name]"  value="{{ $branch->{'name:'.$language->code} }}"  class="form-control @error('name') is-invalid @enderror" placeholder="Enter Facility Name" />
                                    </div>
                                    @error("$language->code.name")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    <label>
                                       Address
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[address]"  value="{{ $branch->{'address:'.$language->code} }}"  class="form-control @error('address') is-invalid @enderror" placeholder="Enter Branch address" />
                                    </div>
                                    @error("$language->code.address")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    <div class="col-md-12">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="row card-body">
                                <div class="form-group row col-12">
                                    <label >
                                        Main Image
                                    </label>
                                    <div class="col-3">
                                        <div class="image-input image-input-outline image-input-circle" id="kt_image_3" >
                                            <div class="image-input-wrapper" style="background-image: url({{ @$branch->image->url}})"></div>
                                            <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                                <i class="fa fa-pen icon-sm text-muted"></i>
                                                <input type="file" name="mainImg[file]" accept=".png, .jpg, .jpeg" />
                                                <input type="hidden" value="1" name="mainImg[is_main]"  />
                                            </label>
                                            @error("mainImg.file")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                            <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                                <i class="ki ki-bold-close icon-xs text-muted"></i>
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row col-md-8">
                                        @foreach ($languages as $language)
                                            <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                                <label for="{{$language->code}}[alts]">
                                                Alt {{ $language->language }}
                                                </label>
                                                <div class="input-group">
                                                    <input type="text" name="mainImg[{{$language->code}}][alts]" value="{{ @$branch->image->{'alts:'.$language->code} }}" class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                                </div>
                                                @error("$language->code.alts")
                                                    <small class="form-text text-danger">{{$message}}</small>
                                                @enderror
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="form-group col-6">
                                    <label>
                                       Phone
                                    </label>
                                    <div class="input-group">
                                        <input type="number" name="phone"  value="{{ $branch->phone }}"  class="form-control @error('phone') is-invalid @enderror" placeholder="Enter Branch phone" />
                                    </div>
                                    @error("phone")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    <label>
                                       fax
                                    </label>
                                    <div class="input-group">
                                        <input type="number" name="fax" value="{{ $branch->fax }}"  class="form-control @error('fax') is-invalid @enderror" placeholder="Enter Branch fax" />
                                    </div>
                                    @error("fax")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    <label>
                                       email
                                    </label>
                                    <div class="input-group">
                                        <input type="email" name="email"  value="{{ $branch->email }}"  class="form-control @error('email') is-invalid @enderror" placeholder="Enter Branch email" />
                                    </div>
                                    @error("email")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    <label>
                                       facebook
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="facebook"  value="{{ $branch->facebook }}"  class="form-control @error('facebook') is-invalid @enderror" placeholder="Enter Branch facebook" />
                                    </div>
                                    @error("facebook")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    <label>
                                       instagram
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="instagram"  value="{{ $branch->instagram }}"  class="form-control @error('instagram') is-invalid @enderror" placeholder="Enter Branch instagram" />
                                    </div>
                                    @error("instagram")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    <label>
                                       twitter
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="twitter"  value="{{ $branch->twitter }}"  class="form-control @error('twitter') is-invalid @enderror" placeholder="Enter Branch twitter" />
                                    </div>
                                    @error("twitter")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group col-6">
                                    <label>
                                       whatsapp
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="whatsapp"  value="{{ $branch->whatsapp }}"  class="form-control @error('whatsapp') is-invalid @enderror" placeholder="Enter Branch whatsapp" />
                                    </div>
                                    @error("whatsapp")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>


                            </div>
                        </div>
                    </div>
                    <div class="col-md-5"></div>
                    <div class="col-md-7">
                        <button type="submit" class="btn btn-primary mr-2">
                          Save
                        </button>
                    </div>
                </div>
            </div>
        </form>
@endsection


@section('js')
<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

@endsection
