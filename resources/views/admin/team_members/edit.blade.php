@extends('layouts.admin_main_page')

@section('page_title','Team Members')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <form class="form" method="POST" action="{{route('team.members.update')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$teamMember->id}}">
        <div class="container-fluid">
            <div class="row">		    
                @foreach ($languages as $key => $language )
                <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-header">
                            <div class="card-title">
                                <span class="card-icon">
                                    <i class="fas fa-user-circle text-primary"></i>
                                </span>
                                <h3 class="card-label">
                                    <span class="font-weight-bolder text-info mb-4 text-hover-state-dark"> 
                                        Team Member {{ $language->language }}
                                    </span>
                                </h3>
                            </div>                    
                        </div>                        
                        <div class="card-body"> 
                            <div class="form-group">
                                <label>
                                Name                                         
                                </label>
                                <div class="input-group">
                                    <input type="text" name="{{$language->code}}[name]"  required value="{{ $teamMember->{'name:'.$language->code} }}"  class="form-control @error('name') is-invalid @enderror" placeholder="Enter name Value" />
                                </div>
                                @error("$language->code.name")
                                    <small class="form-text text-danger">{{$message}}</small>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label>
                                Position                                         
                                </label>
                                <div class="input-group">
                                    <input type="text" name="{{$language->code}}[position]"  required value="{{ $teamMember->{'position:'.$language->code} }}" class="form-control @error('position') is-invalid @enderror" placeholder="Enter position Value" />
                                </div>
                                @error("$language->code.position")
                                    <small class="form-text text-danger">{{$message}}</small>
                                @enderror
                            </div>                                                               
                        </div>                        
                    </div>                        
                </div>                
                @endforeach 
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">                     
                        <div class="card-body">   
                            <div class="form-group col-md-5">
                                <label>
                                LinkedIn 
                                </label>
                                <div class="input-group">
                                    <input type="text" name="linkedin" required value="{{ $teamMember->linkedin }}" class="form-control @error('linkedin') is-invalid @enderror" placeholder="Enter linkedin Value" />
                                </div>
                                @error('linkedin')
                                <small class="form-text text-danger">{{$message}}</small>
                                @enderror
                            </div> 
                            <div class="form-group">
                                <label >
                                    Image
                                </label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="image-input image-input-outline image-input-circle" id="kt_image_3" >
                                        <div class="image-input-wrapper" style="background-image: url({{asset($teamMember->image->url) }})"></div>
                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input type="file" name="mainImg[file]"  accept=".png, .jpg, .jpeg" />
                                        </label>
                                        @error('mainImg.file')
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>   
                            <div class="row col-md-12">
                                @foreach ($languages as $language)
                                    <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                        <label for="{{$language->code}}[alts]">
                                        Alt {{ $language->language }}
                                        </label>
                                        <div class="input-group">
                                            <input type="text" name="mainImg[{{$language->code}}][alts]" value="{{$teamMember->image->{'alts:'.$language->code} }}" class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                        </div>
                                        @error("$language->code.alts")
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>
                                @endforeach
                            </div>                                                                
                        </div>                        
                    </div>                        
                </div>     
                <div class="col-md-5"></div>
                <div class="col-md-7">                    
                    <button type="submit" class="btn btn-primary mr-2">
                        Update
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/bootstrap-switch.js')}}"></script>

@endsection