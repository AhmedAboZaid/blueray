@extends('layouts.admin_main_page')

@section('page_title','Videos')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection

@section('content')
    <form class="form" method="POST" action="{{route('videos.store')}}" enctype="multipart/form-data">
        @csrf
        <div class="container-fluid">
            <div class="row">
                @foreach ($languages as $key => $language )
                    <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="fas fa-photo-video text-primary"></i>
                                    </span>
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            Video {{ $language->language }}
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>
                                      Title
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[title]" required value="{{ @old( $language->code.'.title') }}"   class="form-control @error('title') is-invalid @enderror" placeholder="Enter Title Value" />
                                    </div>
                                    @error("$language->code.title")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label >
                                        Description
                                    </label>
                                    <div class="input-group">
                                        <textarea class="form-control @error('description') is-invalid @enderror" name="{{$language->code}}[description]" id="">{{ @old( $language->code.'.description') }} </textarea>
                                    </div>
                                    @error("$language->code.description")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="row col-md-12">
                                    <div class="form-group col-md-12">
                                        <label for="{{$language->code}}[alt]">
                                           Alt
                                        </label>
                                        <div class="input-group">
                                            <input type="text" name="{{$language->code}}[alt]" class="form-control @error('alt') is-invalid @enderror" placeholder="Enter Alt Value" />
                                        </div>
                                        @error("$language->code.alt")
                                            <small class="form-text text-danger">{{$message}}</small>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col">
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            Seo Section
                                        </span>
                                    </h3>
                                    <hr>
                                    <div class="row col-md-12">
                                        <div class="form-group col-md-6">
                                            <label>
                                                Meta Title
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][meta_title]"  value="{{ @old( 'seo.'.$language->code.'.meta_title') }}" class="form-control @error('meta_title') is-invalid @enderror" placeholder="Enter Meta Title Value" />
                                            </div>
                                            @error("$language->code.meta_title")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>
                                              Slug
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][slug]"  value="{{ @old( 'seo.'.$language->code.'.slug') }}" class="form-control @error('slug') is-invalid @enderror" placeholder="Enter slug Value" />
                                            </div>
                                            @error("$language->code.slug")
                                            <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>
                                               Meta Keywords
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="seo[{{$language->code}}][meta_keywords]"  value="{{ @old( 'seo.'.$language->code.'.meta_keywords') }}"  class="form-control @error('meta_keywords') is-invalid @enderror" placeholder="Enter Meta Keywords Value" />
                                            </div>
                                            @error("$language->code.meta_keywords")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        <div class="form-group col-md-12">
                                            <label>
                                                Meta Description
                                            </label>
                                            <div class="input-group">
                                                <textarea class="form-control @error('meta_description') is-invalid @enderror" name="seo[{{$language->code}}][meta_description]" id=""> {{ @old( 'seo.'.$language->code.'.meta_description') }}</textarea>
                                            </div>
                                            @error("$language->code.meta_description")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-body">
                            <div class="form-group col-md-12">
                                <label>
                                   Video Link
                                </label>
                                <div class="input-group">
                                    <input type="text" name="video_link" value="{{ @old('video_link') }}" class="form-control @error('video_link') is-invalid @enderror" placeholder="Enter Youtube Video Link Value" />
                                </div>
                                @error('video_link')
                                <small class="form-text text-danger">{{$message}}</small>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-5"></div>
                <div class="col-md-7">
                    <button type="submit" class="btn btn-primary mr-2">
                        Save
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
@endsection
