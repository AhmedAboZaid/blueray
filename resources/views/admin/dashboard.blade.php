@extends('layouts.admin_main_page')

@section('page_title','Dashboard')

@section('content')

<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <!--begin::Entry-->
    <div class="d-flex flex-column-fluid">
        <!--begin::Container-->
        <div class="container">
           Dashboard
        </div>
        <!--end::Container-->
    </div>
    <!--end::Entry-->
</div>

@endsection