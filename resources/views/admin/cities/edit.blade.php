@extends('layouts.admin_main_page')

@section('page_title','Cities')

@section('css')
<link href="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.css')}}" rel="stylesheet" type="text/css" />
@endsection
@section('content')
    <form class="form" method="POST" action="{{route('cities.update')}}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="id" value="{{$city->id}}">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card card-custom gutter-b example example-compact">
                        <div class="card-body">
                            <div class="form-group">
                                <label >
                                    Main Image
                                </label>
                                <div class="col-lg-9 col-xl-6">
                                    <div class="image-input image-input-outline image-input-circle" id="kt_image_3" >
                                        <div class="image-input-wrapper" style="background-image: url({{asset(@$city->image->url) }})"></div>
                                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar">
                                            <i class="fa fa-pen icon-sm text-muted"></i>
                                            <input type="file" name="image[file]" accept=".png, .jpg, .jpeg" />
                                            <input type="hidden" value="1" name="image[is_main]"  />
                                            <input type="hidden" name="profile_avatar_remove" />
                                        </label>
                                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                                            <i class="ki ki-bold-close icon-xs text-muted"></i>
                                        </span>
                                    </div>
                                    @error("image.file")
                                    <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <br>

                                <div class="row col-md-12">
                                    @foreach ($languages as $language)
                                        <div class="form-group col-md-{{($languages->count() == 1) ? 12 : 6}}">
                                            <label for="{{$language->code}}[alts]">
                                            Alt {{ $language->language }}
                                            </label>
                                            <div class="input-group">
                                                <input type="text" name="image[{{$language->code}}][alts]" value="{{ @$city->image->{'alts:'.$language->code} ?? @old('image')[$language->code]['alts'] }}" class="form-control @error('alts') is-invalid @enderror" placeholder="Enter alts Value" />
                                            </div>
                                            @error("image.$language->code.alts")
                                                <small class="form-text text-danger">{{$message}}</small>
                                            @enderror
                                        </div>
                                        @endforeach
                                </div>

                            </div>
                            <div class="form-group col-5">
                                <label>Parent id</label>
                                <div class="col-lg-12 col-md-12 col-sm-12">
                                    <select class="form-control input-lg" id="kt_select2_1" name="parent_id" >
                                        <option value="">Choose parent country</option>
                                        @foreach ($countries as $country )
                                        <option
                                           @if ($city->parent_id == $country->id)
                                                 {{'selected'}}
                                        @endif
                                        value="{{$country->id}}">{{$country->title}}</option>
                                        @endforeach
                                    </select>
                                    @error('name')
                                        <small class="form-text text-danger">{{ $message }}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @foreach ($languages as $key => $language )
                    <div class="col-md-{{($languages->count() == 1) ? 12 : 6}}">
                        <div class="card card-custom gutter-b example example-compact">
                            <div class="card-header">
                                <div class="card-title">
                                    <span class="card-icon">
                                        <i class="far fa-images text-primary"></i>
                                    </span>
                                    <h3 class="card-label">
                                        <span class="font-weight-bolder text-info mb-4 text-hover-state-dark">
                                            city {{ $language->language }}
                                        </span>
                                    </h3>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label>
                                      Title
                                    </label>
                                    <div class="input-group">
                                        <input type="text" name="{{$language->code}}[title]" required  value="{{ $city->{'title:'.$language->code} ?? @old($language->code)['title'] }}"  class="form-control @error('title') is-invalid @enderror" placeholder="Enter Title Value" />
                                    </div>
                                    @error("$language->code.title")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label >
                                        Description
                                    </label>
                                    <div class="input-group">
                                        <textarea class="form-control @error('description') is-invalid @enderror" name="{{$language->code}}[description]" id="">{{ $city->{'description:'.$language->code} ?? @old($language->code)['description'] }}</textarea>
                                    </div>
                                    @error("$language->code.description")
                                        <small class="form-text text-danger">{{$message}}</small>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach

                <div class="col-md-5"></div>
                <div class="col-md-7">
                    <button type="submit" class="btn btn-primary mr-2">
                        Update
                    </button>
                </div>
            </div>
        </div>
    </form>
@endsection


@section('js')

<script src="{{asset('admin-assets/plugins/custom/datatables/datatables.bundle.js')}}"></script>

<script src="{{asset('admin-assets/js/pages/crud/datatables/extensions/responsive.js')}}"></script>
<script src="{{asset('admin-assets/js/pages/crud/forms/widgets/form-repeater.js')}}"></script>

<script>
    var counter = 1;
    var avatar6 = new KTImageInput('kt_image_6');
    function changeKeyImageId(){
        var num = counter + 66 ;
        setTimeout(() => {
            $('#kt_image_66').attr('id','kt_image_'+num);
            new KTImageInput('kt_image_'+num);
            new KTImageInput('kt_image_66');
            counter =counter + 1;
        },1000);
     }

     setTimeout(() => {
        @if($city->attachments->count())
         $('.first-delete-btn').click();
        @endif

     }, 500);

     var attach_count = "{{ $city->attachments->count() }}";
     for (let index = 0; index < attach_count ; index++) {
        var new_count = 70 + index;
        console.log(new_count);
        new KTImageInput('kt_image_' + new_count);
     }
</script>
@endsection
