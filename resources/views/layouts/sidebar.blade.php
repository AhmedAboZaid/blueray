<!--begin::Aside Menu-->
<div class="aside-menu-wrapper flex-column-fluid" id="kt_aside_menu_wrapper">
    <!--begin::Menu Container-->
    <div id="kt_aside_menu" class="aside-menu my-4" data-menu-vertical="1" data-menu-scroll="1"
        data-menu-dropdown-timeout="500">
        <!--begin::Menu Nav-->
        <ul class="menu-nav">
            <li class="menu-item menu-item-active" aria-haspopup="true">
                <a href="#" class="menu-link menu-link-dashboard">
                    <span class="svg-icon menu-icon">
                        <i class="fas fa-tachometer-alt text-black"></i>
                    </span>
                    <span class="menu-text">Dashboard</span>
                </a>
            </li>
            <li class="menu-item menu-item {{ Request::segment(2) == 'sliders' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('sliders.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="icon-xl la la-sliders-h"></i>
                    </span>
                    <span class="menu-text">Sliders</span>
                </a>
            </li>
            <li class="menu-item menu-item {{ Request::segment(2) == 'main-pages' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('main-pages.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="icon-xl fas fa-file"></i>
                    </span>
                    <span class="menu-text">Main Pages</span>

                </a>
            </li>
            <li class="menu-item menu-item {{ Request::segment(2) == 'social-media' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('social.media.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="fas fa-icons"></i>
                    </span>
                    <span class="menu-text">Social Media</span>
                </a>
            </li>
            <li class="menu-item menu {{ Request::segment(2) == 'branch' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('branch.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="fas fa-shield-alt "></i>
                    </span>
                    <span class="menu-text">Branch</span>
                </a>
            </li>
            <li class="menu-item menu-item-submenu {{ Request::segment(2) == 'media-center' ? 'menu-item-open' : '' }}"
                aria-haspopup="true" data-menu-toggle="hover">
                <a href="javascript:;" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon ">
                        <i class="fas fa-building "></i></span>
                    </span>
                    <span class="menu-text">Media Center</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                        <li class="menu-item menu-item-parent" aria-haspopup="true">
                            <span class="menu-link">
                                <span class="menu-text">{{ __('Types') }}</span>
                            </span>
                        </li>
                        <li class="menu-item menu {{ Request::segment(2) == 'media-center' &&  Request::segment(3) == 'blogs' ? 'menu-item-active' : '' }}"
                            aria-haspopup="true">
                            <a href="{{ route('blogs.index') }}" class="menu-link">
                                <span class="svg-icon menu-icon">
                                    <i class="fab fa-blogger-b"></i>
                                </span>
                                <span class="menu-text">Blogs</span>
                            </a>
                        </li>

                        <li class="menu-item menu {{ Request::segment(2) == 'media-center'  &&  Request::segment(3) == 'videos'  ? 'menu-item-active' : '' }}"
                            aria-haspopup="true">
                            <a href="{{ route('videos.index') }}" class="menu-link">
                                <span class="svg-icon menu-icon">
                                    <i class="fas fa-photo-video"></i>
                                </span>
                                <span class="menu-text">Videos</span>
                            </a>
                        </li>
                        <li class="menu-item menu {{ Request::segment(2) == 'media-center'  &&  Request::segment(3) == 'albums' ? 'menu-item-active' : '' }}"
                            aria-haspopup="true">
                            <a href="{{ route('albums.index') }}" class="menu-link">
                                <span class="svg-icon menu-icon">
                                    <i class="far fa-images"></i>
                                </span>
                                <span class="menu-text">Albums</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="menu-item menu {{ Request::segment(2) == 'cities' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('cities.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="fas fa-building"></i>
                    </span>
                    <span class="menu-text">Cities</span>
                </a>
            </li>
            <li class="menu-item menu {{ Request::segment(2) == 'services' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('services.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="fab fa-servicestack"></i>
                    </span>
                    <span class="menu-text">Services</span>
                </a>
            </li>
            <li class="menu-item menu {{ Request::segment(2) == 'tags' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('tags.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="fas fa-hashtag"></i>
                    </span>
                    <span class="menu-text">Tags</span>
                </a>
            </li>
            <li class="menu-item menu-item-submenu {{ Request::segment(2) == 'about' ? 'menu-item-open' : '' }}"
                aria-haspopup="true" data-menu-toggle="hover">
                <a href="javascript:;" class="menu-link menu-toggle">
                    <span class="svg-icon menu-icon ">
                        <i class="fas fa-building "></i></span>
                    </span>
                    <span class="menu-text">About</span>
                    <i class="menu-arrow"></i>
                </a>
                <div class="menu-submenu">
                    <i class="menu-arrow"></i>
                    <ul class="menu-subnav">
                        <li class="menu-item menu-item-parent" aria-haspopup="true">
                            <span class="menu-link">
                                <span class="menu-text">About</span>
                            </span>
                        </li>
                        <li class="menu-item menu {{ Request::segment(2) == 'about'  &&  Request::segment(3) == 'team-members' ? 'menu-item-active' : '' }}"
                            aria-haspopup="true">
                            <a href="{{ route('team.members.index') }}" class="menu-link">
                                <span class="svg-icon menu-icon">
                                    <i class="fas fa-user-circle"></i>
                                </span>
                                <span class="menu-text">Team Members</span>
                            </a>
                        </li>

                        <li class="menu-item menu {{ Request::segment(2) == 'about'  &&  Request::segment(3) == 'sections' ? 'menu-item-active' : '' }}"
                            aria-haspopup="true">
                            <a href="{{ route('section.index') }}" class="menu-link">
                                <span class="svg-icon menu-icon">
                                    <i class="far fa-newspaper"></i>
                                </span>
                                <span class="menu-text">Sections</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="menu-item menu {{ Request::segment(2) == 'tourism-type' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('tourism.type.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="fas fa-umbrella"></i>
                    </span>
                    <span class="menu-text">Tourism Type</span>
                </a>
            </li>
            <li class="menu-item menu {{ Request::segment(2) == 'packages' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('packages.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="fas fa-building"></i>
                    </span>
                    <span class="menu-text">Packages</span>
                </a>
            </li>
            <li class="menu-item menu {{ Request::segment(2) == 'facility' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('facility.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="far fa-gem"></i>
                    </span>
                    <span class="menu-text">Facility</span>
                </a>
            </li>
            <li class="menu-item menu {{ Request::segment(2) == 'settings' ? 'menu-item-active' : '' }}"
                aria-haspopup="true">
                <a href="{{ route('settings.index') }}" class="menu-link">
                    <span class="svg-icon menu-icon">
                        <i class="fas fa-cog"></i>
                    </span>
                    <span class="menu-text">Settings</span>
                </a>
            </li>
            <li class="menu-item menu-item-submenu menu-item-rel pull-right" aria-haspopup="true">
                <a class="menu-link" href="{{ route('logout') }}"
                    onclick="event.preventDefault(); if(confirm('Do You Want To Logout ?'))document.getElementById('logout-form').submit();">
                    <span class="svg-icon menu-icon">
                        <i class="fas fa-sign-out-alt"></i>
                    </span>
                    <span class="menu-text">{{ __('Logout') }}</span>
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                    @csrf
                </form>
            </li>
        </ul>
        <!--end::Menu Nav-->
    </div>
    <!--end::Menu Container-->
</div>
<!--end::Aside Menu-->
