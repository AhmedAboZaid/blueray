<!DOCTYPE html>
<html lang="en">
	<!--begin::Head-->
	<head>
		<meta charset="utf-8" />
		<title>@yield('page_title')</title>
		<meta name="description" content="Blueray" />
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
		<!--begin::Fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
		<!--end::Fonts-->

		<!--begin::Global Theme Styles(used by all pages)-->
		<link href="{{ asset('admin-assets/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('admin-assets/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
		<link href="{{ asset('admin-assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles-->
		<!--begin::Layout Themes(used by all pages)-->
		<!--end::Layout Themes-->
	 <link href="{{ asset('admin-assets/css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css" /> 
     <link href="{{ asset('admin-assets/css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css" /> 
		<link rel="shortcut icon" href="{{asset('admin-assets/media/logos/favicon.ico')}}" />
		<meta name="csrf-token" content="{{ csrf_token() }}">
		@yield('css')
	</head>
	<!--end::Head-->
    <!--begin::Body-->
    <body id="kt_body" class="header-fixed header-mobile-fixed subheader-enabled subheader-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
		<!--begin::Main-->
  <!--begin::Header Mobile-->
  <div id="kt_header_mobile" class="header-mobile header-mobile-fixed">
	<!--begin::Logo-->
	<a href="#">
		<img alt="Logo" src="{{asset('uploads/logo.png')}}" class="logo-default max-h-30px" />
	</a>

	<!--end::Logo-->
	<!--begin::Toolbar-->
	<div class="d-flex align-items-center">
		<!--begin::Aside Mobile Toggle-->
		<button class="btn p-0 burger-icon burger-icon-left" id="kt_aside_mobile_toggle">
			<span>Dashboard</span>
		</button>
		<!--end::Aside Mobile Toggle-->

	</div>
	<!--end::Toolbar-->
</div>
<!--end::Header Mobile-->
		<div class="d-flex flex-column flex-root">
			<!--begin::Page-->
			<div class="d-flex flex-row flex-column-fluid page">
				<!--begin::Aside-->
				<div class="aside aside-left aside-fixed d-flex flex-column flex-row-auto" id="kt_aside">

					 <!--begin::Brand-->
					 <div class="brand flex-column-auto mt-5" id="kt_brand">
						<!--begin::Logo-->
						<a href="#" class="brand-logo">
							<img alt="Logo" src="{{asset('uploads/logo.png')}}" style="width: 100%;height: 100%;"/>
						</a>
						<!--end::Logo-->
						<!--begin::Toggle-->
						<button class="brand-toggle btn btn-sm px-0" id="kt_aside_toggle">
							<span class="svg-icon svg-icon svg-icon-xl">
								<!--begin::Svg Icon | path:assets/media/svg/icons/Navigation/Angle-double-left.svg-->
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
									width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
									<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
										<polygon points="0 0 24 0 24 24 0 24" />
										<path
											d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z"
											fill="#000000" fill-rule="nonzero"
											transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999)" />
										<path
											d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z"
											fill="#000000" fill-rule="nonzero" opacity="0.3"
											transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999)" />
									</g>
								</svg>
								<!--end::Svg Icon-->
							</span>
						</button>
						<!--end::Toolbar-->
					</div>
					@include('layouts.sidebar')
					<!--end::Brand-->
					<!--begin::Footer-->
					<!--end::Footer-->
				</div>
				<!--end::Aside-->
				<!--begin::Wrapper-->
				<div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">
					<!--begin::Header-->
					<div id="kt_header" class="header header-fixed">
						<!--begin::Container-->
						<div class="container-fluid d-flex align-items-stretch justify-content-between">
							<!--begin::Header Menu Wrapper-->
							<div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
							</div>
							<!--end::Header Menu Wrapper-->
							<!--begin::Topbar-->
							<div class="topbar">
							</div>
							<!--end::Topbar-->
						</div>
						<!--end::Container-->
					</div>
					<!--end::Header-->
					<!--begin::Content-->
					<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
						<!--begin::Subheader-->

						<div
							class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
							<!--begin::Info-->

							<div class="d-flex align-items-center flex-wrap mr-2">
								<!--begin::Page Title-->
								@yield('page-info')
								<!--end::Page Title-->
							</div>
							<!--end::Info-->
						</div>

						<!--end::Subheader-->
						<!--begin::Entry-->
						<div class="d-flex flex-column-fluid">
							<!--begin::Container-->
							<div class="container">
								<!--begin::Dashboard-->
								<!--begin::Row-->
								<div class="row">
									<div class="col-lg-12">
										<!--begin::Mixed Widget 1-->
										<div class="card card-custom bg-gray-100 card-stretch gutter-b">
											<!--begin::Body-->
											<div class="card-body p-0 position-relative overflow-hidden">
												@yield('content')
											</div>
											<!--end::Body-->
										</div>
										<!--end::Mixed Widget 1-->
									</div>
								</div>
								<!--end::Row-->
								<!--end::Dashboard-->
							</div>
							<!--end::Container-->
						</div>
						<!--end::Entry-->
					</div>
					<!--end::Content-->
					<!--begin::Footer-->
					<div class="footer bg-white py-4 d-flex flex-lg-column" id="kt_footer">
						<!--begin::Container-->
						<div
							class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between">
						</div>
						<!--end::Container-->
					</div>
					<!--end::Footer-->
				</div>
				<!--end::Wrapper-->

			</div>
			<!--end::Page-->
		</div>
		<!--end::Main-->
		<script>var HOST_URL = "https://preview.keenthemes.com/metronic/theme/html/tools/preview";</script>
		<!--begin::Global Config(global config for global JS scripts)-->
		<script>var KTAppSettings = { "breakpoints": { "sm": 576, "md": 768, "lg": 992, "xl": 1200, "xxl": 1200 }, "colors": { "theme": { "base": { "white": "#ffffff", "primary": "#8950FC", "secondary": "#E5EAEE", "success": "#1BC5BD", "info": "#8950FC", "warning": "#FFA800", "danger": "#F64E60", "light": "#F3F6F9", "dark": "#212121" }, "light": { "white": "#ffffff", "primary": "#E1E9FF", "secondary": "#ECF0F3", "success": "#C9F7F5", "info": "#EEE5FF", "warning": "#FFF4DE", "danger": "#FFE2E5", "light": "#F3F6F9", "dark": "#D6D6E0" }, "inverse": { "white": "#ffffff", "primary": "#ffffff", "secondary": "#212121", "success": "#ffffff", "info": "#ffffff", "warning": "#ffffff", "danger": "#ffffff", "light": "#464E5F", "dark": "#ffffff" } }, "gray": { "gray-100": "#F3F6F9", "gray-200": "#ECF0F3", "gray-300": "#E5EAEE", "gray-400": "#D6D6E0", "gray-500": "#B5B5C3", "gray-600": "#80808F", "gray-700": "#464E5F", "gray-800": "#1B283F", "gray-900": "#212121" } }, "font-family": "Poppins" };</script>
		<!--end::Global Config-->
		<!--begin::Global Theme Bundle(used by all pages)-->
		<script src="{{ asset('admin-assets/plugins/global/plugins.bundle.js')}}"></script>
		<script src="{{ asset('admin-assets/js/all.js') }}"></script>
		<script src="{{ asset('admin-assets/plugins/custom/prismjs/prismjs.bundle.js')}}"></script>
		<script src="{{ asset('admin-assets/js/scripts.bundle.js')}}"></script>
		<!--end::Global Theme Bundle-->
		<!--begin::Page Scripts(used by this page)-->
		<script src="{{ asset('admin-assets/js/pages/widgets.js')}}"></script>
		<script src="{{ asset('admin-assets/js/pages/crud/file-upload/image-input.js')}}"></script>
		<script src="{{ asset('admin-assets/plugins/custom/ckeditor/ckeditor-classic.bundle.js') }}"></script>
		<script src="https://cdn.ckeditor.com/4.17.1/full/ckeditor.js"></script>


		<script>
			$('.editor').each(function(e){
                CKEDITOR.replace( this.id,  {
                    height: 180,
                    baseFloatZIndex: 10005,
                    removeButtons: 'PasteFromWord'
                });
            });

			function deleteData(url , id)
            {
				console.log(id);
				Swal.fire({
                    title: "Are you sure?",
                    text: "You won't be able to revert this!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "<i class='fa fa-check'></i> Yes, Delete it!",
                    cancelButtonText: "<i class='fa fa-times'></i> No, Cancel!",
                    reverseButtons: true,
                    customClass: {
                        confirmButton: "btn btn-danger",
                        cancelButton: "btn btn-success"
                    }
                }).then(function(result) {
                    if (result.value)
                    {
                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                _token: CSRF_TOKEN,
                                 id:id,
                            },
                            dataType: 'JSON',
                            success: function (data) {
								if(data == 'success')
								{
									Swal.fire(
										"Deleted !!",
										"Your Data Has Been Deleted.",
										"success"
									).then(function() {
										location.reload();
									})
								}
                                else
                                {
                                    Swal.fire(
										"Sorry !!",
                                    "Cannot delete Data",
                                    "error"
                                    )
                                }
                            }
                            ,error: function (data) {
                                Swal.fire(
                                    "Sorry !!",
                                    data,
                                    "error"
                                )
                            }
                        });

                    }
                    else if (result.dismiss === "cancel")
                    {
                        Swal.fire(
                            "Cancelled !!",
                            "",
                            "info"
                        )
                    }
                });
            }

			function changeStatus(url , id)
            {
                Swal.fire({
                    title: "Are you sure?",
                    text: "Change Status !!",
                    icon: "warning",
                    showCancelButton: true,
                    confirmButtonText: "<i class='fa fa-check'></i> Yes, Change it!",
                    cancelButtonText: "<i class='fa fa-times'></i> No, Cancel!",
                    reverseButtons: true,
                    customClass: {
                        confirmButton: "btn btn-success",
                        cancelButton: "btn btn-danger"
                    }
                }).then(function(result) {
                    if (result.value)
                    {
                        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: {
                                _token: CSRF_TOKEN,
                                 id:id,
                                 isActive : 1
                            },
                            dataType: 'JSON',
                            success: function (data) {
                                if(data == 'success')
                                {
                                    Swal.fire(
                                        "Changed !!",
                                        "Status Has Been Changed.",
                                        "success"
                                    ).then(function() {
                                        location.reload();
                                    })
                                }
                                else
                                {
                                    Swal.fire(
                                        "Sorry !!",
                                        "Cannot Change Status",
                                        "error"
                                    )
                                }
                            }
                            ,error: function (data) {
                                Swal.fire(
                                    "Sorry !!",
                                    data,
                                    "error"
                                )
                            }
                        });

                    }
                    else if (result.dismiss === "cancel")
                    {
                        Swal.fire(
                            "Cancelled !!",
                            "",
                            "info"
                        )
                    }
                });
			}

			setTimeout(function() {
				$('#successMessage').fadeOut('slow');
			}, 1500);

		 @if (Session::has('success'))
            Swal.fire("Success", "{{ Session::get('success') }}", "success");
        @endif
        @if (Session::has('error'))
            Swal.fire("error", "{{ Session::get('error') }}", "error");
        @endif
        @if (Session::has('warning'))
            Swal.fire("warning", "{{ Session::get('warning') }}", "warning");
        @endif

        </script>
        <!--end::Page Scripts-->
        @yield('js')
	</body>
	<!--end::Body-->
</html>
