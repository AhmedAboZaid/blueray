<?php

namespace Database\Seeders;

use App\Models\Service;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('seos')->where('seoable_type','App\Models\Service')->delete();
        DB::table('attachment_translations')->whereIn('attachment_id', DB::table('attachments')->where('attachmentable_type','App\Models\Service')->pluck('id')->toArray())->delete();
        DB::table('attachments')->where('attachmentable_type','App\Models\Service')->delete();
        DB::table('service_translations')->truncate();
        DB::table('services')->truncate();
        Schema::enableForeignKeyConstraints();

        $servicesEnglishName = array("First", "Second", "Third","Fourth", "Fifth");
        $servicesArabicName = array("اول", "ثانى", "ثالث","رابع", "خامس");


        $serviceMainImage = array("service1.jpg", "service2.jpg", "service3.jpg","service4.jpg", "service5.jpg", "service6.jpg");

        for($j = 0; $j <5 ; $j++){
            $service = Service::create([
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'en' => [
                    'title' => $servicesEnglishName[$j],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'es' => [
                    'title' => $servicesEnglishName[$j],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'ar' => [
                    'title' => $servicesArabicName[$j],
                    'description' => 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.'
                ],
            ]);

            // $service->seo()->create([
            //     'en' => [
            //         'slug' => Str::slug($servicesEnglishName[$j], '-'),
            //         'meta_title' => $servicesEnglishName[$j].' meta_title' ,
            //         'meta_keywords' => $servicesEnglishName[$j].' meta_keywords',
            //         'meta_description' => $servicesEnglishName[$j].' meta_description ',
            //         ],
            //     'ar' => [
            //         'slug' => Str::slug($servicesArabicName[$j], '-'),
            //         'meta_title' => $servicesArabicName[$j] ,
            //         'meta_keywords' => $servicesArabicName[$j],
            //         'meta_description' => $servicesArabicName[$j],
            //         ],
            // ]);

            // for($i = 1 ;$i <=5 ; $i++){
            //     $service->attachments()->create([
            //         'is_main' =>($i == 1) ? 1 : 0,
            //         'file' => ($i == 1 ) ?   $serviceMainImage[$j] : 'att1_'.$i.'.jpg',
            //         'en' => [
            //             'alts' =>'service ' .$servicesEnglishName[$i-1]. ' attachment ',
            //             ],
            //         'ar' => [
            //             'alts' => $servicesArabicName[$i-1].' صورة الخدمه ' ,
            //             ],
            //     ]);
            // }
            // $service->tags()->sync(rand(1,6));
        }



    }
}
