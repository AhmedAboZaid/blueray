<?php

namespace Database\Seeders;

use App\Models\Slider;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SliderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('sliders')->truncate();
        DB::table('slider_translations')->truncate();
        Schema::enableForeignKeyConstraints();

        //slider 1
        $sliders = Slider::create([
            'btn' => 'https://www.google.com',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'First Slider title in english',
                'short_description' => 'First Slider Short Descriptions In English'
            ],
            'es' => [
                'title' => 'First Slider title in english',
                'short_description' => 'First Slider Short Descriptions In English'
            ],
            'ar' => [
                'title' => 'عنوان الغلاف الاول بالعربية ',
                'short_description' => 'اول وصف قصير للغلاف باللغة العربية'
            ],
        ]);
        $sliders->image()->create([
            'file' => '1640002181.png',
            'en' => [
                'alts' =>  'First Slider alt in english',
            ],
            'es' => [
                'alts' =>  'First Slider alt in english',
            ],
            'ar' => [
               'alts' => 'اول وصف قصير للغلاف باللغة العربية'
            ],
        ]);

         //slider 2
         $sliders = Slider::create([
            'btn' => 'https://www.youtube.com/',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Second slider title in english ',
                'short_description' => 'Second Slider Short Descriptions In English'
            ],
            'es' => [
                'title' => 'Second slider title in english ',
                'short_description' => 'Second Slider Short Descriptions In English'
            ],
            'ar' => [
                'title' => 'عنوان الغلاف الثانى بالعربية ',
                'short_description' => 'ثانى وصف قصير للغلاف باللغة العربية'
             ],
        ]);
        $sliders->image()->create([
            'file' => '1639649002.png',
            'en' => [
                'alts' =>  'Second Slider alt in english',
            ],
            'es' => [
                'alts' =>  'Second Slider alt in english',
            ],
            'ar' => [
               'alts' => 'ثانى وصف قصير للغلاف باللغة العربية'
            ],
        ]);

         //slider 3
         $sliders = Slider::create([
            'btn' => 'https://www.youtube.com/',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Third slider title in english ',
                'short_description' => 'Third Slider Short Descriptions In English'
            ],
            'es' => [
                'title' => 'Third slider title in english ',
                'short_description' => 'Third Slider Short Descriptions In English'
            ],
            'ar' => [
                'title' => 'عنوان الغلاف الثالث بالعربية ',
                'short_description' => 'الثالث وصف قصير للغلاف باللغة العربية'
             ],
        ]);
        $sliders->image()->create([
            'file' => '1639649580.png',
            'en' => [
                'alts' =>  'Third Slider alt in english',
            ],
            'es' => [
                'alts' =>  'Third Slider alt in english',
            ],
            'ar' => [
               'alts' => 'الثالث وصف قصير للغلاف باللغة العربية'
            ],
        ]);

          //slider 4
        $sliders = Slider::create([
            'btn' => 'https://www.youtube.com/',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Fourth slider title in english ',
                'short_description' => 'Fourth Slider Short Descriptions In English'
            ],
            'es' => [
                'title' => 'Fourth slider title in english ',
                'short_description' => 'Fourth Slider Short Descriptions In English'
            ],
            'ar' => [
                'title' => 'عنوان الغلاف الرابع بالعربية ',
                'short_description' => 'رابع وصف قصير للغلاف باللغة العربية'
             ],
        ]);
        $sliders->image()->create([
            'file' => '1639900421.png',
            'en' => [
                'alts' =>  'Fourth Slider ali in english',
            ],
            'es' => [
                'alts' =>  'Fourth Slider ali in english',
            ],
            'ar' => [
               'alts' => 'رابع وصف قصير للغلاف باللغة العربية'
            ],
        ]);
    }
}
