<?php

namespace Database\Seeders;

use App\Models\Album;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class AlbumSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('albums')->truncate();
        DB::table('album_translations')->truncate();
        DB::table('attachments')->truncate();
        DB::table('attachment_translations')->truncate();
        DB::table('seos')->truncate();
        DB::table('seo_translations')->truncate();
        Schema::enableForeignKeyConstraints();


        $albumEnglishName = array("First", "Second", "Third","Fourth", "Fifth", "Sexth", "Seventh","eighth", "ninth", "tenth");
        $albumSpanishName = array("First", "Second", "Third","Fourth", "Fifth", "Sexth", "Seventh","eighth", "ninth", "tenth");
        $albumMainImage = array("album1.png", "album2.png", "album3.png","album4.png", "album5.png", "album6.png");
        $albumArabicName = array("اول", "ثانى", "ثالث","رابع", "خامس", "سادس", "سابع","ثامن", "تاسع", "عاشر");

        for($j = 0; $j <6 ; $j++){
            $album = Album::create([
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'en' => [
                    'title' => $albumEnglishName[$j],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'es' => [
                    'title' => $albumSpanishName[$j],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'ar' => [
                    'title' => $albumArabicName[$j],
                    'description' => 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.'
                ],
            ]);

            $album->seo()->create([
                'en' => [
                    'slug' => Str::slug($albumEnglishName[$j], '-'),
                    'meta_title' => $albumEnglishName[$j].' meta_title' ,
                    'meta_keywords' => $albumEnglishName[$j].' meta_keywords',
                    'meta_description' => $albumEnglishName[$j].' meta_description ',
                    ],
                'es' => [
                    'slug' => Str::slug($albumSpanishName[$j], '-'),
                    'meta_title' => $albumSpanishName[$j].' meta_title' ,
                    'meta_keywords' => $albumSpanishName[$j].' meta_keywords',
                    'meta_description' => $albumSpanishName[$j].' meta_description ',
                    ],
                'ar' => [
                    'slug' => Str::slug($albumArabicName[$j], '-'),
                    'meta_title' => $albumArabicName[$j] ,
                    'meta_keywords' => $albumArabicName[$j],
                    'meta_description' => $albumArabicName[$j],
                    ],
            ]);

            for($i = 1 ;$i <=10 ; $i++){
                $album->attachments()->create([
                    'is_main' =>($i == 1) ? 1 : 0,
                    'file' => ($i == 1 ) ?   $albumMainImage[$j] : 'att1_'.$i.'.jpg',
                    'en' => [
                        'alts' =>'Album ' .$albumEnglishName[$i-1]. ' attachment ',
                        ],
                    'es' => [
                        'alts' =>'Album ' .$albumSpanishName[$i-1]. ' attachment ',
                        ],
                    'ar' => [
                        'alts' => $albumArabicName[$i-1].' صورة الالبوم ' ,
                        ],
                ]);
            }
        }
    }
}
