<?php

namespace Database\Seeders;

use App\Models\City;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('seos')->where('seoable_type','App\Models\City')->delete();
        DB::table('city_translations')->truncate();
        DB::table('attachment_translations')->whereIn('attachment_id', DB::table('attachments')->where('attachmentable_type','App\Models\City')->pluck('id')->toArray())->delete();
        DB::table('attachments')->where('attachmentable_type','App\Models\City')->delete();
        DB::table('cities')->truncate();
        Schema::enableForeignKeyConstraints();

        $cities = [
            "Egypt" => [
                "New Cairo"        =>  "القاهرة الجديدة",
                "Al Mansurah"      =>  "المنصورة" ,
                "Al Marj"          =>  "المرج" ,
                "Alexandria"       =>  "الإسكندرية",
                "Almazah"          =>  "الماظة",
                "Ar Rawdah"        =>  "الروض",
                "Assiut"           =>  "أسيوط" ,
                "Az Zamalik"       =>  "الزمالك" ,
                "Heliopolis"       =>  "مصر الجديدة",
            ],
            "United States"=> [
                "Agoura Hills"        => "أجورا هيلز",
                "Abbeville"           => "أبفيل",
            ]
        ];

        $CitiesEnglishName  = array("Egypt", "United States");
        $CitiesArabicName   = array("مصر", "الولايات المتحدة");

        foreach ($CitiesEnglishName as $k => $country)
        {
            $City = City::create([
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
                'en' => [
                    'title'         => $country,
                    'description'   => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
                ],
                'es' => [
                    'title'         => $country,
                    'description'   => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
                ],
                'ar' => [
                    'title'         => $CitiesArabicName[$k] ,
                    'description'   => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا',
                ],
            ]);
            foreach (@$cities[$country] ?? [] as $city_en => $city_ar) {
                City::create([
                    'created_at'        => Carbon::now(),
                    'updated_at'        => Carbon::now(),
                    'parent_id'         =>  $City->id,
                    'en' => [
                        'title'         => $city_en,
                        'description'   => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
                    ],
                    'es' => [
                        'title'         => $city_en,
                        'description'   => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
                    ],
                    'ar' => [
                        'title'         => $city_ar ,
                        'description'   => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا',
                    ],
                ]);
            }

            // $City->seo()->create([
            //     'en'        =>
            //     [
            //         'meta_title'        => ' City meta_title'. $k+1 ,
            //         'slug'              => Str::slug('First City' . $k+1  , '-'),
            //         'meta_keywords'     => ' City meta_keywords'. $k+1,
            //         'meta_description'  => ' City meta_description '. $k+1,
            //     ],
            //     'ar' =>
            //     [
            //         'meta_title'        => ' المدينة '   . $CitiesArabicName[$k] ,
            //         'slug'              => Str::slug('المدينة '  . $CitiesArabicName[$k]  , '-'),
            //         'meta_keywords'     => ' المدينة '  . $CitiesArabicName[$k] ,
            //         'meta_description'  => ' المدينة  '  . $CitiesArabicName[$k] ,
            //     ]
            // ]);

            // $inc = 0;
            // for($i = 1 ;$i <=6 ; $i++)
            // {
            //     $inc ++;
            //     $City->attachments()->create([
            //         'file'      => 'att1_'.$i.'.jpg',
            //         'is_main'   => ($i == 1) ? 1 : 0,
            //         'en'        =>
            //         [
            //             'alts'  => "City $country attachment $i ",
            //         ],
            //         'ar'        =>
            //         [
            //             'alts' => ' صورة المدينة ' .$CitiesArabicName[$k] . $CitiesArabicName[$k],
            //         ],
            //     ]);
            // }

            // dd($inc);

            // $City->tags()->sync([1,2,3,4]);
        }
    }
}
