<?php

namespace Database\Seeders;

use App\Models\MainPage;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class MainPageSeederAPI extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('seos')->where('seoable_type','App\Models\MainPage')->delete();
        DB::table('main_pages')->truncate();
        DB::table('main_page_translations')->truncate();
        Schema::enableForeignKeyConstraints();

        // $mainPages = new MainPage();
        $pageNameEnglish = ["About","Services","Ticketing","Transportation","Tourisms","Media","Blogs","Albums","Videos","Contact Us",'Destination'];
        $fixed_names = ["about","services","ticketing","transportation","tourisms","media","blogs","albums","videos","contact",'destination'];
        $pageNameArabic = ['من نحن','الخدمات' , "التذاكر" , "النقل" , "السياحة" , "الوسائط" , "المدونات" , "الألبومات" , "مقاطع الفيديو" , "اتصل بنا","Destination"];
        $parent_ids     = [null,null,2,2,null,null,6,6,6,null,null];
        $pageImageEnglish = ['1640089701_en.png', 'mediacenter_en.png', 'blog_en.jpg', 'albums_en.png', 'video_en.jpg', 'services_en.png' ,'project_en.png',
                            'solutions_en.png', 'clients_ar.png','faqs_en.png', 'contact_en.png', 'policies_en.png', 'policies_en.png', 'policies_en.png'];
        $pageImageArabic = ['1640089701_ar.png', 'media_center_ar.jpg', 'blog_ar.png', 'albums_ar.jpg', 'video_ar.jpg', 'services_ar.png' ,'project_ar.png',
                            'solutions_ar.png', 'clients_ar.png','faqs_ar.png', 'contact_ar.png', 'policies_ar.png', 'policies_ar.png', 'policies_en.png'];

        for($i = 0 ; $i< count($pageNameEnglish); $i++){
            $mainPage = MainPage::create([
                'fixed_name' => $fixed_names[$i],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'parent_id' => $parent_ids[$i],
                'en' => [
                    'title' => $pageNameEnglish[$i],
                    'image' => $pageImageEnglish[$i],
                    'alt' => $pageNameEnglish[$i],
                    'description' => $i == count($pageNameEnglish) -1 ? "Select Your Best Package For Your Travel" : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'es' => [
                    'title' => $pageNameEnglish[$i],
                    'image' => $pageImageEnglish[$i],
                    'alt' => $pageNameEnglish[$i],
                    'description' => $i == count($pageNameEnglish) -1 ? "Select Your Best Package For Your Travel" : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'ar' => [
                    'title' => $pageNameArabic[$i],
                    'image' => $pageImageArabic[$i],
                    'alt' => $pageNameArabic[$i],
                    'description' => $i == count($pageNameEnglish) -1 ? "Select Your Best Package For Your Travel" : 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.'
                ],
            ]);
            $mainPage->seo()->create([
                'en' => [
                    'slug' => Str::slug($pageNameEnglish[$i], '-'),
                    'meta_title' => $pageNameEnglish[$i].' meta_title' ,
                    'meta_keywords' => $pageNameEnglish[$i].' meta_keywords',
                    'meta_description' => $pageNameEnglish[$i].' meta_description ',
                    ],
                'es' => [
                    'slug' => Str::slug($pageNameEnglish[$i], '-'),
                    'meta_title' => $pageNameEnglish[$i].' meta_title' ,
                    'meta_keywords' => $pageNameEnglish[$i].' meta_keywords',
                    'meta_description' => $pageNameEnglish[$i].' meta_description ',
                    ],
                'ar' => [
                    'slug' => Str::slug($pageNameArabic[$i], '-'),
                    'meta_title' => $pageNameArabic[$i] ,
                    'meta_keywords' => $pageNameArabic[$i],
                    'meta_description' => $pageNameArabic[$i],
                    ],
            ]);
        }

    }
}
