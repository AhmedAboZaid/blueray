<?php

namespace Database\Seeders;

use App\Models\TeamMember;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TeamMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('team_members')->truncate();
        DB::table('team_member_translations')->truncate();
        Schema::enableForeignKeyConstraints();

        $teamMember =TeamMember::create([
            'linkedin' => 'https://www.linkedin.com/',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'name' => 'Mohamed Ali',
                'position' => 'Operation Manager',
            ],
            'es' => [
                'name' => 'Mohamed Ali',
                'position' => 'Operation Manager',
            ],
            'ar' => [
                'name' => 'محمد على',
                'position' => 'مدير',
            ],
        ]);
        $teamMember->image()->create([
            'file' => 'member8.jpg',
            'en' => [
                'alts' => 'Mohamed Ali',
            ],
            'es' => [
                'alts' => 'Mohamed Ali',
            ],
            'ar' => [
                'alts' => 'محمد على',
            ],
        ]);

        $teamMember =TeamMember::create([
            'linkedin' => 'https://www.linkedin.com/',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'name' => 'Ahmed AboZaid',
                'position' => 'Team Lead',
            ],
            'es' => [
                'name' => 'Ahmed AboZaid',
                'position' => 'Team Lead',
            ],
            'ar' => [
                'name' => 'احمد ابو زيد',
                'position' => 'تيم ليد',
            ],
        ]);
        $teamMember->image()->create([
            'file' => 'member1.jpg',
            'en' => [
                'alts' => 'Ahmed AboZaid',
            ],
            'es' => [
                'alts' => 'Ahmed AboZaid',
            ],
            'ar' => [
               'alts' => ' احمد ابو زيد ',
            ],
        ]);

        $teamMember =TeamMember::create([
            'linkedin' => 'https://www.linkedin.com/',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'name' => 'Ahmed Diab',
                'position' => 'Senior',
            ],
            'es' => [
                'name' => 'Ahmed Diab',
                'position' => 'Senior',
            ],
            'ar' => [
                'name' => 'احمد دياب',
                'position' => ' سينيور',
            ],
        ]);
        $teamMember->image()->create([
            'file' => 'member2.jpg',
            'en' => [
                'alts' => 'Ahmed Diab',
            ],
            'es' => [
                'alts' => 'Ahmed Diab',
            ],
            'ar' => [
               'alts' => 'احمد دياب ',
            ],
        ]);

        $teamMember =TeamMember::create([
            'linkedin' => 'https://www.linkedin.com/',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'name' => ' Mohamed Abbas',
                'position' => 'Developer',
            ],
            'es' => [
                'name' => ' Mohamed Abbas',
                'position' => 'Developer',
            ],
            'ar' => [
                'name' => 'محمد عباس',
                'position' => 'ديفلوبر',
            ],
        ]);
        $teamMember->image()->create([
            'file' => 'member3.png',
            'en' => [
                'alts' => 'Mohamed Abbas',
            ],
            'es' => [
                'alts' => 'Mohamed Abbas',
            ],
            'ar' => [
               'alts' => ' محمد عباس ',
            ],
        ]);


        $teamMember =TeamMember::create([
            'linkedin' => 'https://www.linkedin.com/',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'name' => 'Mohamed Gaber',
                'position' => 'Developer',
            ],
            'es' => [
                'name' => 'Mohamed Gaber',
                'position' => 'Developer',
            ],
            'ar' => [
                'name' => 'محمد جابر',
                'position' => 'ديفلوبر',
            ],
        ]);
        $teamMember->image()->create([
            'file' => 'member4.png',
            'en' => [
                'alts' => 'Mohamed Gaber',
            ],
            'es' => [
                'alts' => 'Mohamed Gaber',
            ],
            'ar' => [
               'alts' => ' محمد جابر ',
            ],
        ]);

        $teamMember =TeamMember::create([
            'linkedin' => 'https://www.linkedin.com/',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'name' => 'Bassant',
                'position' => 'Developer',
            ],
            'es' => [
                'name' => 'Bassant',
                'position' => 'Developer',
            ],
            'ar' => [
                'name' => 'يسنت',
                'position' => 'ديفلوبر',
            ],
        ]);
        $teamMember->image()->create([
            'file' => 'member5.png',
            'en' => [
                'alts' => 'Bassant',
            ],
            'es' => [
                'alts' => 'Bassant',
            ],
            'ar' => [
               'alts' =>  ' يسنت',
            ],
        ]);

        

    }
}
