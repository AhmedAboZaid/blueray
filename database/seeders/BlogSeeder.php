<?php

namespace Database\Seeders;

use App\Models\Blog;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;


class BlogSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Schema::disableForeignKeyConstraints();
        DB::table('taggables')->where('taggable_type','App\Models\Blog')->truncate();
        DB::table('attachments')->where('attachmentable_type','App\Models\Blog')->delete();
        DB::table('seos')->where('seoable_type','App\Models\Blog')->delete();
        DB::table('blog_translations')->delete();
        DB::table('blogs')->delete();
        // Schema::enableForeignKeyConstraints();

        $blogsSpanishName = array("First", "Second", "Third","Fourth", "Fifth");
        $blogsEnglishName = array("First", "Second", "Third","Fourth", "Fifth");
        $blogsArabicName = array("اول", "ثانى", "ثالث","رابع", "خامس");

        ////////////// blog 1
        $blog = Blog::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title'         => 'First blog',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.'
            ],
            'es' => [
                'title'         => 'First blog',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.'
            ],
            'ar' => [
                'title' => ' المقال الاول',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا'
            ],
        ]);

        $blog->seo()->create([
            'en' => [
                'meta_title'        => ' blog meta_title' ,
                'slug'              => Str::slug('First blog', '-'),
                'meta_keywords'     => ' blog meta_keywords',
                'meta_description'  => ' blog meta_description ',
            ],
            'es' => [
                'meta_title'        => ' blog meta_title' ,
                'slug'              => Str::slug('First blog', '-'),
                'meta_keywords'     => ' blog meta_keywords',
                'meta_description'  => ' blog meta_description ',
            ],
            'ar' => [
                'meta_title'        => ' المقال الاول' ,
                'slug'              => Str::slug('المقال الاول', '-'),
                'meta_keywords'     => ' المقال الاول',
                'meta_description'  => ' المقال الاول ',
            ]
        ]);


        for($i = 1 ; $i <= 5 ; $i++)
        {
            $blog->attachments()->create([
                'file'    => 'att1_'.$i.'.jpg',
                'is_main'   => ($i == 1) ? 1 : 0,
                'en' => [
                    'alts' => 'First blog ' .$blogsEnglishName[$i-1]. ' attachment ',
                    ],
                'en' => [
                    'alts' => 'First blog ' .$blogsSpanishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $blogsArabicName[$i-1].' صورة المقال الاول',
                    ],
            ]);
        }
        $blog->tags()->sync([1,2,3,4]);

        ///////////////// blog2
        $blog = Blog::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Second blog',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.'
            ],
            'es' => [
                'title' => 'Second blog',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.'
            ],
            'ar' => [
                'title' => ' المقال الثانى',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا'
            ],
        ]);

        $blog->seo()->create([
            'en' => [
                'meta_title'        => ' blog meta_title' ,
                'slug'              => Str::slug('Second blog', '-'),
                'meta_keywords'     => ' blog meta_keywords',
                'meta_description'  => ' blog meta_description ',
            ],
            'ar' => [
                'meta_title'        => ' المقال الثاني' ,
                'slug'              => Str::slug('المقال الثاني', '-'),
                'meta_keywords'     => ' المقال الثاني',
                'meta_description'  => ' المقال الثاني ',
            ]
        ]);

        for($i = 1 ;$i <=5 ; $i++)
        {
            $blog->attachments()->create([
                'file' => 'att2_'.$i.'.jpg',
                'is_main'   => ($i == 2) ? 1 : 0,
                'en' => [
                    'alts' => 'Second blog ' .$blogsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $blogsArabicName[$i-1].' صورة المقال الثانى',
                    ],
            ]);
        }
        $blog->tags()->sync([2,3,4,5]);

        ////////////////// blog3
        $blog = Blog::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Third blog',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.'
            ],
            'ar' => [
                'title' => ' المقال الثالث',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا'
            ],
        ]);
        $blog->seo()->create([
            'en' => [
                'meta_title'        => ' blog meta_title' ,
                'slug'              => Str::slug('Third blog', '-'),
                'meta_keywords'     => ' blog meta_keywords',
                'meta_description'  => ' blog meta_description ',
            ],
            'ar' => [
                'meta_title'        => ' المقال الثالث' ,
                'slug'              => Str::slug('المقال الثالث', '-'),
                'meta_keywords'     => ' المقال الثالث',
                'meta_description'  => ' المقال الثالث ',
            ]
        ]);
        for($i =1 ; $i <=5 ; $i++){
            $blog->attachments()->create([
                'file' => 'att3_'.$i.'.jpg',
                'is_main'   => ($i == 3) ? 1 : 0,
                'en' => [
                    'alts' => 'Third blog ' .$blogsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $blogsArabicName[$i-1].' صورة المقال الثالث',
                    ],
            ]);
        }
        $blog->tags()->sync([3,4,5,6]);

        /////////////// blog 4
        $blog = Blog::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Fourth blog',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.'
            ],
            'ar' => [
                'title' => ' المقال الرابع',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا'
            ],
        ]);

        $blog->seo()->create([
            'en' => [
                'meta_title'        => ' blog meta_title' ,
                'slug'              => Str::slug('Fourth blog', '-'),
                'meta_keywords'     => ' blog meta_keywords',
                'meta_description'  => ' blog meta_description ',
            ],
            'ar' => [
                'meta_title'        => ' المقال الرابع' ,
                'slug'              => Str::slug('المقال الرابع', '-'),
                'meta_keywords'     => ' المقال الرابع',
                'meta_description'  => ' المقال الرابع ',
            ]
        ]);

        for($i = 1 ;$i <=5 ; $i++)
        {
            $blog->attachments()->create([
                'file' => 'att4_'.$i.'.jpg',
                'is_main'   => ($i == 4) ? 1 : 0,
                'en' => [
                    'alts' => 'Fourth blog ' .$blogsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $blogsArabicName[$i-1].' صورة المقال الرابع',
                    ],
            ]);
        }
        $blog->tags()->sync([1,2,3,4]);

        //////////////// blog 5
        $blog = Blog::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Fifth blog',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.'
            ],
            'ar' => [
                'title' => ' المقال الخامس',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا'
            ],
        ]);

        $blog->seo()->create([
            'en' => [
                'meta_title'        => ' blog meta_title' ,
                'slug'              => Str::slug('Fifth blog', '-'),
                'meta_keywords'     => ' blog meta_keywords',
                'meta_description'  => ' blog meta_description ',
            ],
            'ar' => [
                'meta_title'        => ' المقال الخامس' ,
                'slug'              => Str::slug('المقال الخامس', '-'),
                'meta_keywords'     => ' المقال الخامس',
                'meta_description'  => ' المقال الخامس ',
            ]
        ]);
        for($i = 1 ;$i <=5 ; $i++)
        {
            $blog->attachments()->create([
                'file' => 'att5_'.$i.'.jpg',
                'is_main'   => ($i == 5) ? 1 : 0,
                'en' => [
                    'alts' => 'Fifth blog ' .$blogsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $blogsArabicName[$i-1].' صورة المقال الخامس',
                    ],
            ]);
        }
        $blog->tags()->sync([2,3,4,5]);

        /////////////////// blog 6
        $blog = Blog::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Sexth blog',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.'
            ],
            'ar' => [
                'title' => ' المقال السادس',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا'
            ],
        ]);
        $blog->seo()->create([
            'en' => [
                'meta_title'        => ' blog meta_title' ,
                'slug'              => Str::slug('Sexth blog', '-'),
                'meta_keywords'     => ' blog meta_keywords',
                'meta_description'  => ' blog meta_description ',
            ],
            'ar' => [
                'meta_title'        => ' المقال السادس' ,
                'slug'              => Str::slug('المقال السادس', '-'),
                'meta_keywords'     => ' المقال السادس',
                'meta_description'  => ' المقال السادس ',
            ]
        ]);
        for($i = 1 ;$i <=5 ; $i++)
        {
            $blog->attachments()->create([
                'file' => 'att6_'.$i.'.jpg',
                'is_main'   => ($i == 1) ? 1 : 0,
                'en' => [
                    'alts' => 'Sexth blog ' .$blogsEnglishName[$i-1]. ' attachment ',
                ],
                'ar' => [
                    'alts' => $blogsArabicName[$i-1].' صورة المقال السادس',
                ],
            ]);
        }
        $blog->tags()->sync([3,4,5,6]);

    }
}
