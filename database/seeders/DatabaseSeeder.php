<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call(LanguageSeeder::class);
        $this->call(AlbumSeeder::class);
        $this->call(BlogSeeder::class);
        $this->call(MainPageSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SocialMediaSeeder::class);
      //  $this->call(ClientSeeder::class);
        // $this->call(LanguageSeeder::class);
        $this->call(BlogSeeder::class);
        // $this->call(MainPageSeeder::class);
        $this->call(MainPageSeederAPI::class);
        $this->call(CitySeeder::class);
        $this->call(FacilitySeeder::class);
        $this->call(TourismTypeSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(PackagesSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(SocialMediaSeeder::class);
        $this->call(SliderSeeder::class);
        $this->call(VideoSeeder::class);
        $this->call(TeamMemberSeeder::class);
        $this->call(SectionSeeder::class);
        $this->call(ServiceSeeder::class);
        $this->call(BranchSeeder::class);
        $this->call(SettingSeeder::class);
    }
}
