<?php

namespace Database\Seeders;

use App\Models\Timeline;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
class TimelineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('timelines')->truncate();
        DB::table('timeline_translations')->truncate();
        Schema::enableForeignKeyConstraints();

        $timeline = new Timeline();  
        $timeline->create([
            'date' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [ 
                'title' => 'First Timeline title in english',
                'description' => 'First Timeline Description In English'
            ],
            'ar' => [
                'title' => 'عنوان التاريخ الاول بالعربية ',
                'description' => 'اول وصف قصير للتاريخ باللغة العربية'
            ],
        ]);
        $timeline->create([
            'date' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [ 
                'title' => 'Second Timeline title in english',
                'description' => 'Second Timeline Description In English'
            ],
            'ar' => [
                'title' => 'عنوان التاريخ الاول بالعربية ',
                'description' => 'ثاتى وصف قصير للتاريخ باللغة العربية'
            ],
        ]);
        $timeline->create([
            'date' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [ 
                'title' => 'Third Timeline title in english',
                'description' => 'Third Timeline Description In English'
            ],
            'ar' => [
                'title' => 'عنوان التاريخ الاول بالعربية ',
                'description' => 'ثالث وصف قصير للتاريخ باللغة العربية'
            ],
        ]);
        $timeline->create([
            'date' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [ 
                'title' => 'Fourth Timeline title in english',
                'description' => 'Fourth Timeline Description In English'
            ],
            'ar' => [
                'title' => 'عنوان التاريخ الاول بالعربية ',
                'description' => 'رابع وصف قصير للتاريخ باللغة العربية'
            ],
        ]);
        $timeline->create([
            'date' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [ 
                'title' => 'Fifth Timeline title in english',
                'description' => 'First Timeline Description In English'
            ],
            'ar' => [
                'title' => 'عنوان التاريخ الاول بالعربية ',
                'description' => 'خامس وصف قصير للتاريخ باللغة العربية'
            ],
        ]);
        $timeline->create([
            'date' => Carbon::now(),
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [ 
                'title' => 'Sexth Timeline title in english',
                'description' => 'Sexth Timeline Description In English'
            ],
            'ar' => [
                'title' => 'عنوان التاريخ الاول بالعربية ',
                'description' => 'سادس وصف قصير للتاريخ باللغة العربية'
            ],
        ]);
    }
}
