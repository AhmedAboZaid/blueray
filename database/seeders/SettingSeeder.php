<?php

namespace Database\Seeders;

use App\Models\Setting;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('settings')->truncate();
        DB::table('setting_translations')->truncate();
        Schema::enableForeignKeyConstraints();

        $setting = new Setting();

        $setting->create([
            'key' => 'title',
            'flag' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'EGIcons',
            ],
            'es' => [
                'value' => 'EGIcons',
            ],
            'ar' => [
                'value' => ' أى جى أيكونز',
            ],
        ]);

        $setting->create([
            'key' => 'alt',
            'flag' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'Image Alt',
            ],
            'es' => [
                'value' => 'Image Alt',
            ],
            'ar' => [
                'value' => 'عنوان للصورة',
            ],
        ]);

        $setting->create([
            'key' => 'meta_keywords',
            'flag' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'Setting Meta Keywords',
            ],
            'es' => [
                'value' => 'Setting Meta Keywords',
            ],
            'ar' => [
                'value' => 'الاعدادات',
            ],
        ]);

        $setting->create([
            'key' => 'meta_description',
            'flag' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'Setting Meta Description',
            ],
            'es' => [
                'value' => 'Setting Meta Description',
            ],
            'ar' => [
                'value' => 'الاعدادات',
            ],
        ]);

        $setting->create([
            'key' => 'meta_title',
            'flag' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'Setting Meta Title',
            ],
            'es' => [
                'value' => 'Setting Meta Title',
            ],
            'ar' => [
                'value' => 'الاعدادات',
            ],
        ]);
        $setting->create([
            'key' => 'from_day',
            'flag' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'sunday',
            ],
            'es' => [
                'value' => 'sunday',
            ],
            'ar' => [
                'value' => 'الاحد',
            ],
        ]);
        $setting->create([
            'key' => 'to_day',
            'flag' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'friday',
            ],
            'es' => [
                'value' => 'friday',
            ],
            'ar' => [
                'value' => 'الجمعة',
            ],
        ]);

        $setting->create([
            'key' => 'close_day',
            'flag' => '1',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'friday',
            ],
            'es' => [
                'value' => 'friday',
            ],
            'ar' => [
                'value' => 'الجمعة',
            ],
        ]);

        $setting->create([
            'key' => 'logo',
            'flag' => '2',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'logo_en.png',
            ],
            'es' => [
                'value' => 'logo_en.png',
            ],
            'ar' => [
                'value' => 'logo_ar.png',
            ],
        ]);


        $setting->create([
            'key' => 'map',
            'flag' => '3',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => 'https://www.google.com/maps?ll=44.947086,-93.339608&z=15&t=m&hl=en-GB&gl=US&mapclient=embed&cid=13983112962245836178',
            ],
        ]);

        $setting->create([
            'key' => 'from_hour',
            'flag' => '3',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => '10:00Am',
            ],
        ]);
        $setting->create([
            'key' => 'to_hour',
            'flag' => '3',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'value' => '10:00Pm',
            ],

        ]);
    }
}
