<?php

namespace Database\Seeders;

use App\Models\TourismType;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class TourismTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('seos')->where('seoable_type','App\Models\TourismType')->delete();
        DB::table('attachment_translations')->whereIn('attachment_id', DB::table('attachments')->where('attachmentable_type','App\Models\TourismType')->pluck('id')->toArray())->delete();
        DB::table('attachments')->where('attachmentable_type','App\Models\TourismType')->delete();
        DB::table('tourism_type_translations')->truncate();
        DB::table('tourism_types')->truncate();
        Schema::enableForeignKeyConstraints();

        $tourismTypeEnglishName = array("Religious Tourism", "Familt Tourism", "Entertainment Tourism");
        $tourismTypeArabicName = array("السياحة الدينية" , "السياحة العائلية" , "السياحة الترفيهية");


        $tourismTypeMainImage = array("tourismType1.jpg", "tourismType2.jpg", "tourismType3.jpg","tourismType4.jpg", "tourismType5.jpg", "tourismType6.jpg");

        for($j = 0; $j < 3 ; $j++){
            $tourismType = TourismType::create([
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'en' => [
                    'title' => $tourismTypeEnglishName[$j] . ' Tourism Type',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'es' => [
                    'title' => $tourismTypeEnglishName[$j] . ' Tourism Type',
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'ar' => [
                    'title' => $tourismTypeArabicName[$j]. ' رحلة',
                    'description' => 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.'
                ],
            ]);

            $tourismType->seo()->create([
                'en' => [
                    'slug' => Str::slug($tourismTypeEnglishName[$j], '-'),
                    'meta_title' => $tourismTypeEnglishName[$j].' meta_title' ,
                    'meta_keywords' => $tourismTypeEnglishName[$j].' meta_keywords',
                    'meta_description' => $tourismTypeEnglishName[$j].' meta_description ',
                    ],
                'es' => [
                    'slug' => Str::slug($tourismTypeEnglishName[$j], '-'),
                    'meta_title' => $tourismTypeEnglishName[$j].' meta_title' ,
                    'meta_keywords' => $tourismTypeEnglishName[$j].' meta_keywords',
                    'meta_description' => $tourismTypeEnglishName[$j].' meta_description ',
                    ],
                'ar' => [
                    'slug' => Str::slug($tourismTypeArabicName[$j], '-'),
                    'meta_title' => $tourismTypeArabicName[$j] ,
                    'meta_keywords' => $tourismTypeArabicName[$j],
                    'meta_description' => $tourismTypeArabicName[$j],
                    ],
            ]);

            for($i = 1 ;$i <=3 ; $i++){
                $tourismType->attachments()->create([
                    'is_main' =>($i == 1) ? 1 : 0,
                    'file' => ($i == 1 ) ?   $tourismTypeMainImage[$j] : 'att1_'.$i.'.jpg',
                    'en' => [
                        'alts' =>'tourismType ' .$tourismTypeEnglishName[$i-1]. ' attachment ',
                        ],
                    'es' => [
                        'alts' =>'tourismType ' .$tourismTypeEnglishName[$i-1]. ' attachment ',
                        ],
                    'ar' => [
                        'alts' => $tourismTypeArabicName[$i-1].' صورة الرحلة ' ,
                        ],
                ]);
            }
            $tags=[1,2,3,4,5,6];
            $tourismType->tags()->sync(array_rand($tags,3));
        }
    }
}
