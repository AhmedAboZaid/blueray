<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class SocialMediaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('social_media')->truncate();
        Schema::enableForeignKeyConstraints();

        DB::table('social_media')->insert(array (
            0 =>
            array (
                'name' => 'Facebook',
                'url' => 'https://www.facebook.com/blueRay',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
            1 =>
            array (
                'name' => 'Twitter',
                'url' => 'https://twitter.com/blueRay',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
            2 =>
            array (
                'name' => 'YouTube',
                'url' => 'https://www.youtube.com/',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
            3 =>
            array (
                'name' => 'LinkedIn',
                'url' => 'https://www.linkedin.com/',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ),
        ));
    }
}
