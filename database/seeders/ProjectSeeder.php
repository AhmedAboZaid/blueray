<?php

namespace Database\Seeders;

use App\Models\Project;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('seos')->where('seoable_type','App\Models\Project')->delete();
        DB::table('project_translations')->truncate();
        DB::table('attachment_translations')->whereIn('attachment_id', DB::table('attachments')->where('attachmentable_type','App\Models\Project')->pluck('id')->toArray())->delete();
        DB::table('attachments')->where('attachmentable_type','App\Models\Project')->delete();
        DB::table('projects')->truncate();
        Schema::enableForeignKeyConstraints();

        $projectsEnglishName = array("First", "Second", "Third","Fourth", "Fifth","Sexth","Seventh");
        $projectsArabicName = array("اول", "ثانى", "ثالث","رابع", "خامس", "سادس", "سابع");


            $project = Project::create([
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'en' => [
                    'title' => 'First project',
                    'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
                ],
                'ar' => [
                    'title' => ' المشروع الاول',
                    'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا',
                ],
            ]);

            $project->seo()->create([
                'en' => [
                    'meta_title' => ' project meta_title' ,
                    'slug' => Str::slug('First project', '-'),
                    'meta_keywords' => ' project meta_keywords',
                    'meta_description' => ' project meta_description ',
                ],
                'ar' => [
                    'meta_title' => ' المشروع الاول' ,
                    'slug' => Str::slug('المشروع الاول', '-'),
                    'meta_keywords' => ' المشروع الاول',
                    'meta_description' => ' المشروع الاول ',
                ]
            ]);

        for($i = 1 ;$i <=7 ; $i++){
            $project->attachments()->create([
                'file' => 'att1_'.$i.'.jpg',
                'is_main' => ($i == 1) ? 1 : 0,
                'en' => [
                    'alts' => 'First project ' .$projectsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $projectsArabicName[$i-1].' صورة المشروع الاول',
                    ],
            ]);
        }
        $project->tags()->sync([1,2,3,4]);

       $project = Project::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Second project',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
            ],
            'ar' => [
                'title' => ' المشروع الثانى',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا',
            ],
        ]);
        $project->seo()->create([
            'en' => [
                'meta_title' => ' project meta_title' ,
                'slug' => Str::slug('Second project', '-'),
                'meta_keywords' => ' project meta_keywords',
                'meta_description' => ' project meta_description ',
            ],
            'ar' => [
                'meta_title' => ' المشروع الثاني' ,
                'slug' => Str::slug('المشروع الثاني', '-'),
                'meta_keywords' => ' المشروع الثاني',
                'meta_description' => ' المشروع الثاني ',
            ]
        ]);


        for($i = 1 ;$i <=7 ; $i++){
            $project->attachments()->create([
                'file' => 'att2_'.$i.'.jpg',
                'is_main' => ($i == 1) ? 1 : 0,
                'en' => [
                    'alts' => 'Second project ' .$projectsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $projectsArabicName[$i-1].' صورة المشروع الثانى',
                    ],
            ]);
        }
        $project->tags()->sync([2,3,4,5]);

        //////////////////////
       $project = Project::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Third project',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
            ],
            'ar' => [
                'title' => ' المشروع الثالث',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا',
            ],
        ]);
        $project->seo()->create([
            'en' => [
                'meta_title' => ' project meta_title' ,
                'slug' => Str::slug('Third project', '-'),
                'meta_keywords' => ' project meta_keywords',
                'meta_description' => ' project meta_description ',
            ],
            'ar' => [
                'meta_title' => ' المشروع الثالث' ,
                'slug' => Str::slug('المشروع الثالث', '-'),
                'meta_keywords' => ' المشروع الثالث',
                'meta_description' => ' المشروع الثالث ',
            ]
        ]);

        for($i =1 ; $i <=7 ; $i++){
            $project->attachments()->create([
                'file' => 'att3_'.$i.'.jpg',
                'is_main' => ($i == 1) ? 1 : 0,
                'en' => [
                    'alts' => 'Third project ' .$projectsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $projectsArabicName[$i-1].' صورة المشروع الثالث',
                    ],
            ]);
        }
        $project->tags()->sync([3,4,5,6]);

        /////////////////
       $project = Project::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Fourth project',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
            ],
            'ar' => [
                'title' => ' المشروع الرابع',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا',
            ],
        ]);

        $project->seo()->create([
            'en' => [
                'meta_title' => ' project meta_title' ,
                'slug' => Str::slug('Third project', '-'),
                'meta_keywords' => ' project meta_keywords',
                'meta_description' => ' project meta_description ',
            ],
            'ar' => [
                'meta_title' => ' المشروع الثالث' ,
                'slug' => Str::slug('المشروع الثالث', '-'),
                'meta_keywords' => ' المشروع الثالث',
                'meta_description' => ' المشروع الثالث ',
            ]
        ]);


        for($i = 1 ;$i <=7 ; $i++){
            $project->attachments()->create([
                'file' => 'att4_'.$i.'.jpg',
                'is_main' => ($i == 1) ? 1 : 0,
                'en' => [
                    'alts' => 'Fourth project ' .$projectsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $projectsArabicName[$i-1].' صورة المشروع الرابع',
                    ],
            ]);
        }
        $project->tags()->sync([1,2,3,4]);

        /////////////////////////
       $project = Project::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Fifth project',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
            ],
            'ar' => [
                'title' => ' المشروع الخامس',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا',
            ],
        ]);

        $project->seo()->create([
            'en' => [
                'meta_title' => ' project meta_title' ,
                'slug' => Str::slug('Fifth project', '-'),
                'meta_keywords' => ' project meta_keywords',
                'meta_description' => ' project meta_description ',
            ],
            'ar' => [
                'meta_title' => ' المشروع الخامس' ,
                'slug' => Str::slug('المشروع الخامس', '-'),
                'meta_keywords' => ' المشروع الخامس',
                'meta_description' => ' المشروع الخامس ',
            ]
        ]);


        for($i = 1 ;$i <=7 ; $i++){
            $project->attachments()->create([
                'file' => 'att5_'.$i.'.jpg',
                'is_main' => ($i == 1) ? 1 : 0,
                'en' => [
                    'alts' => 'Fifth project ' .$projectsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $projectsArabicName[$i-1].' صورة المشروع الخامس',
                    ],
            ]);
        }
        $project->tags()->sync([2,3,4,5]);

        //////////////////////////////
       $project = Project::create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Sexth project',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.',
            ],
            'ar' => [
                'title' => ' المشروع السادس',
                'description' => 'أم تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا',
            ],
        ]);

        $project->seo()->create([
            'en' => [
                'meta_title' => ' project meta_title' ,
                'slug' => Str::slug('Sexth project', '-'),
                'meta_keywords' => ' project meta_keywords',
                'meta_description' => ' project meta_description ',
            ],
            'ar' => [
                'meta_title' => ' المشروع السادس' ,
                'slug' => Str::slug('المشروع السادس', '-'),
                'meta_keywords' => ' المشروع السادس',
                'meta_description' => ' المشروع السادس ',
            ]
        ]);

        for($i = 1 ;$i <=7 ; $i++){
            $project->attachments()->create([
                'file' => 'att6_'.$i.'.jpg',
                'is_main' => ($i == 1) ? 1 : 0,
                'en' => [
                    'alts' => 'Sexth project ' .$projectsEnglishName[$i-1]. ' attachment ',
                    ],
                'ar' => [
                    'alts' => $projectsArabicName[$i-1].' صورة المشروع السادس',
                    ],
            ]);
        }
        $project->tags()->sync([3,4,5,6]);

    }

}
