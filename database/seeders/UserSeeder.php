<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Schema;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('users')->truncate();
        Schema::enableForeignKeyConstraints();

        DB::table('users')->insert(array (
            0 =>
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'ahmed.abozaid@egicons.com',
                'email_verified_at' => NULL,
                'password' => '$2y$12$lE/YUE.CVAACmFEoPZCWMuPxkmvtfeA6oBg0jdUp9lHlb5ZnnV3s.',
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
           1 =>
            array (
                'id' => 2,
                'name' => 'Admin',
                'email' => 'dev@ileadco.com',
                'email_verified_at' => NULL,
                'password' =>Hash::make('123456'),
                'remember_token' => NULL,
                'created_at' => NULL,
                'updated_at' => NULL,
            ),
        ));


    }
}
