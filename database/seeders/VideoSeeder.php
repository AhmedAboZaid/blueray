<?php

namespace Database\Seeders;

use App\Models\Video;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class VideoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('video_translations')->truncate();
        DB::table('videos')->truncate();
        Schema::enableForeignKeyConstraints();

        //Video 1
        $video = Video::create([
            'video_link' => 'https://www.youtube.com/watch?v=S2P_bRKyQnc',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'First Video',
                'alt' => 'First Video ',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.</p>'
            ],
            'es' => [
                'title' => 'First Video',
                'alt' => 'First Video ',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.</p>'
            ],
            'ar' => [
                'title' => 'الفيديو الاول',
                'alt' => 'الفيديو الاول',
                'description' => 'م تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا، الفيديو الاول '
            ],
        ]);
        $video->seo()->create([
            'en' => [
                'meta_title' => ' Video meta_title' ,
                'slug' => Str::slug('First Video', '-'),
                'meta_keywords' => ' Video meta_keywords',
                'meta_description' => ' Video meta_description ',
            ],
            'es' => [
                'meta_title' => ' Video meta_title' ,
                'slug' => Str::slug('First Video', '-'),
                'meta_keywords' => ' Video meta_keywords',
                'meta_description' => ' Video meta_description ',
            ],
            'ar' => [
                'meta_title' => ' الفيديو الاول' ,
                'slug' => Str::slug('الفيديو الاول', '-'),
                'meta_keywords' => ' الفيديو الاول',
                'meta_description' => ' الفيديو الاول ',
            ]
        ]);
         //Video 2
         $video = Video::create([
            'video_link' => 'https://www.youtube.com/watch?v=6ndIkR5dL64',
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Second Video',
                'alt' => 'Second Video ',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.</p>'
            ],
            'es' => [
                'title' => 'Second Video',
                'alt' => 'Second Video ',
                'description' => 'Varius dis proin luctus dictumst gravida ante, ornare inceptos ridiculus. Eros porttitor gravida, sapien nunc cum morbi lacus fringilla dolor. At platea non nam turpis ac turpis faucibus mauris class consequat egestas. Integer, viverra consectetur netus! Justo pulvinar blandit vel interdum phasellus dui montes lectus vitae luctus tincidunt pellentesque. Dictum per sodales ullamcorper feugiat.</p>'
            ],
            'ar' => [
                'title' => 'الفيديو الثانى',
                'alt' => 'الفيديو الثانى',
                'description' => 'م تاريخ يتمكن الضغوط حين, جمعت نهاية الإيطالية جهة عل. الإنزال مليارات وباستثناء هو فعل, كرسي أهّل عل لمّ, يذكر بأيدي استمرار و مكن. لها أن تصفح معارضة استرجاع. مقاومة المواد ان الا. بين خيار العالم تم, تعديل العصبة بـ أخذ, ولم بداية لبلجيكا، الفيديو الثانى '
            ],
        ]);
        $video->seo()->create([
            'en' => [
                'meta_title' => ' Video meta_title' ,
                'slug' => Str::slug('Second Video', '-'),
                'meta_keywords' => ' Video meta_keywords',
                'meta_description' => ' Video meta_description ',
            ],
            'es' => [
                'meta_title' => ' Video meta_title' ,
                'slug' => Str::slug('Second Video', '-'),
                'meta_keywords' => ' Video meta_keywords',
                'meta_description' => ' Video meta_description ',
            ],
            'ar' => [
                'meta_title' => ' الفيديو الثانى' ,
                'slug' => Str::slug('الفيديو الثانى', '-'),
                'meta_keywords' => ' الفيديو الثانى',
                'meta_description' => ' الفيديو الثانى ',
            ]
        ]);
      
    }
}
