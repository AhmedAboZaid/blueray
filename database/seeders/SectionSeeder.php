<?php

namespace Database\Seeders;

use App\Models\MainPage;
use App\Models\Mainsection;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        Schema::enableForeignKeyConstraints();



        $sectionNameEnglish = ['section 1', 'section 2', 'section 3', 'section 4', 'section 5', 'section 6'];
        $sectionNameArabic = ['القسم السادس', 'القسم الخامس', 'القسم الرابع' ,'القسم الثالت', 'القسم الثانى', 'القسم الاول '];

        $sectionImageEnglish = ['section1_en.jpg', 'section2_en.jpg', 'section3_en.jpg', 'section4_en.jpg', 'section5_en.jpg', 'section6_en.jpg' ];
        $sectionImageArabic = ['section1_ar.jpg', 'section2_ar.jpg', 'section3_ar.jpg', 'section4_ar.jpg', 'section5_ar.jpg', 'section6_ar.jpg' ];


        for($i = 0 ; $i< count($sectionNameEnglish); $i++){
            $section = MainPage::create([
                'parent_id' => 1,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'en' => [
                    'title' => $sectionNameEnglish[$i],
                    'image' => $sectionImageEnglish[$i],
                    'alt' => $sectionNameEnglish[$i],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a section when looking at its layout'
                ],
                'es' => [
                    'title' => $sectionNameEnglish[$i],
                    'image' => $sectionImageEnglish[$i],
                    'alt' => $sectionNameEnglish[$i],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a section when looking at its layout'
                ],
                'ar' => [
                    'title' => $sectionNameArabic[$i],
                    'image' => $sectionImageArabic[$i],
                    'alt' => $sectionNameArabic[$i],
                    'description' => 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.'
                ],
            ]);
            $section->seo()->create([
                'en' => [
                    'slug' => Str::slug($sectionNameEnglish[$i], '-'),
                    'meta_title' => $sectionNameEnglish[$i].' meta_title' ,
                    'meta_keywords' => $sectionNameEnglish[$i].' meta_keywords',
                    'meta_description' => $sectionNameEnglish[$i].' meta_description ',
                    ],
                'es' => [
                    'slug' => Str::slug($sectionNameEnglish[$i], '-'),
                    'meta_title' => $sectionNameEnglish[$i].' meta_title' ,
                    'meta_keywords' => $sectionNameEnglish[$i].' meta_keywords',
                    'meta_description' => $sectionNameEnglish[$i].' meta_description ',
                    ],
                'ar' => [
                    'slug' => Str::slug($sectionNameArabic[$i], '-'),
                    'meta_title' => $sectionNameArabic[$i] ,
                    'meta_keywords' => $sectionNameArabic[$i],
                    'meta_description' => $sectionNameArabic[$i],
                    ],
            ]);
        }
    }
}
