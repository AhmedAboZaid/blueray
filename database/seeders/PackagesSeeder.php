<?php

namespace Database\Seeders;

use App\Models\City;
use App\Models\Facility;
use App\Models\Language;
use App\Models\Tag;
use App\Models\TourismType;
use App\Models\Package;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Illuminate\Support\Arr;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('seos')->where('seoable_type','App\Models\Package')->delete();
        DB::table('attachment_translations')->whereIn('attachment_id', DB::table('attachments')->where('attachmentable_type','App\Models\Package')->pluck('id')->toArray())->delete();
        DB::table('taggables')->where('taggable_type','App\Models\Package')->delete();
        DB::table('package_facility_exclude')->truncate();
        DB::table('package_facility_include')->truncate();
        DB::table('attachments')->where('attachmentable_type','App\Models\Package')->delete();
        DB::table('package_translations')->truncate();
        DB::table('package_languages')->truncate();
        DB::table('packages')->truncate();
        Schema::enableForeignKeyConstraints();

        $tripEnglishName = ["First", "Second", "Third","Fourth", "Fifth"];
        $tripArabicName  = ["اول", "ثانى", "ثالث","رابع", "خامس"];

        $cities         = City::pluck('id')->toArray();
        $tags           = Tag::pluck('id')->toArray();
        $languages      = Language::pluck('id')->toArray();
        $tourism_types  = TourismType::pluck('id')->toArray();
        $facilities     = Facility::pluck('id')->toArray();
        $facilities_ids     = Arr::random($facilities,3);
        $facilities_ids2    = Arr::random($facilities,3);
        for($j = 0; $j <5 ; $j++)
        {
            $trip = Package::create([
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
                'price'             => 180,
                'nights'            => 6,
                'group_size'        => 30,
                'from_date'         => Carbon::now(),
                'to_date'           => Carbon::now()->addDays(rand(1,5)),
                'tourism_type_id'   => Arr::random($tourism_types , 1 )[0],
                'city_id'           => Arr::random($cities , 1 )[0],
                'en' => [
                    'title' => $tripEnglishName[$j] . ' Trip',
                    'description'   => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout',
                    'travel_plan_description'   => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout',
                ],
                'es' => [
                    'title' => $tripEnglishName[$j] . ' Trip',
                    'description'   => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout',
                    'travel_plan_description'   => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout',
                ],
                'ar' => [
                    'title' => $tripArabicName[$j]. ' رحلة',
                    'description' => 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.',
                    'travel_plan_description' => 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.',
                ],
            ]);

            $trip->seo()->create([
                'en' => [
                    'slug' => Str::slug($tripEnglishName[$j], '-'),
                    'meta_title' => $tripEnglishName[$j].' meta_title' ,
                    'meta_keywords' => $tripEnglishName[$j].' meta_keywords',
                    'meta_description' => $tripEnglishName[$j].' meta_description ',
                    ],
                'es' => [
                    'slug' => Str::slug($tripEnglishName[$j], '-'),
                    'meta_title' => $tripEnglishName[$j].' meta_title' ,
                    'meta_keywords' => $tripEnglishName[$j].' meta_keywords',
                    'meta_description' => $tripEnglishName[$j].' meta_description ',
                    ],
                'ar' => [
                    'slug' => Str::slug($tripArabicName[$j], '-'),
                    'meta_title' => $tripArabicName[$j] ,
                    'meta_keywords' => $tripArabicName[$j],
                    'meta_description' => $tripArabicName[$j],
                    ],
            ]);

            $trip->TravelPlans()->createMany(
                [
                    [
                        'from'  => "1",
                        'from'  => "10:00",
                        'to'    =>  "22:00",
                        'en' => [
                            'title'         => "DAY 1 : Departure And Small Tour",
                            'description'   => "Pellentesque accumsan magna in augue sagittis, non fringilla eros molestie. Sed feugiat mi nec ex vehicula, nec vestibulum orci semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec tristique commodo fringilla.",
                        ],
                        'es' => [
                            'title'         => "DAY 1 : Departure And Small Tour",
                            'description'   => "Pellentesque accumsan magna in augue sagittis, non fringilla eros molestie. Sed feugiat mi nec ex vehicula, nec vestibulum orci semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec tristique commodo fringilla.",
                        ],
                        'ar' => [
                            'title'         => "DAY 1 : Departure And Small Tour",
                            'description'   => "Pellentesque accumsan magna in augue sagittis, non fringilla eros molestie. Sed feugiat mi nec ex vehicula, nec vestibulum orci semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec tristique commodo fringilla.",
                        ],
                    ],
                    [
                        'from'  => "2",
                        'from'  => "10:00",
                        'to'    =>  "22:00",
                        'en' => [
                            'title'         => "DAY 1 : Departure And Small Tour",
                            'description'   => "Pellentesque accumsan magna in augue sagittis, non fringilla eros molestie. Sed feugiat mi nec ex vehicula, nec vestibulum orci semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec tristique commodo fringilla.",
                        ],
                        'es' => [
                            'title'         => "DAY 1 : Departure And Small Tour",
                            'description'   => "Pellentesque accumsan magna in augue sagittis, non fringilla eros molestie. Sed feugiat mi nec ex vehicula, nec vestibulum orci semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec tristique commodo fringilla.",
                        ],
                        'ar' => [
                            'title'         => "DAY 1 : Departure And Small Tour",
                            'description'   => "Pellentesque accumsan magna in augue sagittis, non fringilla eros molestie. Sed feugiat mi nec ex vehicula, nec vestibulum orci semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec tristique commodo fringilla.",
                        ],
                    ],
                    [
                        'from'  => "3",
                        'from'  => "10:00",
                        'to'    =>  "22:00",
                        'en' => [
                            'title'         => "DAY 1 : Departure And Small Tour",
                            'description'   => "Pellentesque accumsan magna in augue sagittis, non fringilla eros molestie. Sed feugiat mi nec ex vehicula, nec vestibulum orci semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec tristique commodo fringilla.",
                        ],
                        'es' => [
                            'title'         => "DAY 1 : Departure And Small Tour",
                            'description'   => "Pellentesque accumsan magna in augue sagittis, non fringilla eros molestie. Sed feugiat mi nec ex vehicula, nec vestibulum orci semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec tristique commodo fringilla.",
                        ],
                        'ar' => [
                            'title'         => "DAY 1 : Departure And Small Tour",
                            'description'   => "Pellentesque accumsan magna in augue sagittis, non fringilla eros molestie. Sed feugiat mi nec ex vehicula, nec vestibulum orci semper. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Donec tristique commodo fringilla.",
                        ],
                    ],
                ],
            );

            for($i = 1 ;$i <=3 ; $i++)
            {
                $trip->attachments()->create([
                    'is_main' =>($i == 1) ? 1 : 0,
                    'file' => ($i == 1 ) ?   'att1_'.($j+1).'.jpg' : 'att1_'.$i.'.jpg',
                    'en' => [
                        'alts' =>'trip ' .$tripEnglishName[$i-1]. ' attachment ',
                        ],
                    'es' => [
                        'alts' =>'trip ' .$tripEnglishName[$i-1]. ' attachment ',
                        ],
                    'ar' => [
                        'alts' => $tripArabicName[$i-1].' صورة الرحلة ' ,
                        ],
                ]);
            }

            $trip->tags()->sync(Arr::random($tags,3));

            $trip->languages()->sync(Arr::random($languages,2));

            $trip->facilitiesInclude()->sync($facilities_ids);
            $trip->facilitiesExclude()->sync($facilities_ids2);
        }
    }
}
