<?php

namespace Database\Seeders;

use App\Models\Facility;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class FacilitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('facilities')->truncate();
        DB::table('facility_translations')->truncate();
        Schema::enableForeignKeyConstraints();

        $facilityEnglishName = ['Swimming Pool', 'Room Service', 'food', 'bike' , 'fishing', 'shopping', 'new facility'];
        $facilitySpanishhName = ['Swimming Pool', 'Room Service', 'food', 'bike' , 'fishing', 'shopping', 'new facility'];
        $facilityArabicName = ['سباحة', 'خدمات', 'طعام', 'عجل' , 'صيد', 'تسوق', 'تسهيلات'];

        for($i = 0 ; $i< count($facilityEnglishName); $i++)
        {
            Facility::create([
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'en' => [
                    'name' => $facilityEnglishName[$i],
                ],
                'es' => [
                    'name' => $facilitySpanishhName[$i],
                ],
                'ar' => [
                    'name' => $facilityArabicName[$i],
                ],
            ]);
        }
    }

}
