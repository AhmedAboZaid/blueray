<?php

namespace Database\Seeders;

use App\Models\Branch;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('branches')->truncate();
        DB::table('branch_translations')->truncate();
        Schema::enableForeignKeyConstraints();

            Branch::create([
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'phone'=>'01234456789',
                'fax'=>'0123645',
                'email'=>'info@blueray.com',
                'facebook'=>'https://www.facebook.com/',
                'instagram'=>'https://www.instagram.com/',
                'whatsapp'=>'https://web.whatsapp.com/',
                'twitter'=>'https://twitter.com/',
                'en' => [
                    'name' => 'Kwuite',
                    'address' => 'Kwuite/kwuite',
                ],
                'es' => [
                    'name' => 'Kwuite',
                    'address' => 'Kwuite/kwuite',
                ],
                'ar' => [
                    'name' => 'الكويت',
                    'address' => 'الكويت/الكويت',
                ],
            ]);

            Branch::create([
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'phone'=>'01234456789',
                'fax'=>'0123645',
                'email'=>'info@blueray.com',
                'facebook'=>'https://www.facebook.com/',
                'instagram'=>'https://www.instagram.com/',
                'whatsapp'=>'https://web.whatsapp.com/',
                'twitter'=>'https://twitter.com/',
                'en' => [
                    'name' => 'Egypt',
                    'address' => 'Egypt/cairo',
                ],
                'es' => [
                    'name' => 'Egypt',
                    'address' => 'Egypt/cairo',
                ],
                'ar' => [
                    'name' => 'مصر',
                    'address' => 'مصر/القاهرة',
                ],
            ]);

    }
}
