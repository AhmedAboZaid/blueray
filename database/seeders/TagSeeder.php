<?php

namespace Database\Seeders;

use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('tags')->truncate();
        DB::table('tag_translations')->truncate();
        Schema::enableForeignKeyConstraints();

        $tag = new Tag();

        $tag->create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Web Development',
            ],
            'es' => [
                'title' => 'Web Development',
            ],
            'ar' => [
                'title' => ' تطوير المواقع',
            ],
        ]);
        $tag->create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Web Design',
            ],
            'es' => [
                'title' => 'Web Design',
            ],
            'ar' => [
                'title' => ' تصميم المواقع ',
            ],
        ]);
        $tag->create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'UI/UX',
            ],
            'es' => [
                'title' => 'UI/UX',
            ],
            'ar' => [
                'title' => ' تطوير الوجهات ',
            ],
        ]);
        $tag->create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Advertisement',
            ],
            'es' => [
                'title' => 'Advertisement',
            ],
            'ar' => [
                'title' => ' الإعلانات ',
            ],
        ]);
        $tag->create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'Marketing',
            ],
            'es' => [
                'title' => 'Marketing',
            ],
            'ar' => [
                'title' => '  تسويق ',
            ],
        ]);
        $tag->create([
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'en' => [
                'title' => 'ًSales',
            ],
            'es' => [
                'title' => 'ًSales',
            ],
            'ar' => [
                'title' => '  مبيعات ',
            ],
        ]);
    }
}
