<?php

namespace Database\Seeders;

use App\Models\MainPage;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class MainPageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Schema::disableForeignKeyConstraints();
        DB::table('seos')->where('seoable_type','App\Models\MainPage')->delete();
        DB::table('main_pages')->truncate();
        DB::table('main_page_translations')->truncate();
        Schema::enableForeignKeyConstraints();

        // $mainPages = new MainPage();
        $pageNameEnglish = ['About', 'Media Center', 'Blogs', 'Albums', 'Videos', 'Services' ,'Ticketing','Transportation','Our Projects', 'Solutions', 'Clients',
                            'FAQS', 'Contact Us', 'Privacy And Policies'];
        $pageSpanishEnglish = ['About', 'Media Center', 'Blogs', 'Albums', 'Videos', 'Services' ,'Ticketing','Transportation','Our Projects', 'Solutions', 'Clients',
                            'FAQS', 'Contact Us', 'Privacy And Policies'];
        $multimedia=['Blogs', 'Albums', 'Videos'];
        $services=['Ticketing', 'Transportation'];
        $pageNameArabic = ['من نحن', 'مركز الوسائط', 'المقالات', 'البومات', 'الفيديوهات', 'الخدمات' ,'الحجوزات','التنقلات','المشاريع', 'الحلول', 'العملاء',
                             'الأسئلة الشائعة', 'تواصل معنا', 'سياستنا والخصوصية'];
        $pageImageEnglish = ['1640089701_en.png', 'mediacenter_en.png', 'blog_en.jpg', 'albums_en.png', 'video_en.jpg', 'services_en.png','services_ar.png','services_ar.png' ,'project_en.png',
                            'solutions_en.png', 'clients_ar.png','faqs_en.png', 'contact_en.png', 'policies_en.png'];
        $pageImageArabic = ['1640089701_ar.png', 'media_center_ar.jpg', 'blog_ar.png', 'albums_ar.jpg', 'video_ar.jpg', 'services_ar.png','services_ar.png','services_ar.png' ,'project_ar.png',
                            'solutions_ar.png', 'clients_ar.png','faqs_ar.png', 'contact_ar.png', 'policies_ar.png'];

        for($i = 0 ; $i< count($pageNameEnglish); $i++){
            if(in_array($pageNameEnglish[$i],$multimedia)){
            $mainPage = MainPage::create([
                'fixed_name' => $pageNameEnglish[$i],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'parent_id' => 2,
                'en' => [
                    'title' => $pageNameEnglish[$i],
                    'image' => $pageImageEnglish[$i],
                    'alt' => $pageNameEnglish[$i],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'es' => [
                    'title' => $pageNameEnglish[$i],
                    'image' => $pageImageEnglish[$i],
                    'alt' => $pageNameEnglish[$i],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'ar' => [
                    'title' => $pageNameArabic[$i],
                    'image' => $pageImageArabic[$i],
                    'alt' => $pageNameArabic[$i],
                    'description' => 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.'
                ],
            ]);
        }elseif(in_array($pageNameEnglish[$i],$services)){
            $mainPage = MainPage::create([
                'fixed_name' => $pageNameEnglish[$i],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'parent_id' => 6,
                'en' => [
                    'title' => $pageNameEnglish[$i],
                    'image' => $pageImageEnglish[$i],
                    'alt' => $pageNameEnglish[$i],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'es' => [
                    'title' => $pageNameEnglish[$i],
                    'image' => $pageImageEnglish[$i],
                    'alt' => $pageNameEnglish[$i],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'ar' => [
                    'title' => $pageNameArabic[$i],
                    'image' => $pageImageArabic[$i],
                    'alt' => $pageNameArabic[$i],
                    'description' => 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.'
                ],
            ]);
        }else{
            $mainPage = MainPage::create([
                'fixed_name' => $pageNameEnglish[$i],
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'parent_id' => null,
                'en' => [
                    'title' => $pageNameEnglish[$i],
                    'image' => $pageImageEnglish[$i],
                    'alt' => $pageNameEnglish[$i],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'es' => [
                    'title' => $pageNameEnglish[$i],
                    'image' => $pageImageEnglish[$i],
                    'alt' => $pageNameEnglish[$i],
                    'description' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout'
                ],
                'ar' => [
                    'title' => $pageNameArabic[$i],
                    'image' => $pageImageArabic[$i],
                    'alt' => $pageNameArabic[$i],
                    'description' => 'حقيقة مثبتة منذ زمن طويل وهي أن المحتوى المقروء لصفحة ما سيلهي القارئ عن التركيز على الشكل الخارجي للنص أو شكل توضع الفقرات في الصفحة التي يقرأها.'
                ],
            ]);
        }
            $mainPage->seo()->create([
                'en' => [
                    'slug' => Str::slug($pageNameEnglish[$i], '-'),
                    'meta_title' => $pageNameEnglish[$i].' meta_title' ,
                    'meta_keywords' => $pageNameEnglish[$i].' meta_keywords',
                    'meta_description' => $pageNameEnglish[$i].' meta_description ',
                    ],
                'es' => [
                    'slug' => Str::slug($pageNameEnglish[$i], '-'),
                    'meta_title' => $pageNameEnglish[$i].' meta_title' ,
                    'meta_keywords' => $pageNameEnglish[$i].' meta_keywords',
                    'meta_description' => $pageNameEnglish[$i].' meta_description ',
                    ],
                'ar' => [
                    'slug' => Str::slug($pageNameArabic[$i], '-'),
                    'meta_title' => $pageNameArabic[$i] ,
                    'meta_keywords' => $pageNameArabic[$i],
                    'meta_description' => $pageNameArabic[$i],
                    ],
            ]);
        }

    }
}
