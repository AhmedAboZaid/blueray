<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('packages', function (Blueprint $table) {
            $table->dropColumn('from_date');
            $table->dropColumn('to_date');
            $table->dropColumn('price');
            $table->dropColumn('group_size');
        });

        Schema::table('travel_plans', function (Blueprint $table) {
            $table->dropColumn('from');
            $table->dropColumn('to');
        });

        Schema::table('package_duration_list', function (Blueprint $table) {
            Schema::dropIfExists('package_duration_list');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('packages', function (Blueprint $table) {
            //
        });
    }
}
