<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSeosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seos', function (Blueprint $table) {
            $table->id();

            $table->morphs('seoable');

            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('seo_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('seo_id');
            $table->string('locale')            ->index();

            $table->text('meta_description')  ->nullable();
            $table->string('meta_title')        ->nullable();
            $table->string('meta_keywords')     ->nullable();
            $table->string('slug')              ->nullable();

            $table->unique(['seo_id','locale']);
            $table->foreign('seo_id')->on('seos')->references('id')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seo_translations');
        Schema::dropIfExists('seos');
    }
}
