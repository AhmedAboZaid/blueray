<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('tourism_type_id');
            $table->unsignedBigInteger('city_id');
            $table->dateTime('from_date');
            $table->dateTime('to_date');
            $table->integer('rate')->default(0);
            $table->double('price' , 16 , 2)->default(0);
            $table->integer('nights')->default(0);
            $table->integer('group_size')->default(0);


            $table->foreign('tourism_type_id')->on('tourism_types')->references('id')->onDelete('CASCADE');
            $table->foreign('city_id')->on('cities')->references('id')->onDelete('CASCADE');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('package_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('package_id');
            $table->string('locale')->index();
            $table->string('title');
            $table->text('description');
            $table->text('travel_plan_description')->nullable();
            $table->unique(['package_id','locale']);
            $table->foreign('package_id')->on('packages')->references('id')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_translations');
        Schema::dropIfExists('trips');
    }
}
