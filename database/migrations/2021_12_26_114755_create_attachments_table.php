<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use phpDocumentor\Reflection\Types\Boolean;

class CreateAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attachments', function (Blueprint $table) {
            $table->id();
            $table->string('file')->nullable();
            $table->integer('attachmentable_id');
            $table->string('attachmentable_type');
            $table->boolean('is_main')->nullable()->default(false);
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('attachment_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('attachment_id');
            $table->string('locale')->index();
            $table->string('alts')->nullable();
            $table->unique(['attachment_id','locale']);
            $table->foreign('attachment_id')->references('id')->on('attachments')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attachments');
        Schema::dropIfExists('attachment_translations');
    }
}
