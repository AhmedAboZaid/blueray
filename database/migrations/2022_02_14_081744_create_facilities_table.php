<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFacilitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('facilities', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('facility_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('facility_id');
            $table->string('locale')->index();
            $table->string('name');
            $table->unique(['facility_id','locale']);
            $table->foreign('facility_id')->on('facilities')->references('id')->onDelete('CASCADE');
        });
        Schema::create('package_facility_include', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('package_id');
            $table->unsignedBigInteger('facility_id');
            $table->foreign('package_id')->on('packages')->references('id')->onDelete('CASCADE');
            $table->foreign('facility_id')->on('facilities')->references('id')->onDelete('CASCADE');
        });
        Schema::create('package_facility_exclude', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('package_id');
            $table->unsignedBigInteger('facility_id');
            $table->foreign('package_id')->on('packages')->references('id')->onDelete('CASCADE');
            $table->foreign('facility_id')->on('facilities')->references('id')->onDelete('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_facility_include');
        Schema::dropIfExists('trip_facility_exclude');
        Schema::dropIfExists('facility_translations');
        Schema::dropIfExists('facilities');
    }
}
