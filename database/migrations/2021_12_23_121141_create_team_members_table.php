<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('team_members', function (Blueprint $table) {
            $table->id();
            $table->string('linkedin');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::create('team_member_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('team_member_id');
            $table->string('locale')->index();
            $table->string('name');
            $table->string('position');
            $table->unique(['team_member_id','locale']);
            $table->foreign('team_member_id')->references('id')->on('team_members')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('team_members');
        Schema::dropIfExists('team_member_translations');
    }
}
