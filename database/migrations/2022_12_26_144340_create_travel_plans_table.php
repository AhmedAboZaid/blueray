<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTravelPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('travel_plans', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('package_id');
            $table->integer('number')->default(1);
            $table->time('from')->nullable()->default();
            $table->time('to')->nullable()->default();
            $table->foreign('package_id')->on('packages')->references('id')->onDelete('CASCADE');
            $table->timestamps();
        });

        Schema::create('travel_plan_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('travel_plan_id');
            $table->string('locale')->index();
            $table->string('title');
            $table->text('description');
            $table->unique(['travel_plan_id','locale']);
            $table->foreign('travel_plan_id')->on('travel_plans')->references('id')->onDelete('CASCADE');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('travel_plan_translations');
        Schema::dropIfExists('travel_plans');
    }
}
