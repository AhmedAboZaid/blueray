<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainPagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_pages', function (Blueprint $table) {
            $table->id();
            $table->string('fixed_name')->nullable();
            $table->foreignId('parent_id')->unsined()->nullable();
            $table->foreign('parent_id')->references('id')->on('main_pages');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('main_page_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('main_page_id');
            $table->string('locale')->index();
            $table->string('title');
            $table->text('description');
            $table->string('image')->nullable();
            $table->string('alt')->nullable();
            $table->unique(['main_page_id','locale']);
            $table->foreign('main_page_id')->references('id')->on('main_pages')->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_pages');
        Schema::dropIfExists('main_page_translations');
    }
}
